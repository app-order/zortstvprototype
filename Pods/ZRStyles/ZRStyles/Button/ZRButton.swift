//
//  ZRLoginAndSignUpButton.swift
//  ZRStyles
//
//  Created by Jett on 9/17/15.
//  Copyright © 2015 App-Order. All rights reserved.
//

import UIKit

@IBDesignable
public class ZRButton: UIButton {
  
  @IBInspectable public var buttonIsFilled: Bool = true
  
  @objc override public var isEnabled: Bool {
    didSet {
      setNeedsDisplay()
    }
  }
  
  @objc override public var isSelected: Bool {
    didSet {
      setNeedsDisplay()
    }
  }
  
  public override func awakeFromNib() {
    super.awakeFromNib()
    
    setTitleColor(UIColor.darkGray, for: .selected)
    setTitleColor(UIColor.darkGray, for: .disabled)

  }
  
  public override func draw(_ rect: CGRect) {
    ZRStyleKit.drawButton(rectangleFrame: bounds,
                          buttonIsFilled: buttonIsFilled,
                          isDisabled: !isEnabled,
                          isSelected: isSelected)
    }
}
