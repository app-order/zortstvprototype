//
//  ZRBackgroundView.swift
//  ZRStyles
//
//  Created by Anthony Miller on 6/8/16.
//  Copyright © 2016 App-Order. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
public class ZRBackgroundView: UIView {
    
    public private(set) var imageView: UIImageView!
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        
        setUpImageView()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setUpImageView()
    }
    
    private func setUpImageView() {
        imageView = UIImageView(frame: self.bounds)
        self.addSubview(imageView)
        imageView.clipsToBounds = true
        imageView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        imageView.contentMode = .scaleAspectFill
    }
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        
        imageView.frame = self.bounds
        
        imageView.image = imageForCurrentOrientation()
    }
    
    private func imageForCurrentOrientation() -> UIImage? {
        var cgImage: CGImage?
        
        #if os(iOS)
            cgImage = UIDevice.current.orientation.isPortrait ?
                ZRBackgroundImage.Portrait?.cgImage : ZRBackgroundImage.Landscape?.cgImage
            
        #elseif os(tvOS)
            cgImage = ZRBackgroundImage.Landscape?.cgImage
        #endif
        
        guard let image = cgImage else { return nil }
        return UIImage(cgImage: image, scale: 1.0, orientation: .up)
    }
    
}
