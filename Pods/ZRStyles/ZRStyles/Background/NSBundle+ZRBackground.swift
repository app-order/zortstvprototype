//
//  NSBundle+ZRBackground.swift
//  ZRStyles
//
//  Created by Anthony Miller on 6/8/16.
//  Copyright © 2016 App-Order. All rights reserved.
//

import Foundation

extension Bundle {
    
    class func ZRBackgroundBundle() -> Bundle {
        let sourceBundle = Bundle(for: ZRBackgroundView.self)
        let bundleURL = sourceBundle.url(forResource: "ZRStylesBackground", withExtension: "bundle")!
        return Bundle(url: bundleURL)!
    }
    
}
