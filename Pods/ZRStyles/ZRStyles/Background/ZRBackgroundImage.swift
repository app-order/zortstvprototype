//
//  ZRBackgroundImage.swift
//  ZRStyles
//
//  Created by Anthony Miller on 6/8/16.
//  Copyright © 2016 App-Order. All rights reserved.
//

import Foundation

public struct ZRBackgroundImage {
 
    public static let Portrait = UIImage(named: "Background-portrait",
                                         in: Bundle.ZRBackgroundBundle(),
                                         compatibleWith: nil)
    
    public static let Landscape = UIImage(named: "Background-landscape",
                                         in: Bundle.ZRBackgroundBundle(),
                                         compatibleWith: nil)
    
}
