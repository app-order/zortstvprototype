//
//  NSBundle+ZRTableViewSectionHeaderView.swift
//  ZRStyles
//
//  Created by Anthony Miller on 5/11/16.
//  Copyright © 2016 App-Order. All rights reserved.
//

import Foundation

extension Bundle {
    
    class func ZRTableViewSectionHeaderViewBundle() -> Bundle {
        let sourceBundle = Bundle(for: ZRTableViewSectionHeaderView.self)
        let bundleURL = sourceBundle.url(forResource: "ZRStylesTableViewSectionHeaderView", withExtension: "bundle")!
        return Bundle(url: bundleURL)!
    }
    
}
