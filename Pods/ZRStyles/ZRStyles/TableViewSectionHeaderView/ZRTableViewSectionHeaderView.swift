//
//  ZRTableViewSectionHeaderView.swift
//  ZRStyles
//
//  Created by Anthony Miller on 5/11/16.
//  Copyright © 2016 App-Order. All rights reserved.
//

import UIKit

/// A view for use as a section header view for a `UITableView`
public class ZRTableViewSectionHeaderView: UITableViewHeaderFooterView {
    
    /// Convenience accessor for the view's nib.
    public static let nib: UINib = UINib(nibName: "ZRTableViewSectionHeaderView",
                                         bundle: Bundle.ZRTableViewSectionHeaderViewBundle())
    
    /** 
     The default height for the section header view. 
     
     - note: This should be returned in `tableView(_:heightForHeaderInSection:)`.
    */
    public static let defaultHeight: CGFloat = 43.0
    
    /**
     The default height for the section header view when `removeTopGap` is true`.
     
     - note: This should be returned in `tableView(_:heightForHeaderInSection:)`.
     */
    public static let defaultHeightWithoutTopGap: CGFloat = 43.0
    
    /*
     *  MARK: - Instance Properties
     */
    
    /// The title label. This is the same label returned by `textLabel`. 
    @IBOutlet public var titleLabel: UILabel!
    
    @IBOutlet private var topSpaceConstraint: NSLayoutConstraint!
    
    override public var textLabel: UILabel? {
        get {
            return titleLabel
        }
    }
    
    /// If `true`, this removes the small gap space at the top of the view. Defaults to `false`.
    public var removeTopGap: Bool = false {
        didSet {
            topSpaceConstraint.constant = removeTopGap ? 0.0 : 4.0
        }
    }
    
    public override func prepareForReuse() {
        super.prepareForReuse()
        
        removeTopGap = false
        textLabel?.text = nil
    }
    
}
