//
//  ScheduleItemType.swift
//  ZRModels
//
//  Created by Anthony Miller on 4/29/16.
//  Copyright © 2016 App-Order. All rights reserved.
//

import Foundation

/**
 *  Instances of conforming types can have a start date computed as a `NSDateComponents` object.
 */
public protocol StartDateable {
        
    /**
     The date that the event will start on.
     
     - SeeAlso: `timeIsTBA`
    */
    var startDate: Date { get }

    /// If `true`, the `startDate` does not include a valid time for the schedule item and only the date information should be respected.
    var timeIsTBA: Bool { get }
    
    /// The timeZone for the event
    var timeZone: TimeZone? { get }
    
}

public extension StartDateable {
    
    /**
     The date components indicating the start date for the item.
     
     - note:    These components will always include the `Year`, `Month`, `Day`, `Weekday` and 
                `WeekOfMonth` of the event.
     If the time for the event has been set, the `Hour` and `Minute` components will also be included.
     */
    public var startDateComponents: DateComponents {
        let dateUnits: Set<Calendar.Component> = [.year, .month, .day, .weekday, .weekOfMonth]
        let timeUnits: Set<Calendar.Component> = [.hour, .minute]
        let units: Set<Calendar.Component> = self.timeIsTBA ? dateUnits : dateUnits.union(timeUnits)
        
        var calendar = Calendar.current
        calendar.timeZone = timeZone ?? TimeZone.autoupdatingCurrent
        return calendar.dateComponents(units, from: self.startDate)
    }
    
}
