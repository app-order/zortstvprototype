//
//  AsyncValue.swift
//  ZRExtensions
//
//  Created by Jett Farmer on 12/9/16.
//
//

import Foundation

/// A value wrapper that allows a value to be retrieved asynchronously, and calls all value accessor blocks after the
/// value has been retrieved. If the value has already been retrieved, the accessor completes synchronously.
public class AsyncValue<T> {
    
    public init(_ getter: ((@escaping (T) -> Void) -> Void)) {
        getter() { [weak self] in self?.value = $0 }
    }
    
    private var value: T? {
        didSet {
            guard let value = value else { return }
            for block in completionBlocks {
                autoreleasepool {
                    block(value)
                }
            }
            
            completionBlocks = []
        }
    }
    
    private var completionBlocks: [(T) -> Void] = []
    
    public func getValue(completion: @escaping (T) -> Void) {
        if let value = value {
            completion(value)
        } else {
            completionBlocks.append(completion)
        }
    }
    
}
