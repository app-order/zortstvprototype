//
//  AsyncOperation.swift
//  ZRExtensions
//
//  Created by Anthony Miller on 4/25/16.
//  Copyright © 2016 App-Order. All rights reserved.
//

import Foundation

/// An abstract `Operation` subclass that handles boilerplate setup for an asynchronous operation
/// with a completion block.
///
/// - Note: To use `AsynchronousOperation`, just create a subclass, override `main()`, and call
///         `finishOperation()` when the operation is finished.
public class AsyncOperation<Result>: Operation {
    
    /// The completion block that will be called when the operation is completed.
    public typealias Completion = (Result) -> Void

    private var syncCompletion: Completion?
    
    /// Initializes an operation with a completion block.
    ///
    /// - Parameter completion: The completion block to be called when the operation completes.
    public init(completion: @escaping Completion) {
        super.init()
        syncCompletion = completion
    }
    
    /// Initializes an operation without a completion block.
    public override init() {
        super.init()
    }
    
    /// This function should be called when your asynchronous operation has been finished.
    ///
    /// - Parameter result: The result of the operation.
    public final func finishOperation(result: Result) {
        if let completion = syncCompletion {
            if Thread.isMainThread {
                completion(result)
                
            } else {
                DispatchQueue.main.sync { completion(result) }
            }
        }
        
        setOperationAsFinished()
    }
    
    private func setOperationAsFinished() {
        _executing = false
        _finished = true
    }
    
    override public var isAsynchronous: Bool {
        return true
    }
    
    override public var isExecuting: Bool {
        return _executing
    }
    
    override public var isFinished: Bool {
        return _finished
    }
    
    private var _executing: Bool = false {
        willSet {
            if newValue != isExecuting { willChangeValue(forKey: "isExecuting") }
        }
        didSet {
            if oldValue != isExecuting { didChangeValue(forKey: "isExecuting") }
        }
    }
    
    private var _finished: Bool = false {
        willSet {
            if newValue != isFinished { willChangeValue(forKey: "isFinished") }
        }
        didSet {
            if oldValue != isFinished { didChangeValue(forKey: "isFinished") }
        }
    }
    
    override public func start() {
        if isCancelled { _finished = true; return }
        
        _executing = true
        main()
    }
    
}

/// A convenience alias for an operation with no results.
public typealias AsyncOperation1 = AsyncOperation<NoResult>
public struct NoResult {}
