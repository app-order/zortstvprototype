//
//  SignalExtensions.swift
//  ZRExtensions
//
//  Created by Anthony Miller on 10/26/16.
//  Copyright © 2016 App-Order. All rights reserved.
//

import ReactiveKit

public extension SignalProtocol where Element == Date {
    
    func format(_ formatter: ZRDateFormatter) -> Signal<String, Error> {
        return self.map { formatter.string(from: $0) }
    }
    
}

public extension SignalProtocol where Element == Optional<Date> {
    
    func format(_ formatter: ZRDateFormatter) -> Signal<String?, Error>  {
        return self.map {
            guard let date = $0 else { return nil }
            return formatter.string(from: date)
        }
    }
    
}

public extension SignalProtocol where Element == Bool {
    
    func only(_ value: Bool) -> Signal<Bool, Error> {
        return self.filter { $0 == value }
    }
    
    func inverse() -> Signal<Bool, Error> {
        return self.map { !$0 }
    }
    
}
