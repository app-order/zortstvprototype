//
//  DynamicSubjectExtensions.swift
//  ZRExtensions
//
//  Created by Anthony Miller on 11/10/16.
//  Copyright © 2016 App-Order. All rights reserved.
//

import Bond
import ReactiveKit

public protocol ValueSignalProtocol: BindableProtocol, SignalProtocol {
    
    var value: Element! { get }
    
}

extension DynamicSubject2: ValueSignalProtocol {}

public extension ValueSignalProtocol where Error == NoError {
    
    public func asProperty() -> Property<Element> {
        let property = Property<Element>(value)
        bidirectionalBind(to: property)
        return property
    }
    
}
