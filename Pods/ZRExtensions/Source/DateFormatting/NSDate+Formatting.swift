//
//  NSDate+Formatting.swift
//  ZRExtensions
//
//  Created by Anthony Miller on 8/15/16.
//  Copyright © 2016 App-Order. All rights reserved.
//

import Foundation

/**
 The possible formats for formatting a date.
 
 - long: Weekday Month Day. Ex. "Saturday January 1"
 */
public enum DateFormat {
    case long
    
    var formatString: String {
        switch self {
        case .long:
            return "EEEE MMMM d"
        }
    }
}

/**
 The possible formats for formatting a date as a time.
 
 - long: Hour: Minute AM/PM. Ex. "9:24 AM"
 */
public enum TimeFormat {
    case long
    
    var formatString: String {
        switch self {
        case .long:
            return "h:mm a"
        }
    }
}

/**
 A date formatter that formats a date accounting for the relevance of fields.

 `ZRDateFormatter` will include the year in a formatted date string if the given `NSDate` is not in the current year.
 `ZRDateFormatter` will include the timeZone in a formatted time string if the formatter's `timeZone` is not the user's current `localTimeZone`
 */
open class ZRDateFormatter {
    
    fileprivate let formatter = DateFormatter()
    
    /**
     The format for the date component of a date string generated using the formatter. Defaults to `.long`.
     
     - note: If both the `dateFormat` and `timeFormat` are set, the date generated will include both a date and time.
     */
    open var dateFormat: DateFormat? = DateFormat.long
    
    /**
     The format for the time component of a date string generated using the formatter. Defaults to `nil`.
     
     - note: If both the `dateFormat` and `timeFormat` are set, the date generated will include both a date and time.
     */
    open var timeFormat: TimeFormat? = nil
            
    open var timeZone: TimeZone {
        get { return formatter.timeZone }
        set { formatter.timeZone = newValue }
    }
    
    open var locale: Locale {
        get { return formatter.locale }
        set { formatter.locale = newValue }
    }

    /*
     *  MARK: -  Object Lifecycle
     */
    public init() {}
    
    open func string(from date: Date) -> String {
        formatter.dateFormat = formatString(for: date)
        return formatter.string(from: date)
    }
    
    fileprivate func formatString(for date: Date) -> String {
        var components: [String] = []
        if let dateFormat = dateFormat {
            components.append(dateFormat.formatString)
            
            if !date.isInCurrentYear(in: timeZone) { components.append("yyyy") }
        }
        
        if let timeFormat = timeFormat {
            components.append(timeFormat.formatString)
            
            if timeZone != TimeZone.autoupdatingCurrent { components.append("z") }
        }
        
        return components.joined(separator: " ")
    }
    
}

public extension Date {
    
    public func isInCurrentYear(in timeZone: TimeZone) -> Bool {
        let currentYear = Calendar.current.component(.year, from: Date())
        return currentYear == yearComponent(in: timeZone)
    }
    
    fileprivate func yearComponent(in timeZone: TimeZone) -> Int {
        var calendar = Calendar.current
        calendar.timeZone = timeZone
        return calendar.component(.year, from: self)
    }
    
    /**
     Formats the date as a string in a given format and time zone.
     
     - note: If the date is not in the current year, the date's year will be added to the string.
     
     - parameter format:   The `DateFormat` to use.
     - parameter timeZone: The time zone to convert the date to. Defaults to the device's local time zone.
     
     - returns: A formatted string for the date.
     */
    public func toString(format: DateFormat, in timeZone: TimeZone = TimeZone.autoupdatingCurrent) -> String {
        let dateFormatter = ZRDateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = timeZone
        return dateFormatter.string(from: self)
    }
    
    /**
     Formats the date's time as a string in a given format and time zone.
     
     - note: If the time zone is not the device's local time zone, the time zone will be added to the string.
     
     - parameter format:   The `TimeFormat` to use.
     - parameter timeZone: The time zone to convert the date to. Defaults to the device's local time zone.
     
     - returns: A formatted string for the date's time.
     */
    public func toString(format: TimeFormat, in timeZone: TimeZone = TimeZone.autoupdatingCurrent) -> String {
        let dateFormatter = ZRDateFormatter()
        dateFormatter.dateFormat = nil
        dateFormatter.timeFormat = format
        dateFormatter.timeZone = timeZone
        return dateFormatter.string(from: self)
    }
}
