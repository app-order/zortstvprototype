//
//  EventViewModel.swift
//  ZRScheduleListController
//
//  Created by Anthony Miller on 5/9/16.
//

import Foundation

public protocol EventViewModel: ScheduleListItemViewModel, ScheduleListItemLocationViewModel {
    
    var eventTitle: String { get }
    
}
