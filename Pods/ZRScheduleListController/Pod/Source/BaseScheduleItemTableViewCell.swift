//
//  BaseScheduleItemTableViewCell.swift
//  ZRScheduleListController
//
//  Created by Anthony Miller on 5/9/16.
//  Copyright © 2016 App-Order, LLC. All rights reserved.
//

import UIKit

class BaseScheduleItemTableViewCell: UITableViewCell {
    
    struct Constants {
        static let CanceledText = "Cancelled"
    }
    
    /*
     *  MARK: - IBOutlets
     */
    
    @IBOutlet var timeLabel: UILabel!
    
    @IBOutlet var itemDetailsLabel: UILabel!
    
    @IBOutlet var canceledLabel: UILabel!
    
    /*
     *  MARK: - View Lifecycle
     */
    
    /*
     *  MARK: - Cell Configuration
     */
    
    func configure(with item: ScheduleListItemViewModel) {
        timeLabel.text = item.startDate
        itemDetailsLabel.text = item.detailText
        canceledLabel.text = item.isCanceled ? Constants.CanceledText : nil
    }
    
}
