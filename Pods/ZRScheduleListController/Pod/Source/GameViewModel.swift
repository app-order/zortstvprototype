//
//  GameViewModel.swift
//  ZRScheduleListController
//
//  Created by Anthony Miller on 5/9/16.
//

import Foundation

import ImageValet


/// Describes a view model for a game to be displayed in a `ScheduleListController`.
public protocol GameViewModel: ScheduleListItemViewModel, ScheduleListItemLocationViewModel {
    
    /// The name to display for the home team.
    var homeTeamName: String { get }
    
    /// The score to display for the home team.
    var homeTeamScore: String { get }
    
    /// The seed number to display for the home team
    var homeTeamSeedNumber: String? { get }
    
    /// The icon to display for the home team.
    var homeTeamIcon: ImageValet { get }
    
    /// The name to display for the away team.
    var awayTeamName: String { get }
    
    /// The score to display for the away team.
    var awayTeamScore: String { get }
    
    /// The seed number to display for the away team
    var awayTeamSeedNumber: String? { get }
    
    /// The icon to display for the away team.
    var awayTeamIcon: ImageValet { get }
 
    /// A boolean indicating if the score for the game is final. If `true`, the "FINAL" label will be displayed for
    /// the game.
    var scoreIsFinal: Bool { get }
    
}
