//
//  ScheduleListItemViewModel.swift
//  ZRScheduleListController
//
//  Created by Anthony Miller on 5/9/16.
//  Copyright © 2016 App-Order, LLC. All rights reserved.
//

import Foundation

/**
 *  Describes a view model for an item to be displayed in a `ScheduleListController`.
 */
public protocol ScheduleListItemViewModel {
    
    /// The start date for the schedule item.
    var startDate: String { get }
    
    /// The text to display in the details for the schedule item.
    var detailText: String? { get }
    
    /// If `true` the event will be displayed as "Canceled"
    var isCanceled: Bool { get }
    
}

