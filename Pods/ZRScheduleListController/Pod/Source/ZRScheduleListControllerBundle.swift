//
//  ZRScheduleListControllerBundle.swift
//  ZRScheduleListController
//
//  Created by Anthony Miller on 5/9/16.
//  Copyright © 2016 App-Order, LLC. All rights reserved.
//

import Foundation

extension Bundle {
    
    static func ZRScheduleListControllerBundle() -> Bundle {
        let bundlePath = Bundle(for: ScheduleListController.self)
            .path(forResource: "ZRScheduleListController", ofType: "bundle")!
        
        return self.init(path: bundlePath)!
    }
    
}
