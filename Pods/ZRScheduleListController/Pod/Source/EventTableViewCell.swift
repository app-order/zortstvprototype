//
//  EventTableViewCell.swift
//  ZRScheduleListController
//
//  Created by Anthony Miller on 5/9/16.
//  Copyright © 2016 App-Order, LLC. All rights reserved.
//

import UIKit

class EventTableViewCell: BaseScheduleItemTableViewCell {
    
    /*
     *  MARK: - IBOutlets
     */
    
    @IBOutlet var eventTitleLabel: UILabel!
    
    @IBOutlet var locationLabel: UILabel!
    
    /*
     *  MARK: - Cell Configuration
     */
    
    override func configure(with item: ScheduleListItemViewModel) {
        super.configure(with: item)
        
        if let event = item as? EventViewModel {
            eventTitleLabel.text = event.eventTitle
            locationLabel.text = event.location
        }
    }
    
}
