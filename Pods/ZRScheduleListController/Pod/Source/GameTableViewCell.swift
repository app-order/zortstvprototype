//
//  GameTableViewCell.swift
//  ZRScheduleListController
//
//  Created by Anthony Miller on 5/9/16.
//  Copyright © 2016 App-Order, LLC. All rights reserved.
//

import UIKit

import ImageValet

protocol GameTableViewCellDelegate: class {
    
    func gameCellDidPressScoreButton(_ cell: GameTableViewCell) -> Void
    
}

class GameTableViewCell: BaseScheduleItemTableViewCell {
    
    /*
     *  MARK: - Instance Properties
     */
    
    weak var delegate: GameTableViewCellDelegate?
    
    var showScoreButton: Bool = false {
        didSet {
            // TODO: Test
            scoreButton.isHidden = !showScoreButton
        }
    }
    
    /*
     *  MARK: - IBOutlets
     */
    
    @IBOutlet var homeTeamNameLabel: UILabel!
    
    @IBOutlet var awayTeamNameLabel: UILabel!
    
    @IBOutlet var homeTeamIconImageView: UIImageView!
    
    @IBOutlet var awayTeamIconImageView: UIImageView!
    
    @IBOutlet var homeTeamScoreLabel: UILabel!
    
    @IBOutlet var awayTeamScoreLabel: UILabel!
    
    @IBOutlet var finalScoreIndicatorLabel: UILabel!
    
    @IBOutlet var scoreButton: UIButton!
    
    @IBOutlet var eventSiteLabel: UILabel!
    
    /*
     *  MARK: - IBActions
     */
    
    @IBAction func scoreButtonPressed(_ sender: AnyObject) {
        delegate?.gameCellDidPressScoreButton(self)
    }
    
    /*
     *  MARK: - Cell Configuration
     */
    
    override func configure(with item: ScheduleListItemViewModel) {
        super.configure(with: item)
        
        if let game = item as? GameViewModel {
            
            homeTeamNameLabel.text = game.homeTeamSeedNumber.notNilOrEmpty ?
                game.homeTeamName + " (\(game.homeTeamSeedNumber!))" : game.homeTeamName
           
            awayTeamNameLabel.text = game.awayTeamSeedNumber.notNilOrEmpty ?
                game.awayTeamName + " (\(game.awayTeamSeedNumber!))" : game.awayTeamName
            
            finalScoreIndicatorLabel.isHidden = !game.scoreIsFinal
            homeTeamScoreLabel.text = game.homeTeamScore
            awayTeamScoreLabel.text = game.awayTeamScore
            eventSiteLabel.text = game.location
            
            game.homeTeamIcon.deliverToImageView(homeTeamIconImageView)
            game.awayTeamIcon.deliverToImageView(awayTeamIconImageView)
        }
    }

}

private extension Optional where Wrapped == String {
    var notNilOrEmpty: Bool {
        return self != nil && !self!.isEmpty
    }
    
}
