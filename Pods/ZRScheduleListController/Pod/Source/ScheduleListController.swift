//
//  ScheduleListController.swift
//  ZRScheduleListController
//
//  Created by Anthony Miller on 5/9/16.
//  Copyright © 2016 App-Order, LLC. All rights reserved.
//

import UIKit

import CrashlyticsRecorder
import ReactiveKit
import StatefulViewController
import ZRStyles

fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


/// The data source for a `ScheduleListController`.
public protocol ScheduleListDataSource: class {
    
    /**
     - Returns: The number of days to be displayed in the schedule list.
     */
    func numberOfDays() -> Int
    
    /**
     - Parameter index: The index for a day on the schedule.
     
     - Returns: The number of schedule items for the day at the given index.
     */
    func numberOfItems(forDayAt index: Int) -> Int
    
    /**
     - Parameter indexPath: An index path where the section indicates the day and the row indicates the item in the day.
     
     - Returns: The schedule item for the given index path.
     */
    func scheduleItem(at indexPath: IndexPath) -> ScheduleListItemViewModel
    
    /**
     - Parameter index: The index for a day on the schedule.
     
     - Returns: The title to display for the day at the given index.
     */
    func titleForDay(at index: Int) -> String
    
    /// Should return the index path for the nearest item to the given date.
    ///
    /// - Parameter date: The date to return the nearest item to.
    /// - Returns: The `IndexPath` nearest to the item.
    func indexPathForItem(nearestTo date: Date) -> IndexPath?
    
    /// A Signal1 that will emit an event after the schedule items are reloaded.
    var didReloadContent: Signal1<Void> { get }
    
    /// A Signal1 that will emit an event before any updates are made to the schedule items.
    var willChangeContent: Signal1<Void> { get }
    
    /// A Signal1 that will emit an event after a batch of updates are made to the schedule items.
    var didChangeContent: Signal1<Void> { get }
    
    /// A Signal1 that will emit an event when a new day is added to the schedule with a value of the new day's index.
    var didInsertDay: Signal1<Int> { get }
    
    /// A Signal1 that will emit an event when a day is removed from the schedule with a value of the removed day's index.
    var didDeleteDay: Signal1<Int> { get }
    
    /// A Signal1 that will emit an event when a new schedule item is added to the schedule with a value of the new 
    /// item's index path.
    var didInsertItem: Signal1<IndexPath> { get }
    
    /// A Signal1 that will emit an event when a schedule item is removed from the schedule with a value of the removed
    /// item's index path.
    var didDeleteItem: Signal1<IndexPath> { get }
    
    /// A Signal1 that will emit an event when a the values on a schedule item are changed with a value of the updated 
    /// item's index path.
    var didUpdateItem: Signal1<IndexPath> { get }
    
    /// A Signal1 that will emit an event when a schedule item is moved on the schedule with a value of the index path 
    /// the item is moving from and the index path the item is moving to.
    var didMoveItem: Signal1<(from: IndexPath, to: IndexPath)> { get }
    
}

open class ScheduleListController: UIViewController, UITableViewDataSource, UITableViewDelegate,
 StatefulViewController, GameTableViewCellDelegate {
    
    /*
     *  MARK: - Constants
     */
    
    fileprivate struct Constants {
        static let GameCellIdentifier = "GameCell"
        static let EventCellIdentifier = "EventCell"
        static let SectionHeaderIdentifier = "SectionHeader"
        
        static let rowHeight: CGFloat = UITableViewAutomaticDimension
        static let estimatedRowHeight: CGFloat = 129.0
    }
    
    /*
     *  MARK: - Errors
     */
    
    fileprivate enum Error: ErrorReportable {
        case noDayFound(section: Int)
        
        fileprivate func errorReportTitle() -> String? {
            switch self {
            case .noDayFound(let section):
                return ("Section \(section) was accessed, but there was no day for the section")
            }
        }
        
        fileprivate func errorReportUserInfo() -> [String : Any]? {
            return nil
        }
    }
    
    /*
     *  MARK: - Instance Properties
     */
    
    open internal(set) var tableView: UITableView?
    
    internal var tableViewClass: UITableView.Type = UITableView.self
    
    open weak var dataSource: ScheduleListDataSource? {
        didSet {
            startObservingDataSource()
            tableView?.reloadData()
        }
    }
    
    fileprivate var dataSourceDisposeBag: DisposeBag?
    
    /*
     *  MARK: - Delegate Closures
     */
    
    /**
     A closure that can be set to be notified when a schedule item is selected.     
     */
    open var didSelectScheduleItem: ((ScheduleListItemViewModel) -> Void)?
    
    /**
     A closure that can be set to determine if a given game can be scored.
     
     If this closure returns `true`, a score button will be displayed for the game and the `didPressScoreButton` closure may be called for the given game.
     
     Defaults to return `false`.
     */
    open var canScoreGame: ((GameViewModel) -> Bool) = { _ in return false }
    
    /**
     A closure that can be set to be notified when the score button is pressed for a given game.
     
     This closure will only ever be called for a game for which `canScoreGame` Returns `true`.
     */
    open var didPressScoreButton: ((GameViewModel) -> Void)?
    
    /*
     *  MARK: - Object Lifecycle
     */
    
    public init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    /*
     *  MARK: - Scroll To Date
     */
    
    /**
     Scrolls the `tableView` so the given date or the closest date after the given date.
     
     - Parameter date:     The date to scroll to.
     - Parameter animated: `true` if scrolling should be animated.
     */
    open func scroll(to date: Date, animated: Bool) {
        if let indexPath = dataSource?.indexPathForItem(nearestTo: date) {
            tableView?.scrollToRow(at: indexPath, at: .middle, animated: animated)
        }
    }
    
    /*
     *  MARK: - View Lifecycle
     */
    
    open override func viewDidLoad() {
        setUpEmptyState()
        setUpTableView()
    }
    
    fileprivate func setUpTableView() {
        let tableView = tableViewClass.init(frame: view.frame, style: .plain)
        self.tableView = tableView
        tableView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        view.addSubview(tableView)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = Constants.rowHeight
        tableView.estimatedRowHeight = Constants.estimatedRowHeight
        tableView.backgroundColor = UIColor.clear
        tableView.backgroundView = nil
        #if !os(tvOS)
            tableView.separatorStyle = .none
        #endif
        
        registerGameNib()
        registerEventNib()
        registerSectionHeaderNib()
    }
    
    fileprivate func registerGameNib() {
        let gameNib = UINib(nibName: "GameTableViewCell", bundle: Bundle.ZRScheduleListControllerBundle())
        tableView?.register(gameNib, forCellReuseIdentifier: Constants.GameCellIdentifier)
    }
    
    fileprivate func registerEventNib() {
        let eventNib = UINib(nibName: "EventTableViewCell", bundle: Bundle.ZRScheduleListControllerBundle())
        tableView?.register(eventNib, forCellReuseIdentifier: Constants.EventCellIdentifier)
    }
    
    fileprivate func registerSectionHeaderNib() {
        let sectionHeaderNib = ZRTableViewSectionHeaderView.nib
        tableView?.register(sectionHeaderNib, forHeaderFooterViewReuseIdentifier: Constants.SectionHeaderIdentifier)
    }
    
    fileprivate func setUpEmptyState() {
        emptyView = Bundle.ZRScheduleListControllerBundle()
            .loadNibNamed("EmptyStateView", owner: nil, options: nil)?.first as? UIView
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupInitialViewState()
    }
    
    // MARK: Observing Schedule Changes
    
    fileprivate func startObservingDataSource() {
        self.dataSourceDisposeBag?.dispose()
        
        let dataSourceDisposeBag = DisposeBag()
        self.dataSourceDisposeBag = dataSourceDisposeBag
        
        dataSource?.didReloadContent
            .observeNext { [weak self] in
                self?.tableView?.reloadData()
            }
            .dispose(in: dataSourceDisposeBag)
        
        dataSource?.willChangeContent
            .observeNext { [weak self] in
                self?.tableView?.beginUpdates()
            }
            .dispose(in: dataSourceDisposeBag)
        
        dataSource?.didChangeContent
            .observeNext { [weak self] in
                self?.tableView?.endUpdates()
                self?.endLoading()
            }
            .dispose(in: dataSourceDisposeBag)
        
        dataSource?.didInsertDay
            .observeNext { [weak self] index in
                self?.tableView?.insertSections(IndexSet(integer: index), with: .automatic)
            }
            .dispose(in: dataSourceDisposeBag)

        dataSource?.didDeleteDay
            .observeNext {[weak self] index in
                self?.tableView?.deleteSections(IndexSet(integer: index), with: .automatic)
            }
            .dispose(in: dataSourceDisposeBag)
        
        dataSource?.didInsertItem
            .observeNext { [weak self] indexPath in
                self?.tableView?.insertRows(at: [indexPath], with: .automatic)
            }
            .dispose(in: dataSourceDisposeBag)
        
        dataSource?.didDeleteItem
            .observeNext { [weak self] indexPath in
                self?.tableView?.deleteRows(at: [indexPath], with: .automatic)
            }
            .dispose(in: dataSourceDisposeBag)
        
        dataSource?.didUpdateItem
            .observeNext { [weak self] indexPath in
                if let cell = self?.tableView?.cellForRow(at: indexPath) as? BaseScheduleItemTableViewCell,
                let item = self?.dataSource?.scheduleItem(at: indexPath) {
                    cell.configure(with: item)
                }
            }
            .dispose(in: dataSourceDisposeBag)
        
        dataSource?.didMoveItem
            .observeNext { [weak self] fromIndexPath, toIndexPath in
                self?.tableView?.deleteRows(at: [fromIndexPath], with: .automatic)
                self?.tableView?.insertRows(at: [toIndexPath], with: .automatic)
            }
            .dispose(in: dataSourceDisposeBag)
        
    }
    
    /*
     *  MARK: - UITableViewDataSource
     */
    
    open func numberOfSections(in tableView: UITableView) -> Int {
        return dataSource?.numberOfDays() ?? 0
    }
    
    open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource?.numberOfItems(forDayAt: section) ?? 0
    }

    open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let item = dataSource?.scheduleItem(at: indexPath) else {
            return UITableViewCell(style: .default, reuseIdentifier: "")
        }
        
        if let game = item as? GameViewModel {
            return cell(for: game, at: indexPath)
            
        } else if let event = item as? EventViewModel {
            return cell(for: event, at: indexPath)
            
        } else {
            return UITableViewCell(style: .default, reuseIdentifier: "")
        }

    }
    
    fileprivate func cell(for game: GameViewModel, at indexPath: IndexPath) -> GameTableViewCell {
        let cell = tableView?.dequeueReusableCell(withIdentifier: Constants.GameCellIdentifier,
                                                               for: indexPath) as! GameTableViewCell
        
        cell.configure(with: game)
        cell.showScoreButton = canScoreGame(game)        
        cell.delegate = self
        
        return cell
    }
    
    fileprivate func cell(for event: EventViewModel, at indexPath: IndexPath) -> EventTableViewCell {
        let cell = tableView?.dequeueReusableCell(withIdentifier: Constants.EventCellIdentifier,
                                                               for: indexPath) as! EventTableViewCell
        cell.configure(with: event)
        return cell
    }
    
    /*
     *  MARK: - UITableViewDelegate
     */
    
    open func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return self.tableView(tableView,
                              numberOfRowsInSection: section) > 0 ? ZRTableViewSectionHeaderView.defaultHeight : 0
    }
    
    open func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard self.tableView(tableView, numberOfRowsInSection: section) > 0 else { return nil }
        guard let view = tableView
            .dequeueReusableHeaderFooterView(withIdentifier: Constants.SectionHeaderIdentifier) else { return nil }

        view.textLabel?.text = dataSource?.titleForDay(at: section)
        
        return view
    }
    
    open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let item = dataSource?.scheduleItem(at: indexPath) else { return }
        didSelectScheduleItem?(item)
    }
    
    // MARK: tvOS Focus Support
    
    open func tableView(_ tableView: UITableView, canFocusRowAt indexPath: IndexPath) -> Bool {
        return didSelectScheduleItem != nil
    }
    
    /*
     *  MARK: - GameTableViewCellDelegate
     */
    
    func gameCellDidPressScoreButton(_ cell: GameTableViewCell) {
        guard let indexPath = tableView?.indexPath(for: cell),
            let game = dataSource?.scheduleItem(at: indexPath) as? GameViewModel else {
                return
        }
        
        didPressScoreButton?(game)
    }
    
    /*
     *  MARK: - Stateful View Controller
     */
    
    open func hasContent() -> Bool {
        guard let dataSource = dataSource else { return false }
        return dataSource.numberOfDays() > 0 
    }
}
