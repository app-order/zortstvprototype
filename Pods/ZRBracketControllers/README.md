# ZRBracketControllers

[![CI Status](http://img.shields.io/travis/Anthony Miller/ZRBracketControllers.svg?style=flat)](https://travis-ci.org/Anthony Miller/ZRBracketControllers)
[![Version](https://img.shields.io/cocoapods/v/ZRBracketControllers.svg?style=flat)](http://cocoapods.org/pods/ZRBracketControllers)
[![License](https://img.shields.io/cocoapods/l/ZRBracketControllers.svg?style=flat)](http://cocoapods.org/pods/ZRBracketControllers)
[![Platform](https://img.shields.io/cocoapods/p/ZRBracketControllers.svg?style=flat)](http://cocoapods.org/pods/ZRBracketControllers)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ZRBracketControllers is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "ZRBracketControllers"
```

## Author

Anthony Miller, anthony@app-order.com

## License

ZRBracketControllers is available under the MIT license. See the LICENSE file for more info.
