//
//  BracketViewController.swift
//  Pods
//
//  Created by Max Tower on 5/3/16.
//
//

import UIKit
import ImageValet
import ReactiveKit
import ZRStyles

fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}

fileprivate func >= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l >= r
  default:
    return !(lhs < rhs)
  }
}


open class BracketViewController: UIViewController, UIScrollViewDelegate, MatchupTapDelegate {
	
	var games: [LeagueGameViewModel]?
	var loserGames: [LeagueGameViewModel]?
	var doubleElimChampGames: [LeagueGameViewModel]?
	var allGames: [LeagueGameViewModel]?
	var winnerGamesTree: BinaryTree<LeagueGameViewModel>?
	
	let MATCHUP_WIDTH: CGFloat = 60.0
	let MATCHUP_HEIGHT: CGFloat = 33.0
    let MATCHUP_ZOOM_WIDTH_THRESHOLD: CGFloat = 200
	let CENTER_WIDTH: CGFloat = 270.0
	let HORIZ_SPACING: CGFloat = 3.0
	let VERT_SPACING: CGFloat = 3.0
    let VERT_PADDING: CGFloat = 10.0
    let MATCHUP_ZOOM_PADDING:  CGFloat = 18.0
    let CHAMP_TEXT_HEIGHT: CGFloat = 15.0
    let CHAMP_FONT_SIZE: CGFloat = 12
    let TOURNAMENT_LOGO_SIZE: CGFloat = 90.0
    let ZORTS_BANNER_HEIGHT:CGFloat = 60 * 2 / 3.33
    let ZORTS_BANNER_VERT_SPACING: CGFloat = 10.0
    let WINNERS_BRACKET_FINAL_TO_CHAMP_GAME_VERT_SPACING: CGFloat = 30.0
	var bracketContainerWidth: CGFloat = 0    
	var bracketContainerHeight: CGFloat = 0
	var loserBracketContainerWidth: CGFloat = 0
	var loserBracketContainerHeight: CGFloat = 0
    var bracketContentInset: UIEdgeInsets = UIEdgeInsets.zero
    var loserBracketContentInset: UIEdgeInsets = UIEdgeInsets.zero
	var winnerRoundsOnLeft: Int = 0
	var winnerRoundsOnRight: Int = 0
	var loserCenterAdj:CGFloat = 0.0
    
	private var backgroundLinesView: BackgroundLinesView!
	private var loserBackgroundLinesView: BackgroundLinesView!
	private var scrollingView: BackgroundLinesView!
    
    private var hasBeenSetUp: Bool = false
    
    private let backgroundView: ZRBackgroundView = {
        let view = ZRBackgroundView()
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        return view
    }()
    
    private lazy var winnersScrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        scrollView.maximumZoomScale = 5.0
        scrollView.clipsToBounds = true
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.contentMode = .scaleToFill
        scrollView.delegate = self
        return scrollView
    }()
    
    private lazy var losersScrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        scrollView.maximumZoomScale = 5.0
        scrollView.clipsToBounds = true
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.contentMode = .scaleToFill
        scrollView.delegate = self
        return scrollView
    }()
    
    public var currentScrollView: UIScrollView!
    private var matchupViews: Set<MatchupView> = []
	private var tournamentLogo: ImageValet?
    private var championshipTextLabel: ScaleFactorRedrawingLabel?
	private var doubleChampTextLabel: ScaleFactorRedrawingLabel?
    
    private var lastZoomScaleFactor: CGFloat = 1.0
	
	public private(set) var isSingleElimination = true
    
    public var isDisplayingLosersBracket: Bool {
        return currentScrollView == losersScrollView
    }
	
	public var zoomToFullView: Bool = false
    
    public var showBackgroundImage: Bool = true {
        didSet {
            backgroundView.isHidden = !showBackgroundImage
        }
    }

    /// This can be set to a closure that will be called whenever a `MatchupView` for a game is selected by the user.
    open var didSelectGame: ((LeagueGameViewModel) -> Void)?		
    
    open override func loadView() {
        view = UIView()
        backgroundView.frame = view.frame
        view.addSubview(backgroundView)
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if hasBeenSetUp {
            let scale = scaleFactor(forZoomScale: currentScrollView.zoomScale)
            redrawLabels(at: scale, onlyVisible: false)
        }
    }
    
	open override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if !hasBeenSetUp {
            setUpViews()
        }
	}
    
    private func setUpViews() {
        setUpBackgroundLinesView()
        setupLoserBackgroundLinesView()
        setUpScrollView()
        expandBracketForGames()
        expandLoserBracketForGames()
        winnersScrollView.addSubview(backgroundLinesView!)
        
        if !isSingleElimination {
            losersScrollView.addSubview(loserBackgroundLinesView!)
        }
        
        zoomOutToFullBracket(false)
        
        hasBeenSetUp = true
    }
	
    fileprivate func setUpScrollView() {
        winnersScrollView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        winnersScrollView.contentSize = CGSize(width: bracketContainerWidth, height: bracketContainerHeight)
        winnersScrollView.contentInset = bracketContentInset
				        
		self.view.addSubview(winnersScrollView)
		currentScrollView = winnersScrollView
		
        if !isSingleElimination {
            losersScrollView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            losersScrollView.contentSize = CGSize(width: loserBracketContainerWidth, height: loserBracketContainerHeight)
            losersScrollView.contentInset = loserBracketContentInset
        }
    }
    
    fileprivate func setUpBackgroundLinesView() {
        backgroundLinesView = BackgroundLinesView(frame: CGRect(x: 0, y: 0,width: bracketContainerWidth, height: bracketContainerHeight))
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapBackgroundView(_:)))
        backgroundLinesView?.addGestureRecognizer(tapGesture)
		scrollingView = backgroundLinesView
    }
	
	fileprivate func setupLoserBackgroundLinesView()
	{
        if !isSingleElimination {
            loserBackgroundLinesView = BackgroundLinesView(frame: CGRect(x: 0, y: 0, width: loserBracketContainerWidth, height: loserBracketContainerHeight))
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapLoserBgView(_:)))
            loserBackgroundLinesView?.addGestureRecognizer(tapGesture)
        }
	}
    
    fileprivate func zoomOutToFullBracket(_ animated: Bool) {
        let startingScaleW: CGFloat = winnersScrollView.frame.size.width / bracketContainerWidth
		let startingScaleH: CGFloat = winnersScrollView.frame.size.height / bracketContainerHeight
		if((startingScaleH > startingScaleW) || !zoomToFullView)
		{
			winnersScrollView.minimumZoomScale = startingScaleW
			winnersScrollView.setZoomScale(startingScaleW, animated: animated)
		}
		else
		{
			winnersScrollView.minimumZoomScale = startingScaleH
			winnersScrollView.setZoomScale(startingScaleH, animated: animated)
		}
        
        winnersScrollView.setContentOffset(CGPoint.zero, animated: animated)
    }
	
	fileprivate func zoomOutToFullLoserBracket(_ animated: Bool) {
		let startingScaleW: CGFloat = losersScrollView.frame.size.width / loserBracketContainerWidth
		let startingScaleH: CGFloat = losersScrollView.frame.size.height / loserBracketContainerHeight
		if((startingScaleH > startingScaleW) || !zoomToFullView)
		{
			losersScrollView.minimumZoomScale = startingScaleW
			losersScrollView.setZoomScale(startingScaleW, animated: animated)
		}
		else
		{
			losersScrollView.minimumZoomScale = startingScaleH
			losersScrollView.setZoomScale(startingScaleH, animated: animated)
		}
        
        losersScrollView.setContentOffset(CGPoint.zero, animated: animated)
	}
	
    @objc func didTapBackgroundView(_ sender: UITapGestureRecognizer) {
        zoomOutToFullBracket(true)
    }
	
	@objc func didTapLoserBgView(_ sender: UITapGestureRecognizer) {
		zoomOutToFullLoserBracket(true)
	}
	
	open func setGames(_ listGames: [LeagueGameViewModel])
	{
		func gameNumSort(lhs: LeagueGameViewModel, rhs: LeagueGameViewModel) -> Bool
		{
			return lhs.bracketGame > rhs.bracketGame
		}
		
	    let listGames = listGames.sorted(by: gameNumSort)
		self.allGames = listGames
	    if(isSingleElimination(games: listGames))
		{
		    isSingleElimination = true
			self.games = listGames
			
		}
		else
		{
		    isSingleElimination = false
			sortGamesWinnersLosers(unsortedGames: listGames)
			calculateLoserContentSize()
			bracketContainerHeight = max(bracketContainerHeight, 2*MATCHUP_HEIGHT + 2*TOURNAMENT_LOGO_SIZE)
		}
		
		winnerGamesTree = newTreeFromRootGame(game: (self.games?[0])!)
		self.games = []
		let height: Int = (winnerGamesTree?.height)!
		for i in 1 ... height
		{
			traverseGivenLevel(tree: winnerGamesTree!, level: i)
		}
		
		calculateContentSize()
	}
	
	open func setTournamentLogo(_ tournamentLogo: ImageValet)
	{
		self.tournamentLogo = tournamentLogo
		calculateContentSize()
	}
	
	func calculateContentSize()
	{
	    bracketContainerWidth = HORIZ_SPACING
        
        var minExpandedBracketHeight: CGFloat = 0
        
		var i = 4
		while(games?.count > (i-1))
		{
			if(isRoundPopulated(games!, startGame: i/2-1, endGame: i-i/4-2))
            {
                bracketContainerWidth += MATCHUP_WIDTH + HORIZ_SPACING
                minExpandedBracketHeight = max(minExpandedBracketHeight, CGFloat(i)/2.0*MATCHUP_HEIGHT + CGFloat(i)/2.0*VERT_SPACING)
                winnerRoundsOnLeft += 1
			}
			if(isRoundPopulated(games!, startGame: i-i/4-1, endGame: i-2))
			{
                bracketContainerWidth += MATCHUP_WIDTH + HORIZ_SPACING
                minExpandedBracketHeight = max(minExpandedBracketHeight, CGFloat(i)/2.0*MATCHUP_HEIGHT + CGFloat(i)/2.0*VERT_SPACING)
				winnerRoundsOnRight += 1
			}
            
			i *= 2
		}
		
        let (middleTopHalf, middleBottomHalf) = calculateMiddleColumnHeight()
        let minBracketHalfHeight = (minExpandedBracketHeight / 2) + VERT_PADDING
        let topHalfHeight = max(middleTopHalf, minBracketHalfHeight)
        let bottomHalfHeight = max(middleBottomHalf, minBracketHalfHeight)
        
        bracketContainerHeight = topHalfHeight + bottomHalfHeight
		bracketContainerWidth += CENTER_WIDTH + HORIZ_SPACING + HORIZ_SPACING
	}
    
    private func calculateMiddleColumnHeight() -> (topHalf: CGFloat, bottomHalf: CGFloat) {
        var topHalfHeight: CGFloat = VERT_PADDING
        var bottomHalfHeight: CGFloat = VERT_PADDING
        
        topHalfHeight += MATCHUP_HEIGHT // Championship matchup view
        bottomHalfHeight += MATCHUP_HEIGHT
        
        if (!isSingleElimination) {
            topHalfHeight += WINNERS_BRACKET_FINAL_TO_CHAMP_GAME_VERT_SPACING
            topHalfHeight += MATCHUP_HEIGHT * 2
            bottomHalfHeight +=  CHAMP_TEXT_HEIGHT * 2 + VERT_SPACING * 2 // "Winners Bracket Final" text
            
        } else {
            bottomHalfHeight += ZORTS_BANNER_HEIGHT + ZORTS_BANNER_VERT_SPACING // Zorts banner
        }
        
        if tournamentLogo != nil {
            topHalfHeight += TOURNAMENT_LOGO_SIZE + VERT_SPACING // Tournament logo
        }
        
        topHalfHeight += CHAMP_TEXT_HEIGHT + VERT_SPACING // "Championship" text
        
        return (topHalfHeight, bottomHalfHeight)
    }
	
	func calculateLoserContentSize()
	{
        let widthOfRound = MATCHUP_WIDTH + HORIZ_SPACING
//		let final6width = widthOfRound*4 + 3*(HORIZ_SPACING*3)
		
		if let gamesList = loserGames
		{
            let uniqueRounds = Set(gamesList.map { $0.bracketRound }).count            

			let maxGamesInLastRound: Int = Int(pow(Double(2), Double(((uniqueRounds + 1) / 2 - 1))))
			let lastRoundNumber = -1
			var numberOfGamesInLastRound = 0
			for g in gamesList
			{
				if(g.bracketRound == lastRoundNumber)
				{
					numberOfGamesInLastRound += 1
				}
			}
			let totalHeight = MATCHUP_HEIGHT + VERT_SPACING
			
			let multiplier = CGFloat(2 - uniqueRounds%2)
			loserBracketContainerHeight = max(CGFloat(maxGamesInLastRound/2) * totalHeight * multiplier, totalHeight*7)
			loserBracketContainerHeight += VERT_SPACING
			
			let numInRound1 = countRoundGames(games: gamesList, round: -1)
			
			if(numInRound1 == maxGamesInLastRound && gamesList.count > 6)
			{
				loserBracketContainerHeight = 2*loserBracketContainerHeight - VERT_SPACING
			}
			loserBracketContainerWidth = widthOfRound + (2*HORIZ_SPACING*6)
			if(gamesList.count > 1)
			{
				let loserTree = newLoserTreeFromRootGame(game: gamesList[1])
				
				let widthsToAdd: CGFloat = CGFloat(loserTree.getLeft().height + loserTree.getRight().height)
				
				// last term comes from extra horiz space in center
				loserBracketContainerWidth = (widthsToAdd+1) * widthOfRound + (2*HORIZ_SPACING*6)
				loserBracketContainerWidth += HORIZ_SPACING
				
				if(loserTree.getLeft().height > loserTree.getRight().height)
				{
					loserCenterAdj = -1*widthOfRound/2
				}
				else if(loserTree.getLeft().height < loserTree.getRight().height)
				{
					loserCenterAdj = widthOfRound/2
				}
			}

		}
	}        
	
	func expandBracketForGames()
	{
		var currentRoundX: CGFloat = HORIZ_SPACING
		var i = largestPower2LessThan((games?.count)!)
		
		while(i > 3)
		{
			if(addRoundToView(games!, startGame: i/2-1, endGame: i-i/4-2, curRoundX: currentRoundX))
			{
				currentRoundX += MATCHUP_WIDTH + HORIZ_SPACING
			}
			i = i/2
		}
		currentRoundX += CENTER_WIDTH + HORIZ_SPACING + HORIZ_SPACING
		addChampionshipGameView()
		if(games?.count > 1)
		{
			addSemiFinalLeft()
		}
		if(games?.count > 2)
		{
			addSemiFinalRight()
		}
		i = 4
		while(games?.count > (i-1))
		{
			if(addRoundToView(games!, startGame: i-i/4-1, endGame: i-2, curRoundX: currentRoundX))
			{
				currentRoundX += MATCHUP_WIDTH + HORIZ_SPACING
			}
			i *= 2
		}
	}
	
	func expandLoserBracketForGames()
	{
		var currentRoundX: CGFloat = HORIZ_SPACING
		let loserCount = loserGames?.count ?? 0
		var i = largestPower2LessThan(loserCount)
		i = i*2
		while(i > 10)
		{
		    let startingGameNum = (i-2) - (i/4)
			let endingGameNum = startingGameNum + (i/8) - 1
			if(loserGames?.count > (i-2))
			{
				if(addLoserRoundToView(games: loserGames!, startGame: startingGameNum, endGame: endingGameNum, curRoundX: currentRoundX, awayOnly: false))
				{
					currentRoundX += MATCHUP_WIDTH + HORIZ_SPACING
				}
			}
			let nextStartGame = startingGameNum - (i/4)
			let nextEndGame = nextStartGame + (i/8) - 1
			if(loserGames?.count > startingGameNum)
			{
				if(addLoserRoundToView(games: loserGames!, startGame: nextStartGame, endGame: nextEndGame, curRoundX: currentRoundX, awayOnly: true))
				{
					currentRoundX += MATCHUP_WIDTH + HORIZ_SPACING
				}
			}
			i = i/2
		}
		if(loserCount > 6)
		{
			if(addLoserRoundToView(games: loserGames!, startGame: 4, endGame: 4, curRoundX: currentRoundX, awayOnly: false))
			{
				currentRoundX += MATCHUP_WIDTH + HORIZ_SPACING
			}
		}
		//do center stuff here
		if(loserCount > 0)
		{
			currentRoundX += addFinal6Losers(games: loserGames!)
		}
		
		
		if(loserCount > 6)
		{
			if(addLoserRoundToView(games: loserGames!, startGame: 5, endGame: 5, curRoundX: currentRoundX, awayOnly: false))
			{
				currentRoundX += MATCHUP_WIDTH + HORIZ_SPACING
			}
		}
		i = 16
		while(loserCount > (i/2)-2)
		{
			let startingGameNum = (i-2) - (i/4) - i/8
			let endingGameNum = (i-2) - (i/4) - 1
			if(loserGames?.count > (i-2) - (i/4))
			{
				if(addLoserRoundToView(games: loserGames!, startGame: startingGameNum, endGame: endingGameNum, curRoundX: currentRoundX, awayOnly: true))
				{
					currentRoundX += MATCHUP_WIDTH + HORIZ_SPACING
				}
			}
			let nextStartGame = (i-2) - (i/8)
			let nextEndGame = (i-2) - 1
			if(loserGames?.count > i-2)
			{
				if(addLoserRoundToView(games: loserGames!, startGame: nextStartGame, endGame: nextEndGame, curRoundX: currentRoundX, awayOnly: false))
				{
					currentRoundX += MATCHUP_WIDTH + HORIZ_SPACING
				}
			}
			i = i*2
		}
	}
	
	func isRoundPopulated(_ games: [LeagueGameViewModel], startGame: Int, endGame: Int) -> Bool
	{
		var roundNeeded = false
		for i in startGame ... endGame
		{
			if(games[i].awayTeamSourceGame != nil)
			{
				roundNeeded = true
			}
			if(games[i].homeTeamSourceGame != nil)
			{
				roundNeeded = true
			}
		}
		return roundNeeded
	}
	
	func addFinal6Losers(games: [LeagueGameViewModel]) -> CGFloat
	{
	    let extraHorizSpace = HORIZ_SPACING*3
	    var resultWidth = MATCHUP_WIDTH + extraHorizSpace * 2
		
		var champCenter: CGPoint = CGPoint(x: 0.0, y: 0.0)
		var semiCenter: CGPoint?
		var semiLeftCenter: CGPoint?
		var semiRightCenter: CGPoint?
		
		if(games.count == 1)
		{
			champCenter = addLoserChamp(game: games[0])
		}
		else if(games.count == 2)
		{
			champCenter = addLoserChamp(game: games[0])
			semiCenter = addLoserSemiChamp(game: games[1])
		}
		else if(games.count == 3)
		{
			resultWidth = 2 * MATCHUP_WIDTH + 3 * extraHorizSpace
			champCenter = addLoserChamp(game: games[0])
			semiCenter = addLoserSemiChamp(game: games[1])
			semiLeftCenter = addLoserSemiLeft(game: games[2])
		}
		else if(games.count == 4)
		{
			resultWidth = 3 * MATCHUP_WIDTH + 4 * extraHorizSpace
			champCenter = addLoserChamp(game: games[0])
			semiCenter = addLoserSemiChamp(game: games[1])
			semiLeftCenter = addLoserSemiLeft(game: games[2])
			semiRightCenter = addLoserSemiRight(game: games[3])
		}
		else if(games.count == 5)
		{
			resultWidth = 4 * MATCHUP_WIDTH + 5 * extraHorizSpace
			champCenter = addLoserChamp(game: games[0])
			semiCenter = addLoserSemiChamp(game: games[1])
			semiLeftCenter = addLoserSemiLeft(game: games[2])
			semiRightCenter = addLoserSemiRight(game: games[3])
			addLoserQuarterLeft(game: games[4])
		}
		else if(games.count >= 6)
		{
		    resultWidth = 5 * MATCHUP_WIDTH + 6 * extraHorizSpace
			champCenter = addLoserChamp(game: games[0])
			semiCenter = addLoserSemiChamp(game: games[1])
			semiLeftCenter = addLoserSemiLeft(game: games[2])
			semiRightCenter = addLoserSemiRight(game: games[3])
			addLoserQuarterLeft(game: games[5])
			addLoserQuarterRight(game: games[4])
			
		}
		
		let originX = champCenter.x - MATCHUP_WIDTH/2
		let originY = champCenter.y - MATCHUP_HEIGHT/2
		
		let championshipTextFrame = CGRect(x: originX,
		                                   y: originY - 2*CHAMP_TEXT_HEIGHT,
		                                   width: MATCHUP_WIDTH,
		                                   height: 2*CHAMP_TEXT_HEIGHT)
        let champLabel = ScaleFactorRedrawingLabel(frame: championshipTextFrame)
		champLabel.text = "Loser's Bracket Finals"
		champLabel.textColor = UIColor.white
		champLabel.font = UIFont(name: "TrebuchetMS-Bold", size: 9)
		champLabel.textAlignment = NSTextAlignment.center
		champLabel.numberOfLines = 2
		champLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
		loserBackgroundLinesView?.addSubview(champLabel)
        championshipTextLabel = champLabel
		
		if let pt = semiCenter
		{
			loserBackgroundLinesView.drawSemiFinalLine(champCenter, endPt: pt)
		}
		if let pt = semiLeftCenter
		{
			let midPt = CGPoint(x: pt.x + MATCHUP_WIDTH/2 + 10, y: pt.y)
			loserBackgroundLinesView.drawTwoSegmentLine(startPt: pt, midPt: midPt, endPt: semiCenter!)
		}
		if let pt = semiRightCenter
		{
			let midPt = CGPoint(x: pt.x - MATCHUP_WIDTH/2 - 10, y: pt.y)
			loserBackgroundLinesView.drawTwoSegmentLine(startPt: pt, midPt: midPt, endPt: semiCenter!)
		}
		return resultWidth
	}
	
    @discardableResult
	func addLoserChamp(game: LeagueGameViewModel) -> CGPoint
	{
		let contentCenterPt = loserBackgroundLinesView?.center
		let originX = contentCenterPt!.x - MATCHUP_WIDTH/2 - computeHorizontalCenterAdjLoser()
		let originY = contentCenterPt!.y - MATCHUP_HEIGHT/2 - 2*MATCHUP_HEIGHT - 2*VERT_SPACING
		let muView = MatchupView(frame: CGRect(x: originX,y: originY,width: MATCHUP_WIDTH,height: MATCHUP_HEIGHT))
		loserBackgroundLinesView?.addSubview(muView)
		muView.setGame(game)
		muView.delegate = self
		matchupViews.insert(muView)
		return muView.center
	}
	
    @discardableResult
	func addLoserSemiChamp(game: LeagueGameViewModel) -> CGPoint
	{
		let contentCenterPt = loserBackgroundLinesView?.center
		let originX = contentCenterPt!.x - MATCHUP_WIDTH/2 - computeHorizontalCenterAdjLoser()
		let originY = contentCenterPt!.y - MATCHUP_HEIGHT/2
		let muView = MatchupView(frame: CGRect(x: originX,y: originY,width: MATCHUP_WIDTH,height: MATCHUP_HEIGHT))
		loserBackgroundLinesView?.addSubview(muView)
		muView.setGame(game)
		muView.delegate = self
		matchupViews.insert(muView)
		return muView.center
	}
	
    @discardableResult
	func addLoserSemiRight(game: LeagueGameViewModel) -> CGPoint
	{
		let contentCenterPt = loserBackgroundLinesView?.center
		let originX = contentCenterPt!.x - MATCHUP_WIDTH/2 + 3*HORIZ_SPACING + MATCHUP_WIDTH - computeHorizontalCenterAdjLoser()
		let originY = contentCenterPt!.y - MATCHUP_HEIGHT/2 - MATCHUP_HEIGHT
		let muView = MatchupView(frame: CGRect(x: originX,y: originY,width: MATCHUP_WIDTH,height: MATCHUP_HEIGHT))
		loserBackgroundLinesView?.addSubview(muView)
		muView.setGame(game)
		muView.delegate = self
		matchupViews.insert(muView)
		
		addLoserBgLines(game: game, centerPt: muView.center)
		
		return muView.center
	}
	
    @discardableResult
	func addLoserSemiLeft(game: LeagueGameViewModel) -> CGPoint
	{
		let contentCenterPt = loserBackgroundLinesView?.center
		let originX = contentCenterPt!.x - MATCHUP_WIDTH/2 - 3*HORIZ_SPACING - MATCHUP_WIDTH - computeHorizontalCenterAdjLoser()
		let originY = contentCenterPt!.y - MATCHUP_HEIGHT/2 + MATCHUP_HEIGHT
		let muView = MatchupView(frame: CGRect(x: originX,y: originY,width: MATCHUP_WIDTH,height: MATCHUP_HEIGHT))
		loserBackgroundLinesView?.addSubview(muView)
		muView.setGame(game)
		muView.delegate = self
		matchupViews.insert(muView)
		
		addLoserBgLines(game: game, centerPt: muView.center)
		
		return muView.center
	}
	
    @discardableResult
	func addLoserQuarterLeft(game: LeagueGameViewModel) -> CGPoint
	{
		let contentCenterPt = loserBackgroundLinesView?.center
		let originX = contentCenterPt!.x - MATCHUP_WIDTH/2 + 2*3*HORIZ_SPACING + 2*MATCHUP_WIDTH - computeHorizontalCenterAdjLoser()
		let originY = contentCenterPt!.y - MATCHUP_HEIGHT/2
		let muView = MatchupView(frame: CGRect(x: originX,y: originY,width: MATCHUP_WIDTH,height: MATCHUP_HEIGHT))
		loserBackgroundLinesView?.addSubview(muView)
		muView.setGame(game)
		muView.delegate = self
		matchupViews.insert(muView)
		
		addLoserBgLines(game: game, centerPt: muView.center)
		
		return muView.center
	}
	
    @discardableResult
	func addLoserQuarterRight(game: LeagueGameViewModel) -> CGPoint
	{
		let contentCenterPt = loserBackgroundLinesView?.center
		let originX = contentCenterPt!.x - MATCHUP_WIDTH/2 - 2*3*HORIZ_SPACING - 2*MATCHUP_WIDTH - computeHorizontalCenterAdjLoser()
		
		let originY = contentCenterPt!.y - MATCHUP_HEIGHT/2
		let muView = MatchupView(frame: CGRect(x: originX,y: originY,width: MATCHUP_WIDTH,height: MATCHUP_HEIGHT))
		loserBackgroundLinesView?.addSubview(muView)
		muView.setGame(game)
		muView.delegate = self
		matchupViews.insert(muView)
		
		addLoserBgLines(game: game, centerPt: muView.center)
		
		return muView.center
	}
	
	
	func addLoserBgLines(game: LeagueGameViewModel, centerPt: CGPoint)
	{
		var homeInput = -1
		var awayInput = -1
		if(isLoserGame(gameNum: game.homeTeamSourceGame))
		{
			homeInput = game.homeTeamSourceGame!
		}
		if(isLoserGame(gameNum: game.awayTeamSourceGame))
		{
			awayInput = game.awayTeamSourceGame!
		}
		loserBackgroundLinesView?.setupGameInfo(game.bracketGame, center: centerPt, homeInput: homeInput, awayInput: awayInput)
	}

	
	func addSemiFinalLeft()
	{
		let centerOffset = computeHorizontalCenterAdj()
		var contentCenterPt = backgroundLinesView?.center
        let contentCenterPlusOffsetX = (contentCenterPt?.x)! + centerOffset
		contentCenterPt?.x = contentCenterPlusOffsetX
		let originX = contentCenterPt!.x - MATCHUP_WIDTH - 20 - MATCHUP_WIDTH
		let originY = contentCenterPt!.y - MATCHUP_HEIGHT/2
		let muView = MatchupView(frame: CGRect(x: originX,y: originY,width: MATCHUP_WIDTH,height: MATCHUP_HEIGHT))
		backgroundLinesView?.addSubview(muView)
		muView.setGame(games![1])
		muView.delegate = self
        matchupViews.insert(muView)
        
		backgroundLinesView?.drawSemiFinalLine(contentCenterPt!, endPt: muView.center)
		var homeInput = -1
		var awayInput = -1
		if(games![1].homeTeamSourceGame != nil)
		{
			homeInput = games![1].homeTeamSourceGame!
		}
		if(games![1].awayTeamSourceGame != nil)
		{
			awayInput = games![1].awayTeamSourceGame!
		}
		backgroundLinesView?.setupGameInfo(games![1].bracketGame, center: muView.center, homeInput: homeInput, awayInput: awayInput)
	}
	
	func addSemiFinalRight()
	{
	    let centerOffset = computeHorizontalCenterAdj()
		var contentCenterPt = backgroundLinesView?.center
        let contentCenterPlusOffsetX = (contentCenterPt?.x)! + centerOffset
        contentCenterPt?.x = contentCenterPlusOffsetX
		let originX = contentCenterPt!.x + MATCHUP_WIDTH + 20
		let originY = contentCenterPt!.y - MATCHUP_HEIGHT/2
		let muView = MatchupView(frame: CGRect(x: originX,y: originY,width: MATCHUP_WIDTH,height: MATCHUP_HEIGHT))
		backgroundLinesView?.addSubview(muView)
		muView.setGame(games![2])
		muView.delegate = self
        matchupViews.insert(muView)

		backgroundLinesView?.drawSemiFinalLine(contentCenterPt!, endPt: muView.center)
		var homeInput = -1
		var awayInput = -1
		if(games![2].homeTeamSourceGame != nil)
		{
			homeInput = (games![2].homeTeamSourceGame!)
		}
		if(games![2].awayTeamSourceGame != nil)
		{
			awayInput = games![2].awayTeamSourceGame!
		}
		backgroundLinesView?.setupGameInfo(games![2].bracketGame, center: muView.center, homeInput: homeInput, awayInput: awayInput)
	}
    
    func computeVerticalCenterAdj() -> CGFloat {
//        let contentCenterPt = backgroundLinesView!.center
//        let originY = contentCenterPt.y - MATCHUP_HEIGHT
        
        return 0.0
    }
	
	func addChampionshipGameView()
    {
	    let horizontalCenterOffset = computeHorizontalCenterAdj()
	    let contentCenterPt = backgroundLinesView?.center
		let originX = contentCenterPt!.x - MATCHUP_WIDTH + horizontalCenterOffset
		let originY = contentCenterPt!.y - MATCHUP_HEIGHT
        var topViewY = originY
        
 		let muView = MatchupView(frame: CGRect(x: originX,y: originY,width: MATCHUP_WIDTH*2,height: MATCHUP_HEIGHT*2))
		backgroundLinesView?.addSubview(muView)
		muView.setGame(games![0])
		muView.delegate = self
        matchupViews.insert(muView)
		
		var championshipLabelTextString = "Championship"
		var championshipLabelTextHeight = CHAMP_TEXT_HEIGHT
		
		let originXoffset = (MATCHUP_WIDTH * 2 - TOURNAMENT_LOGO_SIZE) / 2
		var loserBracketChampTextOffset: CGFloat = 0
		var orgLogoImageOffset: CGFloat = 0-TOURNAMENT_LOGO_SIZE - VERT_SPACING*2 - CHAMP_TEXT_HEIGHT // TODO: add this 5?
        
		if(!isSingleElimination)
		{
		    loserBracketChampTextOffset = 2*MATCHUP_HEIGHT + CHAMP_TEXT_HEIGHT + VERT_SPACING
			championshipLabelTextString = "Winner's Bracket Finals"  //change normal champ to winner's champ
			championshipLabelTextHeight = 2*CHAMP_TEXT_HEIGHT
			
			let yOffset = MATCHUP_HEIGHT*2 + WINNERS_BRACKET_FINAL_TO_CHAMP_GAME_VERT_SPACING
			orgLogoImageOffset = 0 - yOffset + orgLogoImageOffset
			
			//add overall champ label
			let doubleChampTextFrame = CGRect(x: originX,
			                                  y: originY - yOffset - CHAMP_TEXT_HEIGHT - VERT_SPACING,
			                                  width: MATCHUP_WIDTH*2,
			                                  height: CHAMP_TEXT_HEIGHT)
            
            topViewY = min(topViewY, doubleChampTextFrame.origin.y)
            
            let champLabel = ScaleFactorRedrawingLabel(frame: doubleChampTextFrame)
			champLabel.text = "Championship"
			champLabel.textColor = UIColor.white
			champLabel.font = UIFont(name: "TrebuchetMS-Bold", size: CHAMP_FONT_SIZE)
			champLabel.textAlignment = NSTextAlignment.center
			backgroundLinesView?.addSubview(champLabel)
            doubleChampTextLabel = champLabel
			
			//add overall champ matchup
			let overallChampView = MatchupView(frame: CGRect(x: originX,y: originY-yOffset,width: MATCHUP_WIDTH*2,height: MATCHUP_HEIGHT*2))
			backgroundLinesView?.addSubview(overallChampView)
			overallChampView.setGame(allGames![0])
			overallChampView.delegate = self
			matchupViews.insert(overallChampView)
			
			backgroundLinesView.drawSemiFinalLine(overallChampView.center, endPt: muView.center)
			
		}
		else
		{
			let image: UIImage = UIImage(named: "zortsyouthsports", in: Bundle.ZRBracketControllersBundle(), compatibleWith: nil)!
			let zortsImage = UIImageView(frame: CGRect(x: originX,
			                                           y: originY + MATCHUP_HEIGHT * 2 + ZORTS_BANNER_VERT_SPACING,
			                                           width: MATCHUP_WIDTH * 2,
			                                           height: ZORTS_BANNER_HEIGHT))
			zortsImage.image = image
			backgroundLinesView?.addSubview(zortsImage)
		}
		
		if let logo = tournamentLogo {
			let logoFrame = CGRect(x: originX+originXoffset,
			                       y: originY + orgLogoImageOffset,
			                       width: TOURNAMENT_LOGO_SIZE,
			                       height: TOURNAMENT_LOGO_SIZE)
            
            topViewY = min(topViewY, logoFrame.origin.y)
            
			let logoImageView = UIImageView(frame: logoFrame)
			logoImageView.backgroundColor = UIColor.white
			logoImageView.layer.cornerRadius = 6.0
			logoImageView.layer.shadowColor = UIColor.black.cgColor
			logoImageView.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
			logoImageView.layer.shadowOpacity = 0.8
			logoImageView.layer.shadowRadius = 3.0
			logoImageView.contentMode = UIViewContentMode.scaleAspectFit
			logo.deliverToImageView(logoImageView)
			backgroundLinesView?.addSubview(logoImageView)
		}
		
		let championshipTextFrame = CGRect(x: originX,
		                                   y: originY - CHAMP_TEXT_HEIGHT - VERT_SPACING + loserBracketChampTextOffset,
		                                   width: MATCHUP_WIDTH*2,
		                                   height: championshipLabelTextHeight)
        
        topViewY = min(topViewY, championshipTextFrame.origin.y)
        
        let champLabel = ScaleFactorRedrawingLabel(frame: championshipTextFrame)
		champLabel.text = championshipLabelTextString
		champLabel.textColor = UIColor.white
		champLabel.font = UIFont(name: "TrebuchetMS-Bold", size: CHAMP_FONT_SIZE)
		champLabel.textAlignment = NSTextAlignment.center
		champLabel.numberOfLines = 2
		champLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
		backgroundLinesView?.addSubview(champLabel)
		championshipTextLabel = champLabel
        
        if topViewY < 0 {
            bracketContentInset.top = -(topViewY) + VERT_PADDING
            bracketContentInset.bottom = topViewY + VERT_PADDING
        }
	}
	
	open func swapBracket()
	{
		if(!isSingleElimination && scrollingView == backgroundLinesView)
		{
			switchToLoserBracket()
		}
		else if(!isSingleElimination)
		{
			switchToWinnersBracket()
		}
	}
	
	func switchToLoserBracket()
	{
		scrollingView = loserBackgroundLinesView
		currentScrollView = losersScrollView
		UIView.transition(from: winnersScrollView, to: losersScrollView, duration: 0.33, options:
			UIViewAnimationOptions.transitionFlipFromBottom, completion: nil)
		zoomOutToFullLoserBracket(false)
		redrawLabels(at: losersScrollView.zoomScale)
	}
	
	func switchToWinnersBracket()
	{
		scrollingView = backgroundLinesView
		currentScrollView = winnersScrollView
		UIView.transition(from: losersScrollView, to: winnersScrollView, duration: 0.33, options:
			UIViewAnimationOptions.transitionFlipFromTop, completion: nil)
		zoomOutToFullBracket(false)
	}
	
	func addLoserRoundToView(games: [LeagueGameViewModel], startGame: Int, endGame: Int, curRoundX: CGFloat, awayOnly: Bool) -> Bool
	{
		var addedGame = false
		let halfSize = awayOnly ? 2 : 1
		let numberOfGamesInRound = ((endGame - startGame + 1) * 4) / halfSize
		let spaceBetweenGames: CGFloat = loserBracketContainerHeight/CGFloat(numberOfGamesInRound)*2
		
		var curRoundY: CGFloat = spaceBetweenGames/2 - MATCHUP_HEIGHT/2 + VERT_SPACING
		
		var adjSpacer = awayOnly ? 0 : spaceBetweenGames/4
		
		
		for i in startGame...endGame
		{
//			let awayNum = games[i].awayTeamSourceGame
//			let homeNum = games[i].homeTeamSourceGame
			
			if (isLoserGame(gameNum: games[i].awayTeamSourceGame))
			{
			    let instanceGame = getGameFromBracketGame(bracketGame: games[i].awayTeamSourceGame!)
				adjSpacer = instanceGame?.bracketRound == -1 ? 0 : adjSpacer
				let muView = MatchupView(frame: CGRect(x: curRoundX, y: curRoundY+adjSpacer, width: MATCHUP_WIDTH, height: MATCHUP_HEIGHT))
				muView.delegate = self
				
				muView.setGame(instanceGame!)
				addedGame = true
				loserBackgroundLinesView?.addSubview(muView)
				matchupViews.insert(muView)
				
				
				var homeInput = -1
				var awayInput = -1
				if(isLoserGame(gameNum: instanceGame?.homeTeamSourceGame))
				{
					homeInput = (instanceGame?.homeTeamSourceGame!)!
				}
				if(isLoserGame(gameNum: instanceGame?.awayTeamSourceGame))
				{
					awayInput = (instanceGame?.awayTeamSourceGame!)!
				}
				
				loserBackgroundLinesView?.setupGameInfo((instanceGame?.bracketGame)!, center: muView.center, homeInput: homeInput, awayInput: awayInput)

			
			}
			if (!awayOnly && isLoserGame(gameNum: games[i].homeTeamSourceGame))
			{
				let instanceGame = getGameFromBracketGame(bracketGame: games[i].homeTeamSourceGame!)
				adjSpacer = instanceGame?.bracketRound == -1 ? 0 : adjSpacer
				
			    curRoundY += spaceBetweenGames
				let muView = MatchupView(frame: CGRect(x: curRoundX, y: curRoundY-adjSpacer, width: MATCHUP_WIDTH, height: MATCHUP_HEIGHT))
				muView.delegate = self
				muView.setGame(instanceGame!)
				addedGame = true
				loserBackgroundLinesView?.addSubview(muView)
				matchupViews.insert(muView)
				
				
				var homeInput = -1
				var awayInput = -1
				if(isLoserGame(gameNum: instanceGame?.homeTeamSourceGame))
				{
					homeInput = (instanceGame?.homeTeamSourceGame!)!
				}
				if(isLoserGame(gameNum: instanceGame?.awayTeamSourceGame))
				{
					awayInput = (instanceGame?.awayTeamSourceGame!)!
				}
				loserBackgroundLinesView?.setupGameInfo((instanceGame?.bracketGame)!, center: muView.center, homeInput: homeInput, awayInput: awayInput)
				

			}
			else if(!awayOnly)
			{
				curRoundY += spaceBetweenGames
			}
			curRoundY += spaceBetweenGames
			
		}
		
		return addedGame
	}
	
	func addRoundToView(_ games: [LeagueGameViewModel], startGame: Int, endGame: Int, curRoundX: CGFloat) -> Bool
	{
		var addedGame = false
		let gamesPerSide: Int = (endGame - startGame + 1) * 2
		let spaceBetweenGames: CGFloat = bracketContainerHeight/CGFloat(gamesPerSide)
		
		var curRoundY: CGFloat = spaceBetweenGames/2 - MATCHUP_HEIGHT/2 + VERT_SPACING
		
		
		
		for i in startGame...endGame
		{
			if(games[i].awayTeamSourceGame != nil)
			{
				let muView = MatchupView(frame: CGRect(x: curRoundX,y: curRoundY,width: MATCHUP_WIDTH,height: MATCHUP_HEIGHT))
				backgroundLinesView?.addSubview(muView)
				//let gameNum = games.count - games[i].awayTeamSourceGame!
				let instanceGame = getGameFromBracketGame(bracketGame: games[i].awayTeamSourceGame!)
				muView.setGame(instanceGame!)
				muView.delegate = self

				var homeInput = -1
				var awayInput = -1
				if(instanceGame!.homeTeamSourceGame != nil)
				{
				    homeInput = (instanceGame?.homeTeamSourceGame!)!
				}
				if(instanceGame!.awayTeamSourceGame != nil)
				{
					awayInput = (instanceGame?.awayTeamSourceGame!)!
				}
				backgroundLinesView?.setupGameInfo((instanceGame?.bracketGame)!, center: muView.center, homeInput: homeInput, awayInput: awayInput)
                
                matchupViews.insert(muView)
				addedGame = true
			}
			curRoundY += spaceBetweenGames
			if(games[i].homeTeamSourceGame != nil)
			{
				let muView = MatchupView(frame: CGRect(x: curRoundX,y: curRoundY,width: MATCHUP_WIDTH,height: MATCHUP_HEIGHT))
				backgroundLinesView?.addSubview(muView)
				let instanceGame = getGameFromBracketGame(bracketGame: games[i].homeTeamSourceGame!)
				muView.setGame(instanceGame!)
				muView.delegate = self
				var homeInput = -1
				var awayInput = -1
				if(instanceGame!.homeTeamSourceGame != nil)
				{
					homeInput = (instanceGame?.homeTeamSourceGame!)!
				}
				if(instanceGame!.awayTeamSourceGame != nil)
				{
					awayInput = (instanceGame?.awayTeamSourceGame!)!
				}
				backgroundLinesView?.setupGameInfo((instanceGame?.bracketGame)!, center: muView.center, homeInput: homeInput, awayInput: awayInput)
                
                matchupViews.insert(muView)
				addedGame = true
			}
			curRoundY += spaceBetweenGames
		}
		return addedGame
	}
    
    // MARK: iOS Orientation Handling
	
    #if os(iOS)
    
        open override var shouldAutorotate : Bool {
            return false
        }

        open override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
            return UIInterfaceOrientationMask.landscape
        }
    
    #endif
    
    // MARK: Zooming
	
	open func viewForZooming(in scrollView: UIScrollView) -> UIView? {
		return scrollingView
	}
	
	func largestPower2LessThan(_ x: Int) -> Int
	{
	    let d: Double = Double(x)
	    return Int(pow(2.0,floor(log2(d))))
	}
	
	func countRoundGames(games: [LeagueGameViewModel], round: Int) -> Int
	{
	    var count = 0
		for g in games
		{
			if(g.bracketRound == round)
			{
				count += 1
			}
		}
		return count
	}
	
	
	
	open func matchupTap(_ game: LeagueGameViewModel, view: UIView)
	{
        if shouldZoom(forTappedView: view) {
            
            let zoomRectSpace: CGFloat = MATCHUP_ZOOM_PADDING
            
            let zoomRect = CGRect(x: view.frame.origin.x - zoomRectSpace,
                                  y: view.frame.origin.y - zoomRectSpace,
                                  width: view.frame.size.width + zoomRectSpace * 2,
                                  height: view.frame.size.height + zoomRectSpace * 2)
            
            currentScrollView.zoom(to: zoomRect, animated: true)
            
        } else {
            didSelectGame?(game)
        }
	}
    
    fileprivate func shouldZoom(forTappedView view: UIView) -> Bool {
        let visibleRect = currentScrollView.convert(winnersScrollView.bounds, to: scrollingView)
        let viewIsShowing = visibleRect.contains(view.frame)
        
        let scaledViewSize = CGSize(width: view.frame.size.width * currentScrollView.zoomScale,
                                    height: view.frame.size.height * currentScrollView.zoomScale)
        
        return !viewIsShowing ||
            scaledViewSize.width < MATCHUP_ZOOM_WIDTH_THRESHOLD
    }
	
	private func sortGamesWinnersLosers(unsortedGames: [LeagueGameViewModel])
	{
		self.loserGames = []
		self.games = []
		var removedChampGames = false
		for g in unsortedGames
		{
			if(g.bracketRound < 0)
			{
				loserGames?.append(g)
				removedChampGames = true
			}
			else if(removedChampGames)
			{
				self.games?.append(g)
			}
		}
	}
	
	func traverseGivenLevel(tree: BinaryTree<LeagueGameViewModel>, level: Int)
	{
		if let game = tree.getValue()
		{
		    if(level == 1)
			{
				self.games?.append(game)
			}
			else if(level > 1)
			{
				traverseGivenLevel(tree: tree.getLeft(), level: level-1)
				traverseGivenLevel(tree: tree.getRight(), level: level-1)
			}
		}
	}
	
	func newTreeFromRootGame(game: LeagueGameViewModel) -> BinaryTree<LeagueGameViewModel>
	{
		var awayNode = BinaryTree<LeagueGameViewModel>.empty
		var homeNode = BinaryTree<LeagueGameViewModel>.empty
		
		if let awayNum = game.awayTeamSourceGame
		{
			let awayGame = getGameFromBracketGame(bracketGame: awayNum)
			awayNode = newTreeFromRootGame(game: awayGame!)
		}
		
		if let homeNum = game.homeTeamSourceGame
		{
			let homeGame = getGameFromBracketGame(bracketGame: homeNum)
			homeNode = newTreeFromRootGame(game: homeGame!)
		}
		
		return BinaryTree.node(awayNode, game, homeNode)
	}
	
	func newLoserTreeFromRootGame(game: LeagueGameViewModel) -> BinaryTree<LeagueGameViewModel>
	{
		var awayNode = BinaryTree<LeagueGameViewModel>.empty
		var homeNode = BinaryTree<LeagueGameViewModel>.empty
		
		if let awayNum = game.awayTeamSourceGame
		{
			let awayGame = getGameFromBracketGame(bracketGame: awayNum)
			if(awayGame?.bracketRound < 0)
			{
				awayNode = newLoserTreeFromRootGame(game: awayGame!)
		    }
		}
		
		if let homeNum = game.homeTeamSourceGame
		{
			let homeGame = getGameFromBracketGame(bracketGame: homeNum)
			if(homeGame?.bracketRound < 0)
			{
				homeNode = newLoserTreeFromRootGame(game: homeGame!)
			}
		}
		
		return BinaryTree.node(awayNode, game, homeNode)
	}
	

	
	private func isSingleElimination(games: [LeagueGameViewModel]) -> Bool
	{
		for game in games
		{
			if(game.bracketRound < 0)
			{
				return false
			}
		}
		return true
	}
	
		
	private func isLoserGame(gameNum: Int?) -> Bool
	{
	    if gameNum != nil
		{
			for g in self.allGames!
			{
				if (g.bracketGame == gameNum)
				{
					return g.bracketRound < 0;
				}
			}
		}
        
		return false;
	}
	
	
	private func getGameFromBracketGame(bracketGame: Int) -> LeagueGameViewModel?
	{
		for  g in self.allGames!
		{
			if (g.bracketGame == bracketGame)
			{
				return g;
			}
		}
		return nil
	}
	
    /*
     *  MARK: - UIScrollViewDelegate
     */
	
    open func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        lastZoomScaleFactor = scale
    }
    
    open func scrollViewDidZoom(_ scrollView: UIScrollView) {
        let scale = scrollView.zoomScale
        redrawLabels(at: scaleFactor(forZoomScale: scale))
        updateContentInsetsForScrollViewAfterZoom(scrollView)
    }
    
    private func updateContentInsetsForScrollViewAfterZoom(_ scrollView: UIScrollView) {
        var inset = scrollView === winnersScrollView ? bracketContentInset : loserBracketContentInset
        
        inset.top *= scrollView.zoomScale
        inset.left *= scrollView.zoomScale
        inset.bottom *= scrollView.zoomScale
        inset.right *= scrollView.zoomScale
        
        scrollView.contentInset = inset;
    }
    
    private func scaleFactor(forZoomScale scale: CGFloat) -> CGFloat {
        return max(2.0, scale * 2.0)
    }
	
	fileprivate func computeHorizontalCenterAdj() -> CGFloat
	{
	    var adj: CGFloat = 0
		if(winnerRoundsOnLeft > winnerRoundsOnRight)
		{
			adj = MATCHUP_WIDTH/2
		}
		else if(winnerRoundsOnLeft < winnerRoundsOnRight)
		{
			adj = -1 * MATCHUP_WIDTH/2
		}
		return adj
	}
	
	fileprivate func computeHorizontalCenterAdjLoser() -> CGFloat
	{
//		let totalWidth = MATCHUP_WIDTH + HORIZ_SPACING
//		var retAdj: CGFloat = 0
//		let widths = Int32(floor(loserBracketContainerWidth/totalWidth))
//		
//		if(widths % 2 == 0 )
//		{
//			retAdj = totalWidth/2
//		}
//		
//		return retAdj
        
		return loserCenterAdj
	}
    
    fileprivate func redrawLabels(at scale: CGFloat, onlyVisible: Bool = true) {
        championshipTextLabel?.preferredContentScaleFactor = scale
        
        doubleChampTextLabel?.preferredContentScaleFactor = scale
        
        redrawMatchupViews(at: scale, onlyVisible: onlyVisible)
    }
    
    fileprivate func redrawMatchupViews(at visibleScale: CGFloat, onlyVisible: Bool = true) {
        for view in matchupViews {
            let scale = !onlyVisible || matchupViewIsVisible(view) ? visibleScale : 2.0
            redraw(matchupView: view, at: scale)
        }
    }
    
    fileprivate func matchupViewIsVisible(_ view: MatchupView) -> Bool {
        let visibleRect = currentScrollView.convert(currentScrollView.bounds, to: scrollingView)
        return visibleRect.intersects(view.frame)
    }
    
    fileprivate func redraw(matchupView view: MatchupView, at scale: CGFloat) {
        view.preferredContentScaleFactor = scale
    }
    
    open func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let scale = scrollView.zoomScale
        if scale == lastZoomScaleFactor {
            redrawMatchupViews(at: scaleFactor(forZoomScale: scale))
        }
    }

}
