//
//  BackgroundLinesView.swift
//  Pods
//
//  Created by Max Tower on 5/7/16.
//
//

import UIKit

class BackgroundLinesView: UIView {

	fileprivate var gameLinesInfo = [Int: GameLinesInfo]()
	fileprivate var extraLines = [Line]()
	fileprivate var bentLines = [BentLine]()
	
	
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
		let context = UIGraphicsGetCurrentContext()
		context?.setStrokeColor(UIColor.lightGray.cgColor)
		context?.setLineWidth(2.0)
		
		for (_, linesInfo) in gameLinesInfo
		{
			if(linesInfo.drawAwayInput)
			{
			    var x = gameLinesInfo[linesInfo.awayInputGame]!.centerP.x
				var y = gameLinesInfo[linesInfo.awayInputGame]!.centerP.y
				let path = UIBezierPath()
				path.move(to: CGPoint(x: x,y: y))
				x = linesInfo.centerP.x
				path.addLine(to: CGPoint(x: x, y: y))
				y = linesInfo.centerP.y
				path.addLine(to: CGPoint(x: x, y: y))
				context?.addPath(path.cgPath)
				context?.strokePath()
			}
			if(linesInfo.drawHomeInput)
			{
				var x = gameLinesInfo[linesInfo.homeInputGame]!.centerP.x
				var y = gameLinesInfo[linesInfo.homeInputGame]!.centerP.y
				let path = UIBezierPath()
				path.move(to: CGPoint(x: x,y: y))
				x = linesInfo.centerP.x
				path.addLine(to: CGPoint(x: x, y: y))
				y = linesInfo.centerP.y
				path.addLine(to: CGPoint(x: x, y: y))
				context?.addPath(path.cgPath)
				context?.strokePath()
			}
		}
		for(line) in extraLines
		{
			let path = UIBezierPath()
			path.move(to: line.pt1)
			path.addLine(to: line.pt2)
			context?.addPath(path.cgPath)
			context?.strokePath()
		}
		for(bent) in bentLines
		{
			let path = UIBezierPath()
			path.move(to: bent.pt1)
			path.addLine(to: bent.pt2)
			path.addLine(to: bent.pt3)
			context?.addPath(path.cgPath)
			context?.strokePath()
		}
	}
	
	func drawSemiFinalLine(_ startPt: CGPoint, endPt: CGPoint)
	{
		let line = Line(startPt: startPt, endPt: endPt)
		extraLines.append(line)
	}
	
	func drawTwoSegmentLine(startPt: CGPoint, midPt: CGPoint, endPt: CGPoint)
	{
		let bent = BentLine(startPt: startPt, midPt: midPt, endPt: endPt)
		bentLines.append(bent)
	}
	
	func eraseLines()
	{
		gameLinesInfo = [:]
		self.setNeedsLayout()
	}

    func setupGameInfo(_ gameNum: Int, center: CGPoint, homeInput: Int, awayInput: Int)
	{
		var newInfo = GameLinesInfo()
		newInfo.centerP = center
		newInfo.homeInputGame = homeInput
		newInfo.awayInputGame = awayInput
		newInfo.drawHomeInput = homeInput != -1
		newInfo.drawAwayInput = awayInput != -1
		gameLinesInfo[gameNum] = newInfo
	}
	
	override init(frame: CGRect)
	{
		super.init(frame: frame)
		viewSetup()
	}
	
	required init?(coder aDecoder: NSCoder)
	{
		super.init(coder: aDecoder)
		viewSetup()
	}
	
	func viewSetup()
	{
		self.contentMode = UIViewContentMode.redraw
		self.backgroundColor = UIColor.clear
		gameLinesInfo = [Int: GameLinesInfo]()
	}

    fileprivate struct GameLinesInfo
	{
		var centerP: CGPoint
		var drawHomeInput: Bool
		var drawAwayInput: Bool
		var homeInputGame: Int
		var awayInputGame: Int
		
		init()
		{
			centerP = CGPoint()
			drawAwayInput = false
			drawHomeInput = false
			homeInputGame = 0
			awayInputGame = 0
		}
	}
	
	fileprivate struct Line
	{
		var pt1: CGPoint
		var pt2: CGPoint
		init(startPt: CGPoint, endPt: CGPoint)
		{
			pt1 = startPt
			pt2 = endPt
		}
	}
	
	fileprivate struct BentLine
	{
		var pt1, pt2, pt3 : CGPoint
		init(startPt: CGPoint, midPt: CGPoint, endPt: CGPoint)
		{
			pt1 = startPt
			pt2 = midPt
			pt3 = endPt
		}
	}
}
