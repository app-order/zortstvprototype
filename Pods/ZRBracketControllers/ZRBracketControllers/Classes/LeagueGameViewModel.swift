//
//  LeagueGameViewModel.swift
//  Pods
//
//  Created by Max Tower on 5/9/16.
//
//

import Foundation

import ReactiveKit
import ImageValet

public protocol LeagueGameViewModel {
    
	var bracketGame: Int { get }   //game number from challonge bracket
	var bracketRound: Int { get }  //game round from challonge bracket
    var awayTeamSourceGame: Int? { get }   //game number where away team came from
    var homeTeamSourceGame: Int? { get }   //game number where home team came from
    
	var awayTeamName: Property<String> { get }
	var homeTeamName: Property<String> { get }
	var awayTeamScore: Property<String> { get }
	var homeTeamScore: Property<String> { get }
	var awayTeamLogo: Property<ImageValet> { get }  // not sure how you do images, but I need the URL or
	var homeTeamLogo: Property<ImageValet> { get }  // something else like ImageValet?
    var location: Property<String> { get }
    var date: Property<String> { get }
    var homeSeedNumber: Property<String> { get }
    var awaySeedNumber: Property<String> { get }

}
