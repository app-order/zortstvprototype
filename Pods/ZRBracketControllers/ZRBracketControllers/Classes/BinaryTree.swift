//
//  BinaryTree.swift
//  Pods
//
//  Created by Max Tower on 1/19/17.
//
//

import Foundation
//enum BinaryTree<T: Comparable>
enum BinaryTree<T>
{
	case empty
	indirect case node(BinaryTree, T, BinaryTree)
	
	var count: Int
	{
		switch self
		{
			case let .node(left, _, right):
				return left.count + 1 + right.count
			case .empty:
				return 0
		}
	}
	
	public func getValue() -> T?
	{
		switch self
		{
		case .empty:
			return nil
		case let .node(_, value, _):
			return value
		}
	}
	
	public func getLeft() -> BinaryTree<T>
	{
		switch self
		{
		case .empty:
			return .empty
		case let .node(left, _, _):
			return left
		}
	}
	
	
	public func getRight() -> BinaryTree<T>
	{
		switch self
		{
		case .empty:
			return .empty
		case let .node(_, _, right):
			return right
		}
	}
	
	
	var height: Int
	{
		switch self
		{
		case .empty:
			return 0
		case let .node(leftTree, _, rightTree):
			let lHeight = leftTree.height
			let rHeight = rightTree.height
			if(lHeight > rHeight)
			{
				return lHeight + 1
			}
			else
			{
				return rHeight + 1
			}
		}
	}
	
	
	private func newTreeWithInsertedValue(newValue: T) -> BinaryTree
	{
		switch self
		{
		// 1
		case .empty:
			return .node(.empty, newValue, .empty)
            
        default: return .empty
            
		// 2
//		case let .node(left, value, right):
//		if newValue < value
//			{
//				return .node(left.newTreeWithInsertedValue(newValue: newValue), value, right)
//			}
//			else
//			{
//				return .node(left, value, right.newTreeWithInsertedValue(newValue: newValue))
//			}
//        return .empty
		}
	}
	
	mutating func insert(newValue: T)
	{
        self = newTreeWithInsertedValue(newValue: newValue)
	}

}

extension BinaryTree: CustomStringConvertible
{
	var description: String {
		switch self {
		case let .node(left, value, right):
			return "value: \(value), left = [" + left.description + "], right = [" + right.description + "]"
		case .empty:
			return ""
		}
	}
}

