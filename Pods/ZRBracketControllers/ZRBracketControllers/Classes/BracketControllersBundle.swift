//
//  BracketControllersBundle.swift
//  Pods
//
//  Created by Anthony Miller on 5/5/16.
//
//

import Foundation

extension Bundle {
    
    static func ZRBracketControllersBundle() -> Bundle {
        let bundlePath = Bundle(for: BracketViewController.self).path(forResource: "ZRBracketControllers", ofType: "bundle")!
        return self.init(path: bundlePath)!
    }
    
}
