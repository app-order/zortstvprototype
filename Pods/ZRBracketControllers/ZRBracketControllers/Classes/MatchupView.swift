//
//  MatchupView.swift
//  Pods
//
//  Created by Max Tower on 5/3/16.
//
//
import UIKit
import Bond

protocol MatchupTapDelegate: class {
    func matchupTap(_ game: LeagueGameViewModel, view: UIView)
}

class MatchupView: UIView, UIGestureRecognizerDelegate {
	
    var game: LeagueGameViewModel?
    
	var awayTeamNameLabel: ScaleFactorRedrawingLabel!
	var homeTeamNameLabel: ScaleFactorRedrawingLabel!
	var awayScoreLabel: ScaleFactorRedrawingLabel!
	var homeScoreLabel: ScaleFactorRedrawingLabel!
	var gameLocationLabel: ScaleFactorRedrawingLabel!
	var gameTimeLabel: ScaleFactorRedrawingLabel!
	var homeSeedLabel: ScaleFactorRedrawingLabel!
	var awaySeedLabel: ScaleFactorRedrawingLabel!
    var awayIconImageView: UIImageView!
    var homeIconImageView: UIImageView!

    weak var delegate: MatchupTapDelegate?
    
    var preferredContentScaleFactor: CGFloat? {
        didSet {
            updateContentScaleFactor()
        }
    }
    
    func updateContentScaleFactor() {
        if let scale = preferredContentScaleFactor {
            for label in subviews where label is ScaleFactorRedrawingLabel {
                (label as? ScaleFactorRedrawingLabel)?.preferredContentScaleFactor = scale
            }
        }
    }
	
	func setGame(_ game: LeagueGameViewModel)
	{
		self.game = game
        game.awayTeamName.bind(to: awayTeamNameLabel).dispose(in: reactive.bag)
		game.homeTeamName.bind(to: homeTeamNameLabel).dispose(in: reactive.bag)
		game.awayTeamScore.bind(to: awayScoreLabel).dispose(in: reactive.bag)
		game.homeTeamScore.bind(to: homeScoreLabel).dispose(in: reactive.bag)
		
		game.awayTeamLogo.observeNext { [weak self] imageValet in
            if let awayIcon = self?.awayIconImageView {
                imageValet.deliverToImageView(awayIcon)
            }
        }.dispose(in: reactive.bag)
		
		game.homeTeamLogo.observeNext { [weak self] imageValet in
            if let homeIcon = self?.homeIconImageView {
                imageValet.deliverToImageView(homeIcon)
            }
			
			}.dispose(in: reactive.bag)
        
		game.date.bind(to: gameTimeLabel).dispose(in: reactive.bag)
		game.location.bind(to: gameLocationLabel).dispose(in: reactive.bag)
		game.homeSeedNumber.bind(to: homeSeedLabel).dispose(in: reactive.bag)
		game.awaySeedNumber.bind(to: awaySeedLabel).dispose(in: reactive.bag)
		
	}
	
	@objc func handleTap(_ sender: UIPanGestureRecognizer)
	{
	    delegate?.matchupTap(game!, view: self)
	}
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
	
	func setupView() {
		let recognizer = UITapGestureRecognizer(target: self, action:#selector(MatchupView.handleTap(_:)))
		recognizer.delegate = self
		self.addGestureRecognizer(recognizer)
	
		let height: CGFloat = self.bounds.height
		let width: CGFloat = self.bounds.width
		let teamFontSize = width/13.333
		let infoFontSize = height/11
		let footer: CGFloat = height/9.166
		let border: CGFloat = width/50
		let homeY: CGFloat = (height-footer)/2 + border/2
		let rowHeight: CGFloat = (height-footer)/2 - border - border/2
		let imageWidth: CGFloat = border + rowHeight + border
		let scoreWidth: CGFloat = teamFontSize * 1.5
		let seedWidth: CGFloat = teamFontSize * 1.5
		let nameWidth: CGFloat = width - imageWidth - border*3 - scoreWidth - seedWidth
		let scoreX: CGFloat = width - border - scoreWidth
				
		self.backgroundColor = UIColor.white
		// border radius
		self.layer.cornerRadius = 2.0
		
		// drop shadow
		self.layer.shadowColor = UIColor.black.cgColor
		self.layer.shadowOpacity = 0.8
		self.layer.shadowRadius = 3.0
		self.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
		
		awayIconImageView = UIImageView(frame: CGRect(x: border, y: border, width: rowHeight, height: rowHeight))
        awayIconImageView.layer.masksToBounds = true
		awayIconImageView.backgroundColor = UIColor.black
		awayIconImageView.layer.cornerRadius = 2.0
		self.addSubview(awayIconImageView)
		
		homeIconImageView = UIImageView(frame: CGRect(x: border, y: homeY, width: rowHeight, height: rowHeight))
        homeIconImageView.layer.masksToBounds = true
		homeIconImageView.backgroundColor = UIColor.black
		homeIconImageView.layer.cornerRadius = 2.0
		self.addSubview(homeIconImageView)
		
		awaySeedLabel = ScaleFactorRedrawingLabel(frame: CGRect(x: imageWidth, y: border, width: seedWidth, height: rowHeight))
        awaySeedLabel.textColor = UIColor.black
		awaySeedLabel.text = "11"
		awaySeedLabel.font = awaySeedLabel.font.withSize(teamFontSize)
		self.addSubview(awaySeedLabel)
		
		homeSeedLabel = ScaleFactorRedrawingLabel(frame: CGRect(x: imageWidth, y: homeY, width: seedWidth, height: rowHeight))
        homeSeedLabel.textColor = UIColor.black
		homeSeedLabel.text = "11"
		homeSeedLabel.font = homeSeedLabel.font.withSize(teamFontSize)
		self.addSubview(homeSeedLabel)
		
		
		awayTeamNameLabel = ScaleFactorRedrawingLabel(frame: CGRect(x: imageWidth+seedWidth, y: border, width: nameWidth, height: rowHeight))
        awayTeamNameLabel.textColor = UIColor.black
		awayTeamNameLabel.text = "Seahawks"
		awayTeamNameLabel.numberOfLines = 2
		awayTeamNameLabel.font = awayTeamNameLabel.font.withSize(teamFontSize)
		
		self.addSubview(awayTeamNameLabel)
		
		homeTeamNameLabel = ScaleFactorRedrawingLabel(frame: CGRect(x: imageWidth+seedWidth, y: homeY, width: nameWidth, height: rowHeight))
        homeTeamNameLabel.textColor = UIColor.black
		homeTeamNameLabel.text = "Panthers"
		homeTeamNameLabel.numberOfLines = 2
		homeTeamNameLabel.font = homeTeamNameLabel.font.withSize(teamFontSize)
		self.addSubview(homeTeamNameLabel)
		
		awayScoreLabel = ScaleFactorRedrawingLabel(frame: CGRect(x: scoreX, y: border,width: scoreWidth,height: rowHeight))
        awayScoreLabel.textColor = UIColor.black
		awayScoreLabel.text = "10"
		awayScoreLabel.font = awayScoreLabel.font.withSize(teamFontSize)
		self.addSubview(awayScoreLabel)
		
		homeScoreLabel = ScaleFactorRedrawingLabel(frame: CGRect(x: scoreX, y: homeY,width: scoreWidth,height: rowHeight))
        homeScoreLabel.textColor = UIColor.black
		homeScoreLabel.text = "17"
		homeScoreLabel.font = homeScoreLabel.font.withSize(teamFontSize)
		self.addSubview(homeScoreLabel)
		
		let timeWidth: CGFloat = width-border*2
		let originX = border

		gameTimeLabel = ScaleFactorRedrawingLabel(frame: CGRect(x: originX, y: height-infoFontSize-border, width: timeWidth, height: infoFontSize))
		gameTimeLabel.text = "xx:xx:xx June 4th"
		gameTimeLabel.font = gameTimeLabel.font.withSize(infoFontSize)
		gameTimeLabel.textColor = UIColor.darkGray
		gameTimeLabel.textAlignment = .left
		self.addSubview(gameTimeLabel)
		
		gameLocationLabel = ScaleFactorRedrawingLabel(frame: CGRect(x: originX, y: height-infoFontSize-border, width: timeWidth, height: infoFontSize))
		gameLocationLabel.font = gameLocationLabel.font.withSize(infoFontSize)
		gameLocationLabel.textColor = UIColor.darkGray
		gameLocationLabel.textAlignment = .right
		self.addSubview(gameLocationLabel)
		
	}
	
}
