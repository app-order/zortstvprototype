//
//  ScaleFactorRedrawingLabel.swift
//
//  Created by Anthony Miller on 2/24/17.
//  Copyright (c) 2017 App-Order, LLC. All rights reserved.
//

import UIKit

class ScaleFactorRedrawingLabel: UILabel {
    
    var preferredContentScaleFactor: CGFloat? {
        didSet {
            updateContentScaleFactor()
        }
    }
    
    func updateContentScaleFactor() {
        if let scale = preferredContentScaleFactor, contentScaleFactor != scale {
            contentScaleFactor = scale
            setNeedsDisplay()
        }
    }
    
    override func didMoveToWindow() {
        super.didMoveToWindow()
        
        updateContentScaleFactor()
    }
    
}
