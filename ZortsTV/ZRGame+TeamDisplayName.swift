//
//  ZRGame+TeamDisplayName.swift
//  Zorts
//
//  Created by Dominic Miller on 1/30/18.
//  Copyright © 2018 App-Order. All rights reserved.
//

import Foundation

import ZRCore

extension ZRGame {
    
    func awayTeamDisplayName() -> String {
        return teamName(for: awayTeamInfo, and: bracketData?.awayTeamPlacement)
    }
    
    func homeTeamDisplayName() -> String {
        return teamName(for: homeTeamInfo, and: bracketData?.homeTeamPlacement)
    }
    
    fileprivate func teamName(for teamInfo: ZRGame.TeamInfo?, and teamPlacement: ZRGameBracketTeamPlacement?) -> String {
        return displayNameIsNotEmpty(for: teamInfo) ? teamInfo!.displayName : teamPlacement?.displayName ?? "TBD"
    }
    
    fileprivate func displayNameIsNotEmpty(for teamInfo: ZRGame.TeamInfo?) -> Bool {
        return teamInfo?.displayName != nil && !teamInfo!.displayName.isEmpty
    }
    
}
