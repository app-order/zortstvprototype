//
//  LoginViewController.swift
//  ZortsTV
//
//  Created by Anthony Miller on 2/21/17.
//  Copyright © 2017 App-Order, LLC. All rights reserved.
//

import UIKit

import ZRCore
import MBProgressHUD

class LoginViewController: UIViewController {

    /*
     *  MARK: - Instance Properties
     */
    
    let authenticationManager = AuthenticationManager()
    
    var didLogin: (() -> Void)?
    
    // MARK: IBOutlets

    @IBOutlet var usernameTextField: UITextField!

    @IBOutlet var passwordTextField: UITextField!
    
    // MARK: IBActions
    
    @IBAction func submitButtonPressed(_ sender: Any) {
        MBProgressHUD.showAdded(to: view, animated: true)
        
        authenticationManager.signIn(userId: usernameTextField.text ?? "",
                                     password: passwordTextField.text ?? "",
                                     controller: self) { [unowned self] error in
                                        if let error = error {
                                            MBProgressHUD.hide(for: self.view, animated: true)
                                            let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                                            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                                            self.present(alert, animated: true, completion: nil)
                                            
                                        } else {
//                                            self.didLogin?()
                                            
                                        }
                                        
        }
    }
    
    public func setTextFieldAuthenticationValues(_ username: String? = nil, password: String? = nil) {
        usernameTextField.text = username
        passwordTextField.text = password
    }
    
}

