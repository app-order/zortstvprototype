//
//  BracketGameViewModelFromZRLeagueGame.swift
//  Zorts
//
//  Created by Anthony Miller on 5/16/16.
//  Copyright © 2016 App-Order. All rights reserved.
//

import Foundation

import Bond
import CrashlyticsRecorder
import ImageValet
import ZRQueryGenie
import ReactiveKit
import RealmSwift

import ZRBracketControllers
import ZRCore
import ZRExtensions

class BracketGameViewModelFromZRLeagueGame: LeagueGameViewModel {
    
    /*
     *   MARK: - Instance Properties
     */
    
    lazy var dateFormatter: DateFormatter = {
        return DateFormatter()
    }()
    
    let game: ZRGame
    
    private var homeTeam: ZRTeam? {
        willSet {
            homeTeamChangeToken?.invalidate()
        }
        didSet {
            self.bindHomeTeam()
        }
    }
    
    private var awayTeam: ZRTeam? {
        willSet {
            awayTeamChangeToken?.invalidate()
        }
        didSet {
            self.bindAwayTeam()
        }
    }
    
    private var gameChangeToken: NotificationToken?
    private var homeTeamChangeToken: NotificationToken?
    private var awayTeamChangeToken: NotificationToken?
    private var baseObjectChangeToken: NotificationToken?
    
    let bindingDisposeBag = DisposeBag()
    
    /*
     *   MARK: - League Game View Model Properties
     */
    
    let bracketGame: Int
    let bracketRound: Int
    let awayTeamSourceGame: Int?
    let homeTeamSourceGame: Int?
    let awayTeamName: ReactiveKit.Property<String>
    let homeTeamName: ReactiveKit.Property<String>
    let awayTeamScore: ReactiveKit.Property<String>
    let homeTeamScore: ReactiveKit.Property<String>
    let awayTeamLogo: ReactiveKit.Property<ImageValet>
    let homeTeamLogo: ReactiveKit.Property<ImageValet>
    let location: ReactiveKit.Property<String>
    let date: ReactiveKit.Property<String>
    let awaySeedNumber = ReactiveKit.Property<String>("")
    let homeSeedNumber = ReactiveKit.Property<String>("")
    
    /*
     *   MARK: - Object Life Cycle
     */
    
    init?(game: ZRGame) {
        guard let bracketData = game.bracketData,
            let gameIdentifier = Int(bracketData.gameIdentifier) else { return nil }
        
        self.game = game
        
        self.bracketGame = gameIdentifier
        self.bracketRound = bracketData.round
        
        switch bracketData.homeTeamPlacement {
        case .game(let gameId, _): homeTeamSourceGame = Int(gameId)
        default: homeTeamSourceGame = nil
        }
        
        switch bracketData.awayTeamPlacement {
        case .game(let gameId, _): awayTeamSourceGame = Int(gameId)
        default: awayTeamSourceGame = nil
        }
        
        self.location = Property(game.site?.title ?? "")
        
        func scoreString(_ score: Int16?) -> String {
            guard let score = score else { return "" }
            return String(score)
        }
        
        self.awayTeamName = Property(game.awayTeamDisplayName())
        self.homeTeamName = Property(game.homeTeamDisplayName())
        
        self.awayTeamScore = Property(scoreString(game.awayScore.value))
        self.homeTeamScore = Property(scoreString(game.homeScore.value))
        
        self.awayTeamLogo = Property(
            ImageValet(imageModel: game.awayTeamInfo?.team?.owner?.icon))
        
        self.homeTeamLogo = Property(
            ImageValet(imageModel: game.homeTeamInfo?.team?.owner?.icon))
        
        self.date = Property("")
        self.date.value = self.startDateString(from: game.startDate)
        
        self.awayTeam = game.awayTeamInfo?.team
        self.homeTeam = game.homeTeamInfo?.team
        
        if let homeSeedValue = homeTeam?.overallStanding?.rank {
            homeSeedNumber.value = String(homeSeedValue)
        }
        
        if let awaySeedValue = awayTeam?.overallStanding?.rank {
            awaySeedNumber.value = String(awaySeedValue)
        }
        
        bindLeagueGameProperties()
    }
    
    private func bindLeagueGameProperties() {
        bindTo(game.changeSignal())
        bindTo(game.baseScheduleItem.baseObject.changeSignal())
    }
    
    private func bindTo(_ changes: Signal<[PropertyChange], NSError>) {
        changes
            .observe { [weak self] event in
                switch event {
                case .next(let properties):
                    for property in properties {
                        self?.updateValue(for: property)
                    }
                    
                case .completed: break
                    
                case .failed(let error):
                    CrashlyticsRecorder.sharedInstance?.recordError(error)
                }
            }
            .dispose(in: bindingDisposeBag)
    }
    
    private func updateValue(for propertyChange: PropertyChange) {
        switch propertyChange.name {
        case ZRGame.awayScore.___name:
            guard let awayScore = propertyChange.newValue as? NSNumber else { awayTeamScore.value = ""; return }
            awayTeamScore.value = awayScore.stringValue
            
        case ZRGame.homeScore.___name:
            guard let homeScore = propertyChange.newValue as? NSNumber else { homeTeamScore.value = ""; return }
            homeTeamScore.value = homeScore.stringValue
            
        case ZRGame.site.___name:
            guard let site = propertyChange.newValue as? ZREventSite else { location.value = ""; return }
            location.value = site.title
            
        case ZRGame.homeTeam_id.___name, ZRGame.homeTeam_name.___name:
            let homeTeamInfo = game.homeTeamInfo
            homeTeamName.value = game.homeTeamDisplayName()
            
            homeTeam = homeTeamInfo?.team
            
        case ZRGame.awayTeam_id.___name, ZRGame.awayTeam_name.___name:
            let awayTeamInfo = game.awayTeamInfo
            awayTeamName.value = game.awayTeamDisplayName()
            
            awayTeam = awayTeamInfo?.team
            
        case ZRAbstractScheduleObject.startDate.___name:
            guard let newStartDate = propertyChange.newValue as? Date else { self.date.value = ""; return }
            self.date.value = self.startDateString(from: newStartDate)
            
        default: break
        }
    }
    
    // MARK: - Team Bindings
    
    private func bindHomeTeam() {
        homeTeamChangeToken = homeTeam?.observe { [weak self] change in
            guard let _self = self else { return }
            
            switch change {
            case .change(let properties):
                for property in properties {
                    _self.set(teamProperty: property, isHomeTeam: true)
                    
                }
                
            case .deleted: break
                
            case .error(let error):
                CrashlyticsRecorder.sharedInstance?.recordError(error)
            }
        }
    }
    
    private func set(teamProperty property: PropertyChange, isHomeTeam: Bool) {
        switch property.name {
        case ZRTeam.overallStandingData.___name:
            if let standingData = property.newValue as? Data,
                let standing = NSKeyedUnarchiver.unarchiveObject(with: standingData) as? ZRStanding {
                let seedNumber = String(standing.rank)
                
                if isHomeTeam {
                    homeSeedNumber.value = seedNumber
                } else {
                    awaySeedNumber.value = seedNumber
                }
            }
            
        default: break
        }
    }
    
    private func bindAwayTeam() {
        awayTeamChangeToken = awayTeam?.observe { [weak self] change in
            guard let _self = self else { return }
            
            switch change {
            case .change(let properties):
                for property in properties {
                    _self.set(teamProperty: property, isHomeTeam: false)
                    
                }
                
            case .deleted: break
                
            case .error(let error):
                CrashlyticsRecorder.sharedInstance?.recordError(error)
            }
        }
    }
    
    private func startDateString(from date: Date) -> String {
        var displayTimeZone: Bool = false
        
        let timeZone = game.season.timeZone
        dateFormatter.timeZone = timeZone
        displayTimeZone = timeZone == TimeZone.autoupdatingCurrent
        
        var dateFormat = game.timeIsTBA ? "M/d" : "M/d h:mm a"
        if displayTimeZone { dateFormat += " z" }
        dateFormatter.dateFormat = dateFormat
        
        return dateFormatter.string(from: date)
    }
    
}
