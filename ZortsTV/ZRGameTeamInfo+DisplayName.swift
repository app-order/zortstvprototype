//
//  ZRGameTeamInfo+DisplayName.swift
//  Zorts
//
//  Created by Anthony Miller on 7/29/17.
//  Copyright © 2017 App-Order. All rights reserved.
//

import Foundation

import ZRCore

extension ZRGame.TeamInfo {
    
    var displayName: String {
        switch self {
        case let .name(name): return name
        case let .team(team): return team.clubDisplayName
        }
    }
    
}
