//
//  SettingsController.swift
//
//  Created by Anthony Miller on 2/21/17.
//  Copyright (c) 2017 App-Order, LLC. All rights reserved.
//

import UIKit

class SettingsViewController: UITableViewController {
    
    @IBOutlet var logoutCell: UITableViewCell!
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) {
            switch cell {
            case logoutCell: logoutCellPressed()
            default: return
            }
        }
    }
    
    func logoutCellPressed() {
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            _ = appDelegate.logout()
        }
    }
    
}
