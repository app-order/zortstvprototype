//
//  MyTeamsListCollectionViewController.swift
//
//  Created by Anthony Miller on 2/21/17.
//  Copyright (c) 2017 App-Order, LLC. All rights reserved.
//

import UIKit

import ImageValet
import RealmSwift

import ZRBracketControllers
import ZRCore

class MyTeamsListCollectionViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    // TODO: Add view for past MyTeams.
    
    /*
     *  MARK: - Instance Properties
     */
    
    let realm = try! Realm()
    
//    /// The predicate to filter the controller's results by.
//    public var filterPredicate: NSPredicate? {
//        didSet {
//            self.resultsController.delegate = nil
//
//            let newResultsController = MyTeamsListResultsController(filterPredicate: filterPredicate, realm: self.realm)
//            newResultsController.delegate = self
//
//            self.resultsController = newResultsController
//        }
//    }
    
    /// The `MyTeamListResultsController` that will fetch and update the objects for the list.
    public internal(set) lazy var resultsController: MyTeamsListResultsController = {
        let controller = MyTeamsListResultsController(//filterPredicate: self.filterPredicate,
                                                      realm: self.realm)
        controller.delegate = self
        return controller
    }()
    
    internal private(set) lazy var seasonsListController: SeasonsListCollectionViewController = {
        let controller = storyboard?.instantiateViewController(withIdentifier: "SeasonsList") as! SeasonsListCollectionViewController
        return controller
    }()
    
    // MARK: IBOutlets
    
    @IBOutlet var collectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource = self
        collectionView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        collectionView.reloadData()
    }
    
}

/*
 *  MARK: - UICollectionViewDataSource
 */

extension MyTeamsListCollectionViewController {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return resultsController.numberOfSections()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return resultsController.numberOfObjects(inSection: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! MyTeamCollectionViewCell
        
        configure(myTeamCell: cell, indexPath: indexPath)
        
        return cell
    }
    
    private func configure(myTeamCell cell: MyTeamCollectionViewCell, indexPath: IndexPath) {
        let myTeam = self.myTeam(for: indexPath)
        
        cell.nameLabel.text = myTeam.name.uppercased()
        cell.infoLabel.text = myTeam.typeDescription
        ImageValet(imageModel: myTeam.icon).deliverToImageView(cell.imageView)
    }
    
    func myTeam(for indexPath: IndexPath) -> ZRMyTeamStub {
        return resultsController.myTeam(for: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView {
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                         withReuseIdentifier: "Header",
                                                                         for: indexPath) as! MyTeamCollectionViewSectionHeader
        headerView.titleLabel.text = titleForHeader(inSection: indexPath.section)
        return headerView
    }
    
    private func titleForHeader(inSection section: Int) -> String? {
        if self.collectionView(collectionView, numberOfItemsInSection: section) == 0 { return nil }
        return resultsController.title(forSection: section)
    }
    
}

extension MyTeamsListCollectionViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        let height = self.collectionView(collectionView, numberOfItemsInSection: section) == 0 ? 0 : 100
        return CGSize(width: 500, height: height)
    }
    
}

/*
 *  MARK: - UICollectionViewDelegate
 */

extension MyTeamsListCollectionViewController {
    
    func collectionView(_ collectionView: UICollectionView, canFocusItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let myTeamStub = self.myTeam(for: indexPath)
        if myTeamStub.role == .creating { return }
        
        if myTeamStub.organizationStub.seasons.count == 1 {
            let myTeamId = myTeamStub.identifier
            let seasonId = myTeamStub.organizationStub.seasons.first!.identifier
            
            UserDataSyncManager.shared.syncSingleMyTeam(for: myTeamStub, seasonId: seasonId)
                .then { sync -> SyncMyTeamOperation.ExecutionResult in
                    let myTeamRealm = try Realm(myTeamId: myTeamId, seasonId: seasonId)
                    myTeamRealm.refresh()
                    
                    guard let myTeam = myTeamRealm.objects(ZRMyTeam.self).first else {
                        print("MyTeam Not Found")
//                    let alert = UIAlertController(title: "MyTeam Not Found", message: nil, preferredStyle: .alert)
//                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
//                    navController.present(alert, animated: true, completion: nil)
                        return sync
                    }
                    guard let myTeamVC = self.storyboard?.instantiateViewController(withIdentifier: "MyTeam") as? MyTeamViewController else { return sync } // TODO: Throw error here
                    myTeamVC.myTeam = myTeam
                    
                    self.present(myTeamVC, animated: true, completion: nil)
                    return sync
            }
            
        } else {
            seasonsListController.myTeamStub = myTeamStub
            self.present(seasonsListController, animated: true, completion: nil)
        }
    }
    
}

/*
 *  MARK: - MyTeamsListResultsControllerDelegate
 */

extension MyTeamsListCollectionViewController: MyTeamsListResultsControllerDelegate {

    open func controllerDidChangeContent(_ controller: MyTeamsListResultsController) {
        collectionView?.reloadData()
    }
}

