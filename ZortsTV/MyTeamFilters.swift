//
//  MyTeamFilters.swift
//  Zorts
//
//  Created by Anthony Miller on 6/14/17.
//  Copyright © 2017 App-Order. All rights reserved.
//

import Foundation

import CrashlyticsRecorder
import RealmSwift

import ZRCore

/*
 *  MARK: - MyTeamFilter
 */

/// Describes a set of entities that compose a filter that can filter collections of `ManagedMyTeamFilterable` or
/// `UnmanagedMyTeamFilterable` entities.
public struct MyTeamFilter: Equatable {
    
    /// The subset of filters that describe the type of organization to filter objects by.
    public let organizationFilter: OrganizationFilter
    
    /// The identifier of the `ZREventSite` to filter objects by.
    public let eventSiteId: String?
    
    /// The designated initializer
    ///
    /// - Parameters:
    ///   - organizationFilter: An `OrganizationFilter`
    ///   - eventSiteId: The identifier of the event site for the filter.
    public init(organizationFilter: OrganizationFilter, eventSiteId: String? = nil) {
        self.organizationFilter = organizationFilter
        self.eventSiteId = eventSiteId
    }
    
    /// A convenience initializer for initializing with only an `OrganizationFilter`.
    ///
    /// - Parameter organizationFilter: An `OrganizationFilter`
    public init(_ organizationFilter: OrganizationFilter) {
        self.init(organizationFilter: organizationFilter, eventSiteId: nil)
    }
    
    /// Initializes a filter for a team identifier.
    ///
    /// - Parameters:
    ///   - teamId: The identifier of the team for the filter.
    ///   - eventSiteId: The identifier of the event site for the filter.
    public init(teamId: String, eventSiteId: String? = nil) {
        self.organizationFilter = .byTeam(teamId: teamId)
        self.eventSiteId = eventSiteId
    }
    
    /// Initializes a filter with a division identifier
    ///
    /// - Parameters:
    ///   - clubId: The identifier of the club for the filter.
    ///   - divisionId: The identifier of the division for the filter.
    ///   - subdivisionId: The identifier of the subdivision for the filter.
    ///   - eventSiteId: The identifier of the event site for the filter.
    public init(clubId: String? = nil, divisionId: String, subdivisionId: String? = nil, eventSiteId: String? = nil) {
        self.init(organizationFilter: .granular(filters: OrganizationGranularFilters(clubId: clubId,
                                                                                     divisionId: divisionId,
                                                                                     subdivisionId: subdivisionId)),
                  eventSiteId: eventSiteId)
    }
    
    /// Initializes a filter with a sport and gender instead of a division.
    ///
    /// - Parameters:
    ///   - clubId: The identifier of the club for the filter.
    ///   - sport: The sport for the filter.
    ///   - gender: The gender for the filter.
    ///   - subdivisionId: The identifier of the subdivision for the filter.
    ///   - eventSiteId: The identifier of the event site for the filter.
    public init(clubId: String? = nil,
                sport: ZRSport? = nil,
                gender: ZRGender? = nil,
                subdivisionId: String? = nil,
                eventSiteId: String? = nil) {
        self.init(organizationFilter: .granular(filters: OrganizationGranularFilters(clubId: clubId,
                                                                                     sport: sport,
                                                                                     gender: gender,
                                                                                     subdivisionId: subdivisionId)),
                  eventSiteId: eventSiteId)
    }
    
    // MARK: Object Retrieval
    
    func eventSite(in realm: Realm) throws -> ZREventSite? {
        guard let eventSiteId = eventSiteId else { return nil }
        guard let eventSite = realm.object(ofType: ZREventSite.self, forPrimaryKey: eventSiteId) else {
            throw FilterObjectNotFoundError(type: ZREventSite.self, identifier: eventSiteId)
        }
        
        return eventSite
    }
    
    /// Returns `true` if none of the filters have any values.
    public var isEmpty: Bool {
        return eventSiteId == nil &&
            organizationFilter.isEmpty
    }
    
    // MARK: Convenience Accessors
    
    public var clubId: String? {
        return organizationFilter.clubId
    }
    
    public var divisionId: String? {
        return organizationFilter.divisionId
    }
    
    public var subdivisionId: String? {
        return organizationFilter.subdivisionId
    }
    
    public var gender: ZRGender? {
        return organizationFilter.gender
    }
    
    public var sport: ZRSport? {
        return organizationFilter.sport
    }
    
}

/// A filter to use for filtering objects for a myTeam based upon their organization
///
/// - byTeam:   Filter by a single `ZRTeam` object
/// - granular: Filter by a set of granular filters.
public enum OrganizationFilter: Equatable {
    case byTeam(teamId: String),
    granular(filters: OrganizationGranularFilters)
    
    /// Returns `true` if none of the filters have any values.
    public var isEmpty: Bool {
        switch self {
        case .byTeam: return false
        case .granular(let filters): return filters.isEmpty
        }
    }
    
    // MARK: Convenience Accessors
    
    private var divisionFilter: DivisionFilter? {
        guard case let .granular(filters) = self else { return nil }
        return filters.divisionFilter
    }
    
    public var teamId: String? {
        guard case let .byTeam(teamId) = self else { return nil }
        return teamId
    }
    
    public var clubId: String? {
        guard case let .granular(filters) = self else { return nil }
        return filters.clubId
    }
    
    public var divisionId: String? {
        return divisionFilter?.divisionId
    }
    
    public var subdivisionId: String? {
        guard case let .granular(filters) = self else { return nil }
        return filters.subdivisionId
    }
    
    public var gender: ZRGender? {
        return divisionFilter?.gender
    }
    
    public var sport: ZRSport? {
        return divisionFilter?.sport
    }
    
    // MARK: Object Retrieval
    
    func team(in realm: Realm) throws -> ZRTeam? {
        guard let teamId = self.teamId else { return nil }
        guard let team = realm.object(ofType: ZRTeam.self, forPrimaryKey: teamId) else {
            throw FilterObjectNotFoundError(type: ZRTeam.self, identifier: teamId)
        }
        
        return team
    }
    
    func club(in realm: Realm) throws -> ZRClub? {
        guard case let .granular(filters) = self else { return nil }
        return try filters.club(in: realm)
    }
    
    func division(in realm: Realm) throws -> ZRDivision? {
        return try self.divisionFilter?.division(in: realm)
    }
    
    func subdivision(in realm: Realm) throws -> ZRSubdivision? {
        guard case let .granular(filters) = self else { return nil }
        return try filters.subdivision(in: realm)
    }
    
}

/// The subset of filters that describe the type of organization to filter objects by.
public struct OrganizationGranularFilters: Equatable {
    
    /// The identifier of the club for the filter.
    public let clubId: String?
    
    /// The subset of filters that describe the type of division to filter objects by.
    public let divisionFilter: DivisionFilter
    
    /// The identifier of the subdivision for the filter.
    public let subdivisionId: String?
    
    /// The designated initializer
    ///
    /// - Parameters:
    ///   - clubId: The identifier of the club for the filter.
    ///   - divisionFilter: The `DivisionFilter` for the filter.
    ///   - subdivisionId: The identifier of the subdivision for the filter.
    public init(clubId: String?, divisionFilter: DivisionFilter, subdivisionId: String?) {
        self.clubId = clubId
        self.divisionFilter = divisionFilter
        self.subdivisionId = subdivisionId
    }
    
    /// Initializes a filter with a division identifier
    ///
    /// - Parameters:
    ///   - clubId: The identifier of the club for the filter.
    ///   - divisionId: The identifier of the division for the filter.
    ///   - subdivisionId: The identifier of the subdivision for the filter.
    public init(clubId: String? = nil, divisionId: String, subdivisionId: String? = nil) {
        self.clubId = clubId
        self.divisionFilter = .byDivision(divisionId: divisionId)
        self.subdivisionId = subdivisionId
    }
    
    /// Initializes a filter with a sport and gender instead of a division.
    ///
    /// - Parameters:
    ///   - clubId: The identifier of the club for the filter.
    ///   - sport: The sport for the filter.
    ///   - gender: The gender for the filter.
    ///   - subdivisionId: The identifier of the subdivision for the filter.
    public init(clubId: String? = nil, sport: ZRSport? = nil, gender: ZRGender? = nil, subdivisionId: String? = nil) {
        self.clubId = clubId
        self.divisionFilter = .granular(sport: sport, gender: gender)
        self.subdivisionId = subdivisionId
    }
    
    /// Converts a `OrganizationGranularFilterObjects` into a `OrganizationGranularFilters`.
    ///
    /// - Parameter filters: The `OrganizationGranularFilterObjects` to convert.
    init(_ filters: OrganizationGranularFilterObjects) {
        self.init(clubId: filters.club?.identifier,
                  divisionFilter: DivisionFilter(filters.divisionFilter),
                  subdivisionId: filters.subdivision?.identifier)
    }
    
    /// Returns `true` if none of the filters have any values.
    public var isEmpty: Bool {
        return clubId == nil &&
            divisionFilter.isEmpty &&
            subdivisionId == nil
    }
    
    // MARK: Convenience Accessors
    
    public var divisionId: String? {
        return divisionFilter.divisionId
    }
    
    public var gender: ZRGender? {
        return divisionFilter.gender
    }
    
    public var sport: ZRSport? {
        return divisionFilter.sport
    }
    
    // MARK: Object Retrieval
    
    func club(in realm: Realm) throws -> ZRClub? {
        guard let clubId = clubId else { return nil }
        guard let club = realm.object(ofType: ZRClub.self, forPrimaryKey: clubId) else {
            throw FilterObjectNotFoundError(type: ZRClub.self, identifier: clubId)
        }
        
        return club
    }
    
    func subdivision(in realm: Realm) throws -> ZRSubdivision? {
        guard let subdivisionId = subdivisionId else { return nil }
        guard let subdivision = realm.object(ofType: ZRSubdivision.self, forPrimaryKey: subdivisionId) else {
            throw FilterObjectNotFoundError(type: ZRSubdivision.self, identifier: subdivisionId)
        }
        return subdivision
    }
    
}

/// The subset of filters that describe the type of organization to filter objects by using actual Realm `Object`
/// instances, rather than object identifiers.
///
/// - Note: This is used to resolve the objects for an `OrganizationGranularFilter` before filtering for
///         an `UnmanagedMyTeamFilterable` entity.
struct OrganizationGranularFilterObjects {
    
    /// The club for the filter.
    let club: ZRClub?
    
    /// The subset of filters that describe the type of division to filter objects by.
    let divisionFilter: DivisionFilterObjects
    
    /// the subdivision for the filter.
    let subdivision: ZRSubdivision?
    
    /// The designated initializer
    ///
    /// - Parameters:
    ///   - club: The club for the filters.
    ///   - divisionFilter: The `DivisionFilterObjects` for the filters.
    ///   - subdivision: subdivision for the filters.
    init(club: ZRClub?, divisionFilter: DivisionFilterObjects, subdivision: ZRSubdivision?) {
        self.club = club
        self.divisionFilter = divisionFilter
        self.subdivision = subdivision
    }
    
    /// Converts an `OrganizationGranularFilters` into an `OrganizationGranularFilterObjects`.
    ///
    /// - Parameters:
    ///   - filter: The `OrganizationGranularFilterObjects` to convert.
    ///   - realm: The `Realm` the converted objects should be fetched in.
    /// - Throws: A `FilterObjectNotFoundError` if the filters fail to fetch an object.
    init(_ filter: OrganizationGranularFilters, in realm: Realm) throws {
        self.init(club: try filter.club(in: realm),
                  divisionFilter: try DivisionFilterObjects(filter.divisionFilter, in: realm),
                  subdivision: try filter.subdivision(in: realm))
    }
    
    // MARK: Convenience Accessors
    
    var division: ZRDivision? {
        return divisionFilter.division
    }
    
    var gender: ZRGender? {
        return divisionFilter.gender
    }
    
    var sport: ZRSport? {
        return divisionFilter.sport
    }
    
}

/// The subset of filters that describe the type of division to filter objects by.
///
/// - byDivision: Filter by a specific division.
/// - granular: Filter by all divisions for a sport and/or gender.
public enum DivisionFilter: Equatable {
    
    case byDivision(divisionId: String),
    granular(sport: ZRSport?, gender: ZRGender?)
    
    /// Converts a `DivisionFilterObjects` into a `DivisionFilter`.
    ///
    /// - Warning: This should only be called in the thread for the realm of the `ZRDivision` for the filter.
    /// - Parameter filterObjects: The `DivisionFilterObjects` to convert.
    init(_ filterObjects: DivisionFilterObjects) {
        switch filterObjects {
        case .byDivision(let division): self = .byDivision(divisionId: division.identifier)
        case .granular(let sport, let gender): self = .granular(sport: sport, gender: gender)
        }
    }
    
    /// Returns `true` if none of the filters have any values.
    public var isEmpty: Bool {
        return divisionId == nil &&
            gender == nil &&
            sport == nil
    }
    
    // MARK: Convenience Accessors
    
    public var divisionId: String? {
        guard case let .byDivision(divisionId) = self else { return nil }
        return divisionId
    }
    
    public var gender: ZRGender? {
        guard case let .granular(_, gender) = self else { return nil }
        return gender
    }
    
    public var sport: ZRSport? {
        guard case let .granular(sport, _) = self else { return nil }
        return sport
    }
    
    // MARK: Object Retrieval
    
    func division(in realm: Realm) throws -> ZRDivision? {
        guard case let .byDivision(divisionId) = self else { return nil }
        guard let division = realm.object(ofType: ZRDivision.self, forPrimaryKey: divisionId) else {
            throw FilterObjectNotFoundError(type: ZRDivision.self, identifier: divisionId)
        }
        
        return division
    }
    
}

/// The subset of filters that describe the type of division to filter objects by using actual Realm `Object`
/// instances, rather than object identifiers.
///
/// - Note: This is used to resolve the objects for an `OrganizationGranularFilter` before filtering for
///         an `UnmanagedMyTeamFilterable` entity.
///
/// - byDivision: Filter by a specific division.
/// - granular: Filter by all divisions for a sport and/or gender.
enum DivisionFilterObjects {
    
    case byDivision(division: ZRDivision),
    granular(sport: ZRSport?, gender: ZRGender?)
    
    init(_ filter: DivisionFilter, in realm: Realm) throws {
        if let division = try filter.division(in: realm) {
            self = .byDivision(division: division)
        } else {
            self = .granular(sport: filter.sport, gender: filter.gender)
        }
    }
    
    // MARK: Convenience Accessors
    
    var division: ZRDivision? {
        guard case let .byDivision(division) = self else { return nil }
        return division
    }
    
    var gender: ZRGender? {
        guard case let .granular(_, gender) = self else { return nil }
        return gender
    }
    
    var sport: ZRSport? {
        guard case let .granular(sport, _) = self else { return nil }
        return sport
    }
    
}

// MARK: - Error

/// An error that can be thrown when attempting to resolve Realm `Object`s while filtering
/// `UnmanagedMyTeamFilterable` entities.
///
/// This error will be thrown if the object for an identifier given in the `MyTeamFilter` cannot be found in the
/// given `realm`.
public struct FilterObjectNotFoundError: Swift.Error, ErrorReportable {
    
    /// The type of object that could not be found.
    let type: Any.Type
    
    /// The identifier that an object could not be found for.
    let identifier: String
    
    public func errorReportTitle() -> String? {
        return "Filter Object Not Found"
    }
    
    public func errorReportUserInfo() -> [String : Any]? {
        return ["Object Type": type, "Identifier": identifier]
    }
}

/*
 *  MARK: - Equatable
 */
public func ==(a: MyTeamFilter, b: MyTeamFilter) -> Bool {
    return a.organizationFilter == b.organizationFilter
        && a.eventSiteId == b.eventSiteId
}

public func ==(a: OrganizationFilter, b: OrganizationFilter) -> Bool {
    switch (a, b) {
    case (.byTeam(let teamA), .byTeam(let teamB)):
        return teamA == teamB
        
    case (.granular(let filtersA), .granular(let filtersB)):
        return filtersA == filtersB
        
    default: return false
    }
}

public func ==(a: OrganizationGranularFilters, b: OrganizationGranularFilters) -> Bool {
    return a.clubId == b.clubId
        && a.divisionFilter == b.divisionFilter
        && a.subdivisionId == b.subdivisionId
}

public func ==(a: DivisionFilter, b: DivisionFilter) -> Bool {
    switch (a, b) {
    case (.byDivision(let divA), .byDivision(let divB)):
        return divA == divB
        
    case (.granular(let filtersA), .granular(let filtersB)):
        return filtersA.gender == filtersB.gender
            && filtersA.sport == filtersB.sport
        
    default: return false
    }
    
}
