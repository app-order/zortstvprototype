//
//  Models+DisplayNameable.swift
//
//  Created by Anthony Miller on 9/22/16.
//  Copyright (c) 2016 App-Order, LLC. All rights reserved.
//

import Foundation

import AssociatedValues
import ImageValet

import ZRCore

public protocol EventSiteDisplayable {
    
    var locationTitle: String? { get }
    
    var addressString: String { get }
    
}

extension ZREventSite: EventSiteDisplayable {
    
    public var locationTitle: String? { return title }
    
    public var addressString: String { return address }
    
}
