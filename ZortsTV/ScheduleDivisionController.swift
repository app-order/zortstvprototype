//
//  ScheduleDivisionController.swift
//
//  Created by Dominic Miller on 1/11/18.
//  Copyright © 2018 App-Order, LLC. All rights reserved.
//

import Foundation
import UIKit

import ReactiveKit

import ZRCore
import ZRScheduleListController

struct ScheduleListManager {
    let listController: ScheduleListController
    
    let fetchController: ScheduleListFetchedResultsController
    
    let filter: Property<MyTeamFilter?>
    
    init(myTeam: ZRMyTeam, filter: MyTeamFilter?) {
        self.filter = Property(filter)
        listController = ScheduleListController()
        listController.shouldShowDetailLabel = false
        
        fetchController = try! ScheduleListFetchedResultsController(myTeamId: myTeam.identifier,
                                                                   seasonId: myTeam.season.identifier,
                                                                   filter: self.filter)
        
        listController.dataSource = fetchController
    }
    
}

class ScheduleDivisionController: UIViewController {
    
    let manager: ScheduleListManager
    
    let divisionName: String
    
    private var isScrolling: Bool = false
    
    var isAtTopOfScrollView: Bool {
        guard let tableView = manager.listController.tableView else { return false }
        return tableView.contentOffset.y == 0
    }
    
    var isAtBottomOfScrollView: Bool {
        guard let tableView = manager.listController.tableView else { return false }
        
        let targetY = max((tableView.contentSize.height - tableView.frame.height), 0)
        return targetY == tableView.contentOffset.y - 30
    }
    
    private var canScroll: Bool {
        guard let tableView = manager.listController.tableView else { return false }
        return tableView.contentSize.height > tableView.frame.height
    }
    
    var hasItems: Bool {
        return manager.fetchController.numberOfDays() > 0
    }
    
    init(myTeam: ZRMyTeam, division: ScheduleDivision) {
        divisionName = division.name
        manager = ScheduleListManager(myTeam: myTeam, filter: division.filter)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) not implemented!")
    }
    
    override func loadView() {
        view = UIView()
        
        let nameLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 50))
        nameLabel.autoresizingMask = [.flexibleWidth]
        nameLabel.text = divisionName
        nameLabel.textColor = .white
        nameLabel.textAlignment = .center
        view.addSubview(nameLabel)
        
        let listController = manager.listController
        
        listController.willMove(toParentViewController: self)
        listController.view.frame = CGRect(x: 0, y: 50, width: view.frame.width, height: view.frame.height)
        listController.tableView?.contentInset = UIEdgeInsets.zero
        listController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addChildViewController(listController)
        
        view.addSubview(listController.view)
        
        listController.didMove(toParentViewController: self)
    }
    
    func startScrolling() {
        self.view.layoutIfNeeded()
        
        if canScroll {
            if isAtTopOfScrollView {
                DispatchQueue.main.after(when: 1.0) { [weak self] in
                    self?.isScrolling = true
                    self?.continueScrolling()
                }
            } else {
                isScrolling = true
                continueScrolling()
            }
        }
    }
    
    private func continueScrolling() {
        UIView.animate(withDuration: 0.01, delay: 0.0, options: [.curveLinear, .layoutSubviews], animations: { [weak self] in
            guard let _self = self, let tableView = _self.manager.listController.tableView else { return }
            
            let lastOffset = tableView.contentOffset
            
            let targetY = min(tableView.contentSize.height, lastOffset.y + 1)
            tableView.contentOffset = CGPoint(x: 0, y: targetY)
            
            }, completion: { [weak self] _ in
                guard let _self = self, _self.isScrolling else { return }
                
                if _self.isAtBottomOfScrollView {
                    DispatchQueue.main.after(when: 1.0) {
                        _self.manager.listController.tableView?.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
                        
                        DispatchQueue.main.after(when: 1.0) {
                            if _self.isScrolling {
                                _self.continueScrolling()
                            }
                        }
                    }
                    
                } else {
                    _self.continueScrolling()
                }
        })
    }
    
    func stopScrolling() {
        isScrolling = false
    }
    
}
