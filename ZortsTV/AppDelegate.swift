//
//  AppDelegate.swift
//  ZortsTV
//
//  Created by Anthony Miller on 2/21/17.
//  Copyright © 2017 App-Order, LLC. All rights reserved.
//

import UIKit

import ImageValet
import RealmSwift
import ReactiveKit

import ZRCore

let ZortsAppDelegate = UIApplication.shared.delegate as! AppDelegate
let ZortsDidLogoutUserNotification: Notification.Name = Notification.Name("com.app-order.zorts.DidLogoutUserNotification")

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
    
    var rootVC: RootContainerViewController!
    
    var window: UIWindow?
    
    /// The `ObjectStoreRequestSerializer` to use app wide for uploading and downloading images and other files.
    var objectStoreRequestSerializer: ObjectStoreRequestSerializer! {
        didSet {
            ImageValet.zr_defaultRequestSerializer = objectStoreRequestSerializer
        }
    }
    
    private lazy var authManager: AuthenticationManager = {
        return AuthenticationManager()
    }()
    
    // MARK: Will Finish Launching
    
    public func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions:
        [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
        
        rootVC = window?.rootViewController as! RootContainerViewController
        
        setUpCriticalServices()
        
        window?.makeKeyAndVisible()
        
        return true
    }
    
    // MARK:  Did Finish Launching
    
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        if !AuthenticationManager.hasAuthenticatedUser {
            _ = showLoginVC()
            
        } else {
//            SyncManager.shared.startPeriodicSyncing()
            showTabBar()
        }
        
        if CurrentUser.isValid {
            UIApplication.shared.registerForRemoteNotifications()
        }
        
        return true
    }
    
    /*
     *  MARK: Set Up Services
     */
    
    private func setUpCriticalServices() {
        setUpRealmConfiguration()
        API.default = API(configuration: apiConfiguration())
        setUpObjectStoreRequestSerializer()
        ImageValet.zr_defaultPlaceholderImage = #imageLiteral(resourceName: "ZortsShieldHiRes")
    }
    
    private func setUpRealmConfiguration() {
        Realm.Configuration.defaultConfiguration = ZRCore.DefaultRealmConfiguration
//        Realm.Configuration.defaultConfiguration.inMemoryIdentifier = "ZortsTV-MemoryIdentifier"
    }
    
    private func apiConfiguration() -> API.Configuration {
        let mainBundle = Bundle.main
        let urlString = mainBundle.object(forInfoDictionaryKey: "ZortsURL") as! String
        let baseURL = URL(string: urlString)!
        let apiVersion = mainBundle.object(forInfoDictionaryKey: "ZortsAPIVersion") as! String
        
        return API.Configuration(baseURL: baseURL, apiVersion: apiVersion)
    }
    
    private func setUpObjectStoreRequestSerializer() {
        let mainBundle = Bundle.main
        let bucket = mainBundle.object(forInfoDictionaryKey: "AmazonS3Bucket") as! String
        let awsKey = Bundle.main.object(forInfoDictionaryKey: "AWSKey") as! String
        let awsSecret = Bundle.main.object(forInfoDictionaryKey: "AWSSecret") as! String
        
        objectStoreRequestSerializer = AmazonS3ObjectStoreRequestSerializer(
            storePath: bucket,
            apiKey: awsKey,
            apiSecret: awsSecret)
    }
    
    /*
     *  MARK: Set Up Initial View
     */
    
    private func showTabBar() {
        let myTeamsList = storyboard.instantiateViewController(withIdentifier: "TabBar") as! UITabBarController
        rootVC.setViewController(myTeamsList, animationOptions: nil)
    }
    
    private func showLoginVC() -> LoginViewController {
        let loginVC = storyboard.instantiateViewController(withIdentifier: "Login") as! LoginViewController
        
        loginVC.didLogin = { [unowned self] in
            UserDataSyncManager.shared.syncRecentMyTeams(forceFullSync: true)
            self.showTabBar()
        }
        
        rootVC.setViewController(loginVC, animationOptions: nil)
        
        return loginVC
    }
    
    func logout() -> LoginViewController {
        CurrentUser.reset()
        deleteAllUserData()
        return showLoginVC()
    }
    
    private func deleteAllUserData() {
        let deleteBlock = {
//            CrashlyticsRecorder.doAndReportErrors {
            do {
                let realm = try Realm()
                
                let myTeamStubs = realm.objects(ZRMyTeamStub.self)
                for stub in myTeamStubs {
                    RealmDeletionManager.markRealmFilesForDeletion(forRealmsWithMyTeamId: stub.identifier)
                }
                
                realm.beginWrite()
                realm.deleteAll()
                try realm.commitWrite()
//            }
            } catch {}
        }
        
        if UserDataSyncManager.shared.isSyncing.value {
            let bag = DisposeBag()
            
            UserDataSyncManager.shared.isSyncing
                .only(false)
                .take(first: 1)
                .observeCompleted {
                    _ = deleteBlock()
                    bag.dispose()
                }
                .dispose(in: bag)
            
        } else {
            _ = deleteBlock()
        }
    }
    
    /*
     *  MARK: - Push Notifications
     */
    
//    fileprivate func requestUserNotificationPermissions() {
//        if #available(iOS 10.0, *) {
//            let center = UNUserNotificationCenter.current()
//            center.requestAuthorization(options: [.badge, .sound, .alert], completionHandler: { _, _ in })
//        } else {
//            let settings = UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil)
//            UIApplication.shared.registerUserNotificationSettings(settings)
//        }
//    }
    
    open func application(_ application: UIApplication,
                          didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        let token = deviceTokenString
        
        var operation: RegisterForPushNotificationsOperation?
        
        #if DEBUG
            operation = RegisterForPushNotificationsOperation(devicePushToken: token, isInDebug: true)
            
        #else
            if token != CurrentUser.pushNotificationToken {
                operation = RegisterForPushNotificationsOperation(devicePushToken: token, isInDebug: false)
            }
            
        #endif
        
        if let operation = operation {
            _ = API.default.execute(operation)
                .then { _ -> Void in
                    CurrentUser.pushNotificationToken = token
                    
                    if let selectedMyTeamInfo = self.getSelectedMyTeamStubAndSeasonId()  {
                        UserDataSyncManager.shared.syncSingleMyTeam(for: selectedMyTeamInfo.myTeamStub,
                                                                    seasonId: selectedMyTeamInfo.seasonId,
                                                                    subscribeToChangeNotifications: true)
                    }
                    
                }.catch { error in
//                    CrashlyticsRecorder.sharedInstance?.recordError(error)
            }
        }
    }
    
    
    private func getSelectedMyTeamStubAndSeasonId() -> (myTeamStub: ZRMyTeamStub, seasonId: String)? {
        guard let currentMyTeamId = ZRApplicationSettings.selectedMyTeamInfo?.myTeamId,
            let currentSeasonId = getSelectedSeasonId(),
            let realm = try? Realm(),
            let stub = realm.object(ofType: ZRMyTeamStub.self, forPrimaryKey: currentMyTeamId) else {
                return nil
        }

        return (stub, currentSeasonId)
    }

    private func getSelectedSeasonId() -> String? {
        return ZRApplicationSettings.selectedMyTeamInfo?.seasonId
    }
    
    open func application(_ application: UIApplication,
                          didFailToRegisterForRemoteNotificationsWithError error: Swift.Error) {
        
    }
    
    fileprivate func verifyRegistration(_ url: URL) {
        let loginVC = CurrentUser.isValid ? logout() : showLoginVC()
        
        if let token = verificationToken(url) {
            
            debugPrint("Verifying Registration")
            debugPrint("Token:\(token)")
            
            authManager.verifyRegistrationAndSignIn(onController: loginVC, withToken: token)
        }
    }
    
    fileprivate func verificationToken(_ url: URL) -> String? {
        if let query = url.query {
            return query.replacingOccurrences(of: "t=",
                                              with: "",
                                              options: [],
                                              range: query.startIndex ..< query.index(query.startIndex, offsetBy: 2))
            
        }
        return nil
    }
    
    
    /*
     *  MARK: Did Receive Remote Notification
     */
    
    open func application(_ application: UIApplication,
                          didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                          fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        if let notificationSyncData = userInfo["sync"] as? [NSNumber] {
            handleRemoteNotificationForMyTeamChange(application,
                                                    syncIds: notificationSyncData,
                                                    completionHandler: completionHandler)
//            handleRemoteNotificationForZortsUserNotification(application, completionHandler: completionHandler)
            
        } else {
            handleRemoteNotificationForZortsUserNotification(application, completionHandler: completionHandler)
        }
    }
    
    private func handleRemoteNotificationForMyTeamChange(_ application: UIApplication,
                                                         syncIds: [NSNumber],
                                                         completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        guard application.applicationState != .background else { completionHandler(.noData); return }

        let syncIds = syncIds.map { $0.stringValue }
        guard let myTeamStub = getSelectedMyTeamStubAndSeasonId()?.myTeamStub,
            syncIds.contains("-1") || syncIds.contains(myTeamStub.organizationStub.identifier),
            let seasonId = getSelectedSeasonId() else {
                completionHandler(.noData)
                return
        }

        UserDataSyncManager.shared.syncSingleMyTeam(for: myTeamStub, seasonId: seasonId)
            .then { newDataSynced in
                completionHandler(newDataSynced ? .newData :  .noData)

            }.catch { error in
                completionHandler(.failed)
        }

    }
    
    private func handleRemoteNotificationForZortsUserNotification(_ application: UIApplication,
                                                                  completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        switch application.applicationState {
        case .background, .active:
            UserDataSyncManager.shared.syncUserData()
                .then { didSyncNewData, _, _ in
                    completionHandler(didSyncNewData ? .newData : .noData)
                    
                }.catch { _ in
                    completionHandler(.failed)
            }
            
        case .inactive:
            // TODO: is .inactive the right place for this? There has to be a function specifically for handling when a user taps a push notification.
//            setNotificationTabSelected()
            completionHandler(.noData)
            
        }
    }
    
//    private func setNotificationTabSelected() {
//        if let index = tabBarController.notificationsTabIndex() {
//            tabBarController.selectedIndex = index
//        }
//    }
    
    /*
     *  MARK: - Background Fetch
     */
    
    public func application(_ application: UIApplication,
                            performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
//        let logger = ZortsLogger()
        
//        logger.info { "Beginning Background Fetch" }
        
        let completion: (UIBackgroundFetchResult) -> Void = {
//            logger.info { "Completed Background Fetch" }
            completionHandler($0)
        }
        
        performSync(backgroundCompletionHandler: completion)
    }
    
    
    /*
     *  MARK: Perform Sync
     */
    
    private func performSync(backgroundCompletionHandler handler: ((UIBackgroundFetchResult) -> Void)? = nil) {
        UserDataSyncManager.shared.syncRecentMyTeams()
            .then { newDataSynced in
                handler?(newDataSynced ? .newData : .noData)
                
            }.catch { _ in
                handler?(.failed)
        }
    }

}

