//
//  AmazonS3ObjectStoreRequestSerializer.swift
//  Zorts
//
//  Created by Anthony Miller on 8/9/17.
//  Copyright © 2017 App-Order. All rights reserved.
//

import Foundation

import Alamofire
import AmazonS3RequestManager

import ZRCore

final class AmazonS3ObjectStoreRequestSerializer: AmazonS3RequestSerializer, ObjectStoreRequestSerializer {
    
    required init(storePath: String, apiKey: String, apiSecret: String) {
        super.init(accessKey: apiKey, secret: apiSecret, region: .USStandard, bucket: storePath)
    }
    
    func downloadRequest(withPath path: String) -> URLRequest {
        return amazonURLRequest(method: .get, path: path)
    }
    
    func uploadRequest(withPath path: String) -> URLRequest {
        return amazonURLRequest(method: .put, path: path, acl: PredefinedACL.publicReadOnly)
    }
    
    func url(withPath path: String) -> URL {
        return self.url(withPath: path, subresource: nil, customParameters: nil)
    }
    
}

