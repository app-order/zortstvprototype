//
//  ScheduleListFetchedResultsController.swift
//  Zorts
//
//  Created by Anthony Miller on 5/25/16.
//  Copyright © 2016 App-Order. All rights reserved.
//

import Foundation

import ReactiveKit
import RealmSwift

import ZRCore
import ZRExtensions
import ZRQueryGenie
import ZRScheduleListController

/// Manages the creation and update handling for a list of `ZRBaseScheduleItem`s.
///
/// - Warning: This class is NOT thread safe. New instances should only be initialized on the main thread.
public final class ScheduleListFetchedResultsController {
    
    /// Errors that may be thrown while initializing a `ScheduleListFetchedResultsController`.
    ///
    /// - teamRealmNotFound: The given `team` was not in a realm.
    /// - teamSeasonNotFound: The given `team` was not in a season.
    /// - myTeamNotFound:      No `myTeam` was found for the given `myTeamId`.
    /// - myTeamSeasonNotFound: The given `myTeam` was not in a season.
    public enum InitError: Swift.Error {
        case teamRealmNotFound
        case teamSeasonNotFound
        case myTeamNotFound
        case myTeamSeasonNotFound
    }
    
    /// The types of items that a 'ScheduleListFetchedResultsController' can fetch.
    ///
    /// - all:  Fetch all schedule items, regardless of type.
    /// - game: Only fetch game type items.
    /// - event: Only fetch event type items.
    public enum ItemType {
        case all, game, event
    }
    
    /*
     *  MARK: - Instance Properties
     */
    
    /// The types of items that the controller should fetch. Defaults to `.all`.
    public var itemType: ItemType = .all {
        didSet {
            refreshFilter()
        }
    }
    
    // The realm to fetch the schedule items in
    let realm: Realm
    
    private lazy var originalItems: Results<ZRBaseScheduleItem> = {
        return self.realm.objects(ZRBaseScheduleItem.self)
            .sortedBy(ascending: true) { $0.dateDescriptor }
            .sortedBy(ascending: true) { $0.timeIsTBA }
            .sortedBy(ascending: true) { $0.startDate }
            .sortedBy(ascending: true) { $0.identifier }
    }()
    
    private(set) lazy var filteredItems: Results<ZRBaseScheduleItem> = {
        return self.originalItems
    }()
    
    var days: [ZRBaseScheduleItem.DateDescriptor] = []
    
    let timeZone: TimeZone?
    
    private var changeNotificationToken: NotificationToken?
    
    private let disposeBag = DisposeBag()
    
    let filter: ReactiveKit.Property<MyTeamFilter?>
    
    fileprivate lazy var dateFormatter: ZRDateFormatter = {
        let dateFormatter = ZRDateFormatter()
        dateFormatter.dateFormat = .long
        if let timeZone = self.timeZone { dateFormatter.timeZone = timeZone }
        return dateFormatter
    }()
    
    fileprivate let didReloadContentStream = PublishSubject1<Void>()
    fileprivate let willChangeContentStream = PublishSubject1<Void>()
    fileprivate let didChangeContentStream = PublishSubject1<Void>()
    fileprivate let didInsertSectionStream = PublishSubject1<Int>()
    fileprivate let didDeleteSectionStream = PublishSubject1<Int>()
    fileprivate let didInsertEntityStream = PublishSubject1<IndexPath>()
    fileprivate let didDeleteEntityStream = PublishSubject1<IndexPath>()
    fileprivate let didUpdateEntityStream = PublishSubject1<IndexPath>()
    fileprivate let didMoveEntityStream = PublishSubject1<(from: IndexPath, to: IndexPath)>()
    
    /*
     *  MARK: - Object Lifecycle
     */
    //TODO: Update documentation.
    /// Initializes a controller to fetch all schedule items that should be displayed in the main schedule for a myTeam
    /// with a set of filters.
    ///
    /// - Parameters:
    ///   - myTeamId:   The identifier of the myTeam to fetch the schedule for.
    ///   - filter:     The reactive property for filtering the results by. If this property's `value` changes,
    ///                 the results will be automatically updated.
    /// - Throws: An `Realm` error if a realm for the myTeam cannot be created.
    public init(myTeamId: String, seasonId: String, filter: ReactiveKit.Property<MyTeamFilter?>) throws {
        let realm = try Realm(myTeamId: myTeamId, seasonId: seasonId)
        self.realm = realm
        self.filter = filter
        
        guard let myTeam = realm.object(ofType: ZRMyTeam.self, forPrimaryKey: myTeamId) else {
            throw InitError.myTeamNotFound
        }
        guard let season = myTeam.season else { throw InitError.myTeamSeasonNotFound }
        timeZone = season.timeZone
        
        if let type = myTeam.myTeamType {
            switch type {
            case .team:
                if let team = myTeam.team {
                    originalItems = originalItems.filter { $0.teams.any { $0.identifier == team.identifier } }
                }
                
            case .club, .group:
                if let club = myTeam.club {
                    let clubTeamIds = Array(club.teams.map { $0.identifier })
                    originalItems = originalItems.filter { $0.teams.any { $0.identifier.isIn(clubTeamIds) } }
                }
                
            default: break
            }
        }
        
        refreshFilter()
        observeChangesToFilter()
        changeNotificationToken = startObservingRealmChanges()
    }
    
    /// Initializes a controller to fetch all schedule items that should be displayed for a given team.
    ///
    /// - Parameter team: The team to display the schedule items for.
    /// - Throws: An `InitError` if the team is invalid for fetching.
    public init(team: ZRTeam) throws {
        guard let realm = team.realm else { throw InitError.teamRealmNotFound }
        guard let season = team.season else { throw InitError.teamSeasonNotFound }
        
        self.realm = realm
        let teamFilter = MyTeamFilter(organizationFilter: .byTeam(teamId: team.identifier),
                                      eventSiteId: nil)
        self.filter = Property(teamFilter)
        timeZone = season.timeZone
        
        refreshFilter()
        changeNotificationToken = startObservingRealmChanges()
    }
    
    deinit {
        disposeBag.dispose()
        changeNotificationToken?.invalidate()
    }
    
    /*
     *  MARK: Instance Methods
     */
    
    // MARK: Filtering
    
    private func refreshFilter() {
        var newFilter = originalItems
        defer { filteredItems = newFilter; self.reloadDays() }
        
        switch itemType {
        case .all: break
        case .game:
            newFilter = newFilter.filter { $0.concreteGameLinkingObjects.count() > 0 }
        case .event:
            newFilter = newFilter.filter { $0.concreteEventLinkingObjects.count() > 0 }
        }
        
        guard let filter = self.filter.value else { return }
        
        switch filter.organizationFilter {
        case .byTeam(let teamId):
            newFilter = newFilter.filter { $0.teams.any { $0.identifier == teamId } }
            
        case .granular(let filters):
            
            if let clubId = filters.clubId,
                let club = realm.object(ofType: ZRClub.self, forPrimaryKey: clubId) {
                let clubTeamIds = Array(club.teams.map { $0.identifier })
                newFilter = newFilter.filter { $0.teams.any { $0.identifier.isIn(clubTeamIds) } }
            }
            
            if let divisionId = filters.divisionId {
                newFilter = newFilter.filter { $0.division.identifier == divisionId }
            }
            
            if let subdivisionId = filters.subdivisionId {
                newFilter = newFilter.filter { $0.teams.any { $0.subdivision.identifier == subdivisionId } }
            }
            
            if let gender = filters.gender {
                newFilter = newFilter.filter { $0.gender_string == gender.rawValue }
            }
            
            if let sport = filters.sport {
                newFilter = newFilter.filter { $0.sport_string == sport.rawValue }
            }
        }
        
        if let siteId = filter.eventSiteId {
            newFilter = newFilter.filter { $0.concreteGameLinkingObjects.any { $0.site.identifier == siteId } }
        }
    }
    
    // MARK: Observe Changes
    
    private func observeChangesToFilter() {
        filter.skip(first: 1)
            .observeIn(.immediateOnMain)
            .observeNext { [weak self] filter in
                self?.refreshFilter()
                
            }.dispose(in: disposeBag)
    }
    
    private func startObservingRealmChanges() -> NotificationToken {
        return originalItems.observe { [weak self] change in
            self?.reloadDays()
        }
    }
    
    // MARK: Reload
    
    private func reloadDays() {
        guard let dateDescriptors = filteredItems.value(forKey: "dateDescriptor") as? [String] else { return }
        
        let addDistinct: ([String], String) -> [String] = { $0 + (!$0.contains($1) ? [$1] : [] ) }
        days = dateDescriptors.reduce([], addDistinct)
        
        didReloadContentStream.next(())
    }
    
    // MARK: Find Nearest Index
    
    /// Finds the index path for the nearest item on or after the given date. If the no items come after the given date,
    /// the index path of the nearest item before the given date is returned. `timeIsTBA` items are considered to occur
    /// at the end of the day they occur on.
    ///
    /// - Parameter date: The target date to find the index path nearest to.
    /// - Returns: The index path of the item nearest to the target date.
    public func indexPathForItem(nearestTo date: Date) -> IndexPath? {
        guard let (day, dayIndex) = dayAndDayIndex(nearestTo: date) else { return nil }
        let items = self.items(forDayAt: dayIndex)
        
        let lastIndexPath = IndexPath(row: items.endIndex - 1, section: dayIndex)
        
        let targetComponents = dateComponents(from: date)
        if compare(day, to: targetComponents) == .orderedAscending {
            return lastIndexPath
        }
        
        if let lastItem = items.last, lastItem.startDate < date && !lastItem.timeIsTBA {
            return lastIndexPath
        }
        
        if let firstItem =  items.first, firstItem.timeIsTBA {
            return lastIndexPath
        }
        
        if let itemAfter = items.first({ $0.timeIsTBA == false && $0.startDate >= date }),
            let indexAfter = items.index(of: itemAfter) {
            return IndexPath(row: indexAfter, section: dayIndex)
        }
        
        return lastIndexPath
    }
    
    private func dateComponents(from date: Date) -> DateComponents {
        let timeZone = self.timeZone ?? TimeZone.autoupdatingCurrent
        return Calendar.current.dateComponents(in: timeZone, from: date)
    }
    
    private func compare(_ day: ZRBaseScheduleItem.DateDescriptor, to target: DateComponents) -> ComparisonResult {
        guard let (year, month, day) = self.dayComponents(from: day) else {
            return .orderedAscending
        }
        
        guard let targetYear = target.year else { return .orderedDescending }
        
        if year < targetYear { return .orderedAscending }
        if year == targetYear {
            
            guard let targetMonth = target.month else { return .orderedDescending }
            if month < targetMonth { return .orderedAscending }
            if month == targetMonth {
                
                guard let targetDay = target.day else { return .orderedDescending }
                if day < targetDay { return .orderedAscending }
                if day == targetDay { return .orderedSame }
            }
        }
        
        return .orderedSame
    }
    
    /// Returns the section index for the day nearest to, but not before, the given date.
    ///
    /// - Parameter date: The target date to find the nearest section to.
    /// - Returns: The index of the section in the results.
    public func indexForDayNearest(to date: Date) -> Int? {
        return dayAndDayIndex(nearestTo: date)?.1
    }
    
    private func dayAndDayIndex(nearestTo date: Date) -> (ZRBaseScheduleItem.DateDescriptor, Int)? {
        guard let lastDay = days.last else { return nil }
        
        let target = dateComponents(from: date)
        
        if compare(lastDay, to: target) == .orderedAscending { return (lastDay, days.endIndex - 1) }
        
        for (i, day) in days.enumerated() {
            let comparison = compare(day, to: target)
            
            switch  comparison {
            case .orderedSame, .orderedDescending: return (day, i)
            case .orderedAscending: continue
            }
            
        }
        
        return (lastDay, days.endIndex - 1)
    }
    
}

/*
 *  MARK: - ScheduleListDataSource
 */

extension ScheduleListFetchedResultsController: ScheduleListDataSource {
    
    public var didReloadContent: Signal1<Void> { return didReloadContentStream.toSignal() }
    public var willChangeContent: Signal1<Void> { return willChangeContentStream.toSignal() }
    public var didChangeContent: Signal1<Void> { return didChangeContentStream.toSignal() }
    public var didInsertDay: Signal1<Int> { return  didInsertSectionStream.toSignal() }
    public var didDeleteDay: Signal1<Int> { return didDeleteSectionStream.toSignal() }
    public var didInsertItem: Signal1<IndexPath> { return didInsertEntityStream.toSignal() }
    public var didDeleteItem: Signal1<IndexPath> { return didDeleteEntityStream.toSignal() }
    public var didUpdateItem: Signal1<IndexPath> { return didUpdateEntityStream.toSignal() }
    public var didMoveItem: Signal1<(from: IndexPath, to: IndexPath)> { return didMoveEntityStream.toSignal() }
    
    public func numberOfDays() -> Int {
        return days.count
    }
    
    public func numberOfItems(forDayAt index: Int) -> Int {
        return items(forDayAt: index).count
    }
    
    func items(forDayAt index: Int) -> Results<ZRBaseScheduleItem> {
        let dayDescriptor = days[index]
        return filteredItems.filter({ $0.dateDescriptor == dayDescriptor })
    }
    
    public func titleForDay(at index: Int) -> String {
        guard let date = dateOfSection(at: index) else { return "" }
        return dateFormatter.string(from: date)
    }
    
    func dateOfSection(at index: Int) -> Date? {
        guard let (year, month, day) = self.dayComponents(from: days[index]) else {
            return nil
        }
        
        return DateComponents(calendar: Calendar.current, timeZone: timeZone, year: year, month: month, day: day).date
    }
    
    fileprivate func dayComponents(from day: ZRBaseScheduleItem.DateDescriptor) -> (year: Int, month: Int, day: Int)? {
        let dayComponents = day.components(separatedBy: "/")
        guard dayComponents.count == 3,
            let year = Int(dayComponents[0]),
            let month = Int(dayComponents[1]),
            let day = Int(dayComponents[2]) else {
                return nil
        }
        return (year, month, day)
    }
    
    public func scheduleItem(at indexPath: IndexPath) -> ScheduleListItemViewModel {
        let item = items(forDayAt: indexPath.section)[indexPath.row]
        return scheduleItemViewModel(item)
    }
    
    fileprivate func scheduleItemViewModel(_ scheduleItem: ZRBaseScheduleItem) -> ScheduleListItemViewModel {
        if let game = scheduleItem.concreteInstance?.game {
            return GameListItemViewModelFromZRGame(game: game)
            
        } else if let event = scheduleItem.concreteInstance?.event {
            return EventListItemViewModelFromZREvent(event: event)
            
        } else {
            return ScheduleListItemViewModelFromZRBaseScheduleItem(scheduleItem: scheduleItem)
        }
    }
    
}
