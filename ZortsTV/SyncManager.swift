//
//  SyncManager.swift
//
//  Created by Anthony Miller on 2/23/17.
//  Copyright (c) 2017 App-Order, LLC. All rights reserved.
//

//import Foundation
//
//import PromiseKit
//
//import ZRCore
//
//class SyncManager {
//    
//    static let shared = SyncManager()
//    
//    private var syncTimer: Timer?
//    
//    private var isRunningSync = false
//    
//    func startPeriodicSyncing() {
//        runSync()
//        syncTimer = Timer.scheduledTimer(timeInterval: 20,
//                                         target: self,
//                                         selector: #selector(runSync),
//                                         userInfo: nil, repeats: true)
//    }
//    
//    @objc func runSync() {
//        if isRunningSync { return }
//        
//        isRunningSync = true
//        print("sync")
//        UserDataSyncManager.shared.syncRecentMyTeams().then { _ in
//            self.isRunningSync = false
//        }
//    }
//    
//    func stopPeriodicSyncing() {
//        syncTimer?.invalidate()
//        syncTimer = nil
//    }
//    
//}

