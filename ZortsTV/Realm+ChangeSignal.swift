//
//  Realm+ChangeSignal.swift
//
//  Created by Anthony Miller on 3/29/17.
//  Copyright (c) 2017 App-Order, LLC. All rights reserved.
//

import Foundation

import ZRQueryGenie
import ReactiveKit
import RealmSwift

extension Realm {
    
    /// Creates a signal that will emit events for changes to the reciever.
    ///
    /// - Note: This uses the notification functionality of `Realm`. All events will be emited asynchronously after a
    ///         change occurs.
    ///
    /// - The `next` event will include the changed properties.
    ///
    /// - Returns: The generated `Signal`
    ///
    // - Warning: The returned `Signal` will retain the reciever until it is disposed.
    public func changeSignal() -> Signal1<(Realm.Notification, Realm)> {
        return Signal { observer in
            let token = self.observe { (n: Realm.Notification, r: Realm) -> Void in
                observer.next((n, r))
            }
            
            return BlockDisposable {
                token.invalidate()
            }
        }
    }
    
}

extension Object {
    
    /// Creates a signal that will emit events for changes to the reciever.
    ///
    /// - Note: This uses the notification functionality of `Realm`. All events will be emited asynchronously after a
    ///         change occurs.
    ///
    /// - The `next` event will include the changed properties.
    /// - The `completed` event indicates that the object has been deleted.
    /// - The `failed` event indicates that an error occured in retrieving changes.
    ///
    /// - Returns: The generated `Signal`
    ///
    // - Warning: The returned `Signal` will retain the reciever until it is disposed.
    public func changeSignal() -> Signal<[PropertyChange], NSError> {
        return Signal { observer in
            let token = self.observe { (change) in
                switch change {
                case .change(let properties): observer.next(properties)
                case .deleted: observer.completed()
                case .error(let error): observer.failed(error)
                }
            }
            
            return BlockDisposable {
                token.invalidate()
            }
        }
    }
    
    /// Creates a signal that will emit events for changes to the given properties on the reciever.
    ///
    /// - SeeAlso: `changeSignal()`
    ///
    /// - Parameter properties: The properties on the reciever to emit change events for.
    /// - Returns:  The generated `Signal`.
    public func changeSignal(forProperties properties: [String]) -> Signal<[PropertyChange], NSError> {
        return changeSignal().filter { $0.first(where: { properties.contains($0.name) }) != nil }
    }
    
    /// Creates a signal that will emit events for changes to the given property on the reciever.
    ///
    /// - SeeAlso: `changeSignal()`
    ///
    /// - Parameter property: The property on the reciever to emit change events for.
    /// - Returns:  The generated `Signal`.
    public func changeSignal(forProperty property: String) -> Signal<[PropertyChange], NSError> {
        return changeSignal(forProperties: [property])
    }
    
}

public typealias RealmCollectionUpdate<T> = (T, deletions: [Int], insertions: [Int], modifications: [Int])

extension RealmCollection {
    
    /// Creates a signal that will emit events for changes to the reciever.
    ///
    /// - Note: This uses the notification functionality of `Realm`. All events will be emited asynchronously after a
    ///         change occurs.
    ///
    /// - The `next` event will include the updated collection and the changes made. If this is the initial load, 
    ///   the `deletions`, `insertions`, and `modifications` will be empty.
    /// - The `failed` event indicates that an error occured in retrieving changes.
    ///
    /// - Returns: The generated `Signal`
    ///
    // - Warning: The returned `Signal` will retain the reciever until it is disposed.
    public func changeSignal() -> Signal<RealmCollectionUpdate<Self>, NSError> {
        return Signal { observer in
            let token = self.observe { change in
                switch change {
                case .initial(let object): observer.next((object, [], [], []))
                case .update(let update): observer.next(update)
                case .error(let error): observer.failed(error as NSError)
                }
            }
            
            return BlockDisposable {
                token.invalidate()
            }
        }
    }
}
