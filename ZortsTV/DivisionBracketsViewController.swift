//
//  DivisionBracketsViewController.swift
//
//  Created by Anthony Miller on 2/22/17.
//  Copyright (c) 2017 App-Order, LLC. All rights reserved.
//

import UIKit

import ZRBracketControllers

class DivisionBracketsViewController: UIViewController {
    
    var brackets: [DivisionBracket] = []
    
    var bracketViewControllers: [BracketViewController] = []
    
    var currentBracketViewController: BracketViewController?
    
    var transitionTimer: Timer?
    
    let delayBetweenTransitions: TimeInterval = 25.0
    
    @IBOutlet var divisionNameLabel: UILabel!
    
    @IBOutlet var bracketContainerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpBracketViewControllers()
        displayInitialBracketController()
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//
//        displayInitialBracketController()
//    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        setUpDivisionNameLabelTransition()
    }
    
    private func setUpDivisionNameLabelTransition() {
        let transition = CATransition()
        transition.duration = 0.75
        transition.type = kCATransitionFade
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.isRemovedOnCompletion = false
        divisionNameLabel.layer.add(transition, forKey: "fade")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        stopTransitionTimer()
    }
    
    private func setUpBracketViewControllers() {
        bracketViewControllers = brackets.map {
            let controller = BracketViewController()
            controller.showBackgroundImage = false
            controller.setGames($0.games)
            return controller
        }
    }
    
    private func displayInitialBracketController() {
        guard let bracketController = bracketViewControllers.first else { return }
        display(bracketViewController: bracketController, animated: false)
        divisionNameLabel.text = brackets.first?.name
        divisionNameLabel.textColor = .white
    }
    
    private func display(bracketViewController: BracketViewController, animated: Bool) {
        self.view.layoutIfNeeded()
        
        let new = bracketViewController
        let old = currentBracketViewController
        
        reset(bracketController: new)
        
        old?.willMove(toParentViewController: nil)
        new.willMove(toParentViewController: self)
        
        self.addChildViewController(new)
        new.view.frame = bracketContainerView.bounds
        
        let completion: (TimeInterval) -> Void = { scrollTime in
            guard self.bracketViewControllers.count > 1 else { return }
            let waitTime = max(self.delayBetweenTransitions - scrollTime, 5.0)
            self.startTransitionTimer(time: waitTime)
        }
        
        if let old = old, animated {
            let animationOptions: UIViewAnimationOptions = [.transitionCrossDissolve, .layoutSubviews]
            self.transition(from: old,
                            to: new,
                            duration: 0.5,
                            options: animationOptions,
                            animations: nil) { (finished) in
                                old.removeFromParentViewController()
                                new.didMove(toParentViewController: self)
                                new.view.frame = self.bracketContainerView.bounds
                                self.currentBracketViewController = new
                                self.scrollFromTopOfBracketToBottom(new, completion: completion)
            }
        } else {
            old?.view.removeFromSuperview()
            old?.removeFromParentViewController()
            bracketContainerView.addSubview(new.view)
            new.didMove(toParentViewController: self)
            self.currentBracketViewController = new
            self.scrollFromTopOfBracketToBottom(new, completion: completion)
        }
    }
    
    private func reset(bracketController: BracketViewController) {
        if !bracketController.isSingleElimination && bracketController.isDisplayingLosersBracket {
            bracketController.swapBracket()
        }
        
        if let scrollView = bracketController.currentScrollView {
            scrollView.contentOffset = CGPoint(x: 0, y: 0)
        }
    }
    
    private func scrollFromTopOfBracketToBottom(_ bracketController: BracketViewController, completion: ((TimeInterval) -> Void)? ) {
        guard let scrollView = bracketController.currentScrollView else { completion?(0); return }
        let contentHeight = scrollView.contentSize.height
        let frameHeight = scrollView.frame.height
        let difference = contentHeight - frameHeight
        if difference > 30 {
            
            let duration = Double(difference / 30)
            
            UIView.animate(withDuration: duration,
                           delay: 0.0,
                           options: [.curveLinear, .layoutSubviews],
                           animations: {
                            scrollView.contentOffset = CGPoint(x: scrollView.contentOffset.x, y: difference)
            },
                           completion: { _ in
                            completion?(duration)
            })
            
        } else {
            completion?(0)
        }
    }
    
    private func startTransitionTimer(time: TimeInterval) {
        transitionTimer = Timer.scheduledTimer(timeInterval: time,
                                               target: self,
                                               selector: #selector(self.transitionToNextBracket),
                                               userInfo: nil,
                                               repeats: true)
    }
    
    private func stopTransitionTimer() {
        transitionTimer?.invalidate()
        transitionTimer = nil
    }
    
    @objc func transitionToNextBracket() {
        guard let current = currentBracketViewController,
            let currentIndex = bracketViewControllers.index(of: current) else {
                displayInitialBracketController()
                return
        }
        
        stopTransitionTimer()
        
        let nextIndex = bracketViewControllers.count > currentIndex + 1 ? currentIndex + 1 : 0
        let nextBracket = bracketViewControllers[nextIndex]
        let nextName = brackets[nextIndex].name
        
        divisionNameLabel.text = nextName
        display(bracketViewController: nextBracket, animated: true)
    }
    
    public func startTransitions() {
        guard bracketViewControllers.count > 1 else {
            displayInitialBracketController()
            return
        }
        transitionToNextBracket()
    }
    
    public func stopTransitions() {
        stopTransitionTimer()
    }
    
    
}

struct DivisionBracket {
    
    let name: String
    let games: [LeagueGameViewModel]
    
}
