//
//  SeasonStubCell.swift
//  Zorts
//
//  Created by Jett Farmer on 9/7/17.
//  Copyright © 2017 App-Order. All rights reserved.
//

import UIKit

/// A `UITableViewCell` for displaying the information from a `SeasonStub` object.
class SeasonCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var logoImageView: UIImageView!
    
    @IBOutlet var nameLabel: UILabel!
    
    @IBOutlet var endDateLabel: UILabel!
    
    override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        switch isFocused {
        case true:
            layer.borderWidth = 2
            layer.borderColor = borderColor()
            
        case false:
            layer.borderWidth = 0
        }
    }
    
    private func borderColor() -> CGColor {
        switch traitCollection.userInterfaceStyle {
        case .light:
            return UIColor.black.cgColor
            
        default:
            return UIColor.lightGray.cgColor
        }
    }
    
}
/*UITableViewCell {
    
    // MARK: - IBOutlets
    
    /// A label for displaying the name of a `seasonStub`
    @IBOutlet var nameLabel: UILabel!
    
    /// Displays the `endDate` of a `seasonStub`
    @IBOutlet var endDateLabel: UILabel!
    
    /// Displays the logo for a `seasonStub`
    @IBOutlet var logoImageView: UIImageView!
    
    // MARK: - Prepare for Reuse

    public override func prepareForReuse() {
        nameLabel.text = nil
        endDateLabel.text = nil
        logoImageView.image = ZortsPlaceholderImage
    }
    
}*/
