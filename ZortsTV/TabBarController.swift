//
//  TabBarController.swift
//
//  Created by Anthony Miller on 2/21/17.
//  Copyright (c) 2017 App-Order, LLC. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBar.isHidden = true
    }
    
}
