//
//  ScheduleListItemViewModelFromZRBaseScheduleItem.swift
//  Zorts
//
//  Created by Anthony Miller on 5/27/16.
//  Copyright © 2016 App-Order. All rights reserved.
//

import Foundation

import ZRCore
import ZRScheduleListController

class ScheduleListItemViewModelFromZRBaseScheduleItem: ScheduleListItemViewModel {
    
    let scheduleItem: ZRBaseScheduleItem
    
    let startDate: String
    
    let detailText: String?
    
    let isCanceled: Bool
    
    init(scheduleItem: ZRBaseScheduleItem) {
        self.scheduleItem = scheduleItem
        self.isCanceled = scheduleItem.canceled
        
        self.detailText = ScheduleItemDivisionInformationFormatter.divisionInfo(for: scheduleItem)
        
        self.startDate = {
            if scheduleItem.timeIsTBA { return "TBA" }
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "h:mm\na"
            dateFormatter.timeZone = scheduleItem.season.timeZone
            return dateFormatter.string(from: scheduleItem.startDate)
        }()
    }
    
}

