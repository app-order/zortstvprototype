//
//  MyTeamViewController.swift
//
//  Created by Anthony Miller on 2/22/17.
//  Copyright (c) 2017 App-Order, LLC. All rights reserved.
//

import UIKit

import ImageValet
import RealmSwift

import ZRBracketControllers
import ZRCore
import ZRQueryGenie

enum ScheduleState {
    case list, brackets
}

class MyTeamViewController: UIViewController {
    
    var myTeam: ZRMyTeam!
    
    var realm: Realm {
        return myTeam.realm!
    }
    var hasBrackets: Bool = false
    
    var stateInView: ScheduleState = .list
    
    var scheduleListViewController: ScrollingStackViewController!
    var bracketsViewController: DivisionBracketsViewController!
    
    @IBOutlet var imageView: UIImageView!
    
    @IBOutlet var teamNameLabel: UILabel!
    
    @IBOutlet var teamInfoLabel: UILabel!
    
    @IBOutlet var scheduleContainerView: UIView!
    
    @IBAction func buttonWasSelected() {
        guard hasBrackets else { return }
        
        switch stateInView {
        case .list:
            scheduleListViewController.stopAllScrolling()
            transition(to: .brackets) { [weak self] in
                guard let _self = self else { return }
                _self.bracketsViewController.startTransitions()
            }
            
        case .brackets:
            bracketsViewController.stopTransitions()
            transition(to: .list) { [weak self] in
                guard let _self = self else { return }
                _self.scheduleListViewController.startAllScrolling()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureMyTeam()
        setUpScheduleView()
    }
    
    private func configureMyTeam() {
        let teamName = myTeam.name ?? myTeam.club?.name ?? ""
        teamNameLabel.text = teamName.uppercased()
        teamInfoLabel.text = myTeamDetailText()
        configureOrganizationIconImage()
    }
    
    private func myTeamDetailText() -> String {
        var components = [String]()
        
        if let team = myTeam.team {
            components.append(team.division.displayName())
        }
        if let seasonName = myTeam.season.name {
            components.append(seasonName)
        }
        
        return components.joined(separator: " | ")
    }
    
    private func configureOrganizationIconImage() {
        ImageValet(imageModel: myTeam.club?.icon).deliverToImageView(imageView)
    }
    
}

extension MyTeamViewController {
    
    func setUpScheduleView() {
        let supportedDivisions = self.supportedDivisions()
        let bracketTypes = self.bracketTypes(supportedDivisions)
        
        if !bracketTypes.isEmpty {
            hasBrackets = true
            setUpDivisionBracketsController(bracketTypes: bracketTypes)
        }
        showScheduleStackViewController(divisions: supportedDivisions)
    }
    
    private func supportedDivisions() -> [ZRDivision] {
        return myTeam.supportedDivisions.sorted { $0.isOrderedBefore($1) }
    }
    
    private func bracketTypes(_ divisions: [ZRDivision]) -> [ZRDivision] {
        return divisions.filter {
            bracketGames($0).count() > 0
        }
    }
    
    private func games(_ division: ZRDivision) -> Results<ZRGame> {
        return realm.objects(ZRGame.self)
            .filter { $0.isLeagueGame == false }
            .filter { $0.baseScheduleItem.division.identifier == division.identifier }
        
    }

    private func bracketGames(_ division: ZRDivision) -> Results<ZRGame> {
        return realm.objects(ZRGame.self)
            .filter { $0.isLeagueGame == true }
            .filter { $0.baseScheduleItem.division.identifier == division.identifier }
            .filter { $0.bracketBackingData != nil }
    }
    
    private func setUpDivisionBracketsController(bracketTypes: [ZRDivision]) {
        guard let controller = storyboard?.instantiateViewController(withIdentifier: "DivisionBrackets") as? DivisionBracketsViewController else { return }
                
        controller.brackets = divisionBrackets(for: bracketTypes)
        bracketsViewController = controller
    }
    
    private func addControllerToScheduleContainerView(controller: UIViewController) {
        controller.view.frame = scheduleContainerView.bounds
        addChildViewController(controller)
        scheduleContainerView.addSubview(controller.view)
    }
    
    private func divisionBrackets(for bracketTypes: [ZRDivision]) -> [DivisionBracket] {
        return bracketTypes.map {
            let games: [LeagueGameViewModel] = self.bracketGames($0)
                .objects()
                .flatMap({ BracketGameViewModelFromZRLeagueGame(game: $0) })
                .sorted { $0.bracketGame > $1.bracketGame }
            
            let name = $0.displayName()
            
            return DivisionBracket(name: name, games: games)
        }
    }
    
    private func showScheduleStackViewController(divisions: [ZRDivision]) {
        guard let controller = UIStoryboard(name: "StackView", bundle: nil).instantiateViewController(withIdentifier: "ScrollingStackViewController") as? ScrollingStackViewController else { return }

        controller.myTeam = myTeam
        controller.divisions = scheduleDivisions(for: divisions)
        scheduleListViewController = controller
        addControllerToScheduleContainerView(controller: controller)
    }
    
    private func scheduleDivisions(for divisions: [ZRDivision]) -> [ScheduleDivision] {
        return divisions.map {
            let filter = MyTeamFilter(divisionId: $0.identifier)
            
            let name = $0.displayName()
            
            return ScheduleDivision(name: name, filter: filter)
        }
    }
    
    private func transition(to state: ScheduleState, completion: (()-> Void)? = nil ) {
        guard stateInView != state else { return }
        let oldVC = stateInView == .list ? scheduleListViewController : bracketsViewController
        let newVC = stateInView == .list ? bracketsViewController : scheduleListViewController
        
        oldVC.willMove(toParentViewController: nil)
        addChildViewController(newVC)
        
        transition(from: oldVC,
                   to: newVC,
                   duration: 0,
                   options: .transitionCrossDissolve,
                   animations: { [weak self, oldVC, newVC] in
                    guard let _self = self else { return }
                    newVC.view.frame = _self.scheduleContainerView.bounds
                    oldVC.view.removeFromSuperview()
                    _self.scheduleContainerView.addSubview(newVC.view)
        }) { [weak self, newVC, oldVC] _ in
            guard let _self = self else { return }
            oldVC.removeFromParentViewController()
            newVC.didMove(toParentViewController: _self)
            _self.stateInView = state
            completion?()
        }
    }
    
}
