//
//  DivisionInformationFormatter.swift
//  Zorts
//
//  Created by Anthony Miller on 7/19/16.
//  Copyright © 2016 App-Order. All rights reserved.
//

import Foundation

import ZRCore

/**
 *  Defines an object that can format the information for a division, sport, and gender to be displayed as a single string.
 */
protocol DivisionInformationFormatter {
    
    var division: ZRDivision? { get }
    
    var sport: ZRSport? { get }
    
    var gender: ZRGender? { get }
    
}

extension DivisionInformationFormatter {
    
    func formattedDivisionInformation() -> String? {
        var components = [String]()
        
        if let sport = sport?.displayName() {
            components.append(sport)
        }
        
        if let gender = gender?.displayName() {
            components.append(gender)
        }
        
        if let division = division?.displayName() , !division.isEmpty {
            components.append(division)
        }
        
        return components.isEmpty ? nil : components.joined(separator: " ")
    }
}
