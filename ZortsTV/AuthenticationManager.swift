//
//  AuthenticationManager.swift
//  ZortsTV
//
//  Created by Anthony Miller on 2/25/15.
//  Copyright (c) 2015 App-Order. All rights reserved.
//

import Foundation

import ZRCore

import KeychainAccess
import PromiseKit

enum SignInState {
    
    case existingUser,
    newViaRegistration,
    newViaInvitation
    
}

class AuthenticationManager {
    
    let api: API
    
    private static let keychain: KeychainProtocol = {
        return CurrentUser.keychainClass.init(service: CurrentUser.keychainServiceIdentifier)
    }()
    
    init(api: API = API.default) {
        self.api = api
    }
    
    func signIn(userId: String, password: String, controller: LoginViewController, completion: @escaping ((String?) -> Void)) {
        
        let signInOperation = SignInOperation(userId: userId, password: password)
        
        api.execute(signInOperation)
            .then { _ -> Void in
//                defer {
//                    // TODO: Put this back if you make a real app.
//                    Answers.logLogin(withMethod: nil, success: true,
//                                     customAttributes: self.logAttributes(userId, error: nil))
//                }
                
                UserDataSyncManager.shared.syncUserData(forceFullSync: true).always {
                    controller.didLogin?()
                }
                
            }.catch { error in
//                defer {
//                    // TODO: Put this back if you make a real app.
//                    Answers.logLogin(withMethod: nil, success: false,
//                                     customAttributes: self.logAttributes(userId, error: error))
//                }
                
                if let error = (error as? ZRCore.Error)?.responseMessage {
                    completion(error.message)
                    
                } else {
                
                completion("An Error Has Occured")
                }
        }
    }
    
    func verifyRegistrationAndSignIn(onController controller: LoginViewController,
                                     withToken token: String) {
        let operation = VerifyRegistrationOperation(verificationToken: token)
        api.execute(operation)
            .then { (userId, _) -> Void in
                guard CurrentUser.userId == userId, !CurrentUser.password.isEmpty else {
                    _ = controller.view
//                    controller.setTextFieldAuthenticationValues(userId)
//                    self.bannerClass.showBanner(withStyle: .success,
//                                                title: "Account Verified",
//                                                subtitle: "Your account has been verified! Please log in to continue.")
                    return
                }
                
                self.signIn(userId: userId,
                            password: CurrentUser.password,
                            controller: controller) { _ in //[unowned self] error in
//                                if let error = error {
////                                    MBProgressHUD.hide(for: self.view, animated: true)
////                                    let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
////                                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
////                                    self.present(alert, animated: true, completion: nil)
//
//                                } else {
//                                    //                                            self.didLogin?()
//
//                                }
                }
                
            } .catch { error in
//                self.handleError(error, on: controller, forUserId: nil)
        }
    }
    
    // TODO: Remove if not used elsewhere.
    static var hasAuthenticatedUser: Bool {
        return CurrentUser.isValid
    }
    
}
