 //
 //  ZRTeam+DisplayName.swift
 //  Zorts
 //
 //  Created by Anthony Miller on 5/27/15.
 //  Copyright (c) 2015 App-Order. All rights reserved.
 //
 
 import Foundation
 
 import ZRCore
 
 /**
  This extension on `ZRTeam` adds a method to get a display name for team to be displayed in the application.
  */
 extension ZRTeam {
   
    /// The team's `clubName` and `name` (if applicable) concatenated into a single display string.
    public var clubDisplayName: String {
        var nameComponents: [String] = []
        
        if let clubName = clubName, !clubName.isEmpty {
            nameComponents.append(clubName)
        }
        
        if let teamName = name, !teamName.isEmpty {
            nameComponents.append(teamName)
        }
        
        return nameComponents.joined(separator: " ")
    }
    
 }
