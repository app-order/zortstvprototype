//
//  EventListItemViewModelFromZREvent.swift
//  Zorts
//
//  Created by Anthony Miller on 6/21/16.
//  Copyright © 2016 App-Order. All rights reserved.
//

import Foundation

import ZRCore
import ZRScheduleListController

class EventListItemViewModelFromZREvent: ScheduleListItemViewModelFromZRBaseScheduleItem, EventViewModel {
    
    let eventTitle: String
    let location: String
    
    init(event: ZREvent) {
        self.eventTitle = event.eventType.displayName
        
        self.location = event.location ?? "Location TBD"
        
        super.init(scheduleItem: event.baseScheduleItem)
    }
    
}

