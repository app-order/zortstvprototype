//
//  ZRApplicationSettings.swift
//  Zorts
//
//  Created by Anthony Miller on 4/22/15.
//  Copyright (c) 2015 App-Order. All rights reserved.
//

import Foundation

import ZRQueryGenie
import RealmSwift

import ZRCore

/*
*  MARK: NSUserDefaults Keys
*/

/// The key for the `NSUserDefault` to retrieve the myTeamId/seasonId tuple for the user's currently selected myTeam.
private let ZRSelectedMyTeamInfoKey = "ZRSelectedMyTeamInformation"

open class ZRApplicationSettings {
    
    /// A tuple containing identifier for the currently selected `ZRMyTeam` and the identifier for the
    /// `ZRSeason` associated with the currently selected `ZRMyTeam`.
    ///
    /// - Note: If no user is logged in, this will be nil.
    open class var selectedMyTeamInfo: (myTeamId: String, seasonId: String)? {
        get {
            let dict = UserDefaults.standard.dictionary(forKey: ZRSelectedMyTeamInfoKey)
            guard let myTeamId = dict?.first?.key, let seasonId = dict?.first?.value as? String else { return nil }
            return (myTeamId, seasonId)
        }
        
        set {
            guard let newValue = newValue else {
                UserDefaults.standard.set(nil, forKey: ZRSelectedMyTeamInfoKey)
                return
            }
            
            var dict = [String:Any]()
            dict[newValue.myTeamId] = newValue.seasonId
            UserDefaults.standard.set(dict, forKey: ZRSelectedMyTeamInfoKey)
        }
    }
    
}
