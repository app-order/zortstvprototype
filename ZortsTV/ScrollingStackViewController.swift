//
//  ScrollingStackViewController.swift
//
//  Created by Dominic Miller on 2/15/18.
//  Copyright © 2018 Dominic Miller. All rights reserved.
//

import Foundation
import UIKit

import ZRCore

struct ScheduleDivision {
    
    let name: String
    let filter: MyTeamFilter
    
}

class ScrollingStackViewController: UIViewController {
    
    var myTeam: ZRMyTeam!
    
    var divisions: [ScheduleDivision] = []
    
    var scheduleControllers: [ScheduleDivisionController] = []
    var validVCs: [ScheduleDivisionController] = []
    
    var currentMiddleViewControllerIndex = 1
    
    var currentIndexes = [0, 1, 2]
    
    var maxNoOfControllers = 3
    
    let autoScrollingDelay: TimeInterval = 10.0
    
    var autoScrollingTimer: Timer? {
        didSet {
            oldValue?.invalidate()
        }
    }
    
    var scrollTimer: Timer? {
        didSet {
            oldValue?.invalidate()
        }
    }
    
    var isScrollingEnabled: Bool = true {
        didSet {
            switch isScrollingEnabled {
            case false: stopAutoScrolling()
            default: break
            }
        }
    }
    
    /*
     *  MARK: - Container ViewControllers
     */
    
    fileprivate var leftContainer: ContainerViewController!
    fileprivate var middleContainer: ContainerViewController!
    fileprivate var rightContainer: ContainerViewController!
    fileprivate var nextContainer: ContainerViewController!
    
    /*
     *  MARK: - IBOutlets
     */
    
    @IBOutlet weak var leftView: UIView!
    @IBOutlet weak var middleView: UIView!
    @IBOutlet weak var rightView: UIView!
    
    @IBOutlet weak var leftContainerWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var middleContainerWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var rightContainerWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var nextContainerWidthConstraint: NSLayoutConstraint!
    
    /*
     *  MARK: - Life Cycle
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpScheduleControllers()
        registerContainerViewControllers()
        setUpWidthConstraints()
        setUpInitialViewControllers()
        startAutoScrolling()
    }
    
    private func setUpScheduleControllers() {
        scheduleControllers = divisions.map {
            let controller = ScheduleDivisionController(myTeam: myTeam, division: $0)
            
            if controller.hasItems && !validVCs.contains(controller) {
                validVCs.append(controller)
            }
            
            return controller
        }
        maxNoOfControllers = min(validVCs.count, maxNoOfControllers)
    }
    
    private func registerContainerViewControllers() {
        guard let leftController = childViewControllers[0] as? ContainerViewController else  {
            fatalError("Check storyboard for missing LeftContainerViewController")
        }
        leftContainer = leftController
        
        if maxNoOfControllers > 1 {
            guard let middleController = childViewControllers[1] as? ContainerViewController else  {
                fatalError("Check storyboard for missing MiddleContainerViewController")
            }
            middleContainer = middleController
        }
        if maxNoOfControllers > 2 {
            guard let rightController = childViewControllers[2] as? ContainerViewController else  {
                fatalError("Check storyboard for missing RightContainerViewController")
            }
            guard let nextController = childViewControllers[3] as? ContainerViewController else  {
                fatalError("Check storyboard for missing NextContainerViewController")
            }
            rightContainer = rightController
            nextContainer = nextController
        }
    }
    
    private func setUpWidthConstraints() {
        var containerWidth: CGFloat = 0
        if maxNoOfControllers > 0 {
            containerWidth = CGFloat((1740 - 24)/maxNoOfControllers)
            leftContainerWidthConstraint.constant = containerWidth
        }
        if maxNoOfControllers > 1 {
            middleContainerWidthConstraint.constant = containerWidth
        }
        if maxNoOfControllers > 2 {
            rightContainerWidthConstraint.constant = containerWidth
            nextContainerWidthConstraint.constant = containerWidth
        }
    }
    
    private func setUpInitialViewControllers() {
        var firstVC: ScheduleDivisionController
        var secondVC: ScheduleDivisionController
        var thirdVC: ScheduleDivisionController
        
        if maxNoOfControllers > 0 {
            firstVC = validVCs[0]
            leftContainer.addChild(viewController: firstVC)
            firstVC.startScrolling()
        }
        if maxNoOfControllers > 1 {
            secondVC = validVCs[1]
            middleContainer.addChild(viewController: secondVC)
            secondVC.startScrolling()
        }
        if maxNoOfControllers > 2 {
            thirdVC = validVCs[2]
            rightContainer.addChild(viewController: thirdVC)
            thirdVC.startScrolling()
        }
    }
    
}

/*
 * MARK - Auto Scrolling
 */

extension ScrollingStackViewController {
    
    private func startAutoScrolling() {
        guard validVCs.count > 3 else { return }
        autoScrollingTimer = Timer.scheduledTimer(timeInterval: autoScrollingDelay,
                                                  target: self,
                                                  selector: #selector(startTryingToScroll),
                                                  userInfo: nil,
                                                  repeats: true)
    }
    
    @objc private func startTryingToScroll() {
        scrollTimer = Timer.scheduledTimer(timeInterval: 1.0,
                                           target: self,
                                           selector: #selector(scrollIfNeeded),
                                           userInfo: nil,
                                           repeats: true)
    }
    
    @objc private func scrollIfNeeded() {
        guard isScrollingEnabled, leftContainer.controllerReference.isAtBottomOfScrollView else { return }
        
        scrollTimer?.invalidate()
        leftContainer.controllerReference.stopScrolling()
        transitionContentViews()
    }
    
    private func transitionContentViews() {
        let nextIndex = nextVCIndex()
        guard let newIndexes = Indexes(startingWith: currentMiddleViewControllerIndex) else { return }
        
        transition(from: currentIndexes, to: newIndexes)
        
        currentMiddleViewControllerIndex = nextIndex
    }
    
    private func nextVCIndex(_ index: Int? = nil) -> Int {
        let index = index != nil ? index! : currentMiddleViewControllerIndex
        let nextIndex = index + 1
        guard validVCs.count != nextIndex else { return 0 }
        return nextIndex
    }
    
    private func Indexes(startingWith left: Int) -> [Int]? {
        guard left >= 0, left < validVCs.count else { return nil }
        let middle = nextVCIndex(left)
        let right = nextVCIndex(middle)
        return [left, middle, right]
    }
    
    private func transition(from old: [Int], to new: [Int]) {
        UIView.animate(withDuration: 0.3,
                       animations: { [weak self] in
                        guard let _self = self else { return }
                        _self.nextContainer.transition(to: _self.validVCs[new[2]])
                        _self.leftView.isHidden = true
            },
                       completion: { [weak self] _ in
                        guard let _self = self else { return }
                        switch _self.shouldRemoveOldVC(givenOld: old, andNew: new) {
                        case true: _self.leftContainer.transition(to: _self.validVCs[new[0]], remove: _self.validVCs[old[0]])
                        case false: _self.leftContainer.transition(to: _self.validVCs[new[0]])
                        }
                        _self.middleContainer.transition(to: _self.validVCs[new[1]])
                        _self.rightContainer.transition(to: _self.validVCs[new[2]])
                        _self.validVCs[new[2]].startScrolling()
                        _self.leftView.isHidden = false
                        _self.currentIndexes = new
        })
    }
    
    private func shouldRemoveOldVC(givenOld old: [Int], andNew new: [Int]) -> Bool {
        return old[0] != new[0] && old[0] != new[1] && old[0] != new[2]
    }
    
    private func stopAutoScrolling() {
        autoScrollingTimer?.invalidate()
        scrollTimer?.invalidate()
    }
    
    public func stopAllScrolling() {
        if maxNoOfControllers > 0 {
            leftContainer.controllerReference.stopScrolling()
        }
        if maxNoOfControllers > 1 {
            middleContainer.controllerReference.stopScrolling()
        }
        if maxNoOfControllers > 2 {
            rightContainer.controllerReference.stopScrolling()
        }
        isScrollingEnabled = false
    }
    
    public func startAllScrolling() {
        if maxNoOfControllers > 0 {
            leftContainer.controllerReference.startScrolling()
        }
        if maxNoOfControllers > 1 {
            middleContainer.controllerReference.startScrolling()
        }
        if maxNoOfControllers > 2 {
            rightContainer.controllerReference.startScrolling()
        }
        isScrollingEnabled = true
        startAutoScrolling()
    }
    
}

class ContainerViewController: UIViewController {
    
    var controllerReference: ScheduleDivisionController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func addChild(viewController: ScheduleDivisionController) {
        addChildViewController(viewController)
        view.addSubview(viewController.view)
        viewController.view.frame = view.bounds
        viewController.view.isUserInteractionEnabled = false
        viewController.didMove(toParentViewController: self)
        controllerReference = viewController
    }
    
    func transition(to newVC: ScheduleDivisionController, remove oldVC: ScheduleDivisionController? = nil) {
        guard let oldVC = oldVC else {
            addChild(viewController: newVC)
            return
        }
        oldVC.willMove(toParentViewController: nil)
        addChildViewController(newVC)

        transition(from: oldVC,
                   to: newVC,
                   duration: 0,
                   options: .curveLinear,
                   animations: { [oldVC, newVC] in
                    newVC.view.frame = oldVC.view.frame
        }) { [weak self, newVC, oldVC] _ in
            guard let _self = self else { return }
            oldVC.removeFromParentViewController()
            newVC.didMove(toParentViewController: _self)
            _self.controllerReference = newVC
        }
    }
    
}
