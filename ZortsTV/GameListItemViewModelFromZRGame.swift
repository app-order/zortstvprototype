//
//  GameListItemViewModelFromZRGame.swift
//  Zorts
//
//  Created by Anthony Miller on 5/31/16.
//  Copyright © 2016 App-Order. All rights reserved.
//

import Foundation

import ImageValet

import ZRCore
import ZRScheduleListController

class GameListItemViewModelFromZRGame: ScheduleListItemViewModelFromZRBaseScheduleItem, GameViewModel {
    
    let homeTeamName: String
    let homeTeamScore: String
    let homeTeamIcon: ImageValet
    let homeTeamSeedNumber: String?
    let awayTeamName: String
    let awayTeamScore: String
    let awayTeamSeedNumber: String?
    let awayTeamIcon: ImageValet
    let scoreIsFinal: Bool
    let location: String
    
    init(game: ZRGame) {
        let homeTeamInfo = game.homeTeamInfo
        let awayTeamInfo = game.awayTeamInfo
        self.homeTeamIcon = ImageValet(imageModel: homeTeamInfo?.team?.owner?.icon)
        self.awayTeamIcon = ImageValet(imageModel: awayTeamInfo?.team?.owner?.icon)
        
        self.homeTeamName = game.homeTeamDisplayName()
        
        self.homeTeamScore = {
            guard let score = game.homeScore.value else { return "" }
            return String(score)
        }()
        
        switch game.bracketData?.homeTeamPlacement {
        case .some(.seed(let seedNumber)): homeTeamSeedNumber = String(seedNumber)
        default: homeTeamSeedNumber = nil
        }
        
        self.awayTeamName = game.awayTeamDisplayName()
        
        self.awayTeamScore = {
            guard let score = game.awayScore.value else { return "" }
            return String(score)
        }()
        
        switch game.bracketData?.awayTeamPlacement {
        case .some(.seed(let seedNumber)): awayTeamSeedNumber = String(seedNumber)
        default: awayTeamSeedNumber = nil
        }
        
        self.scoreIsFinal = game.scoreIsFinal
        
        //TODO: Make site not optional.
//        print(game.site?.title)
        self.location = game.site?.locationTitle ?? "Location TBD"
        
        super.init(scheduleItem: game.baseScheduleItem)
    }
    
}
