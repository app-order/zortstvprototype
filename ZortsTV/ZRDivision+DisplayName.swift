//
//  ZRDivision+DisplayName.swift
//  Zorts
//
//  Created by Anthony Miller on 5/27/15.
//  Copyright (c) 2015 App-Order. All rights reserved.
//

import Foundation
import ZRCore

/**
This extension on `ZRDivision` adds a method to get a display name for team to be displayed in the application.
*/
public extension ZRDivision {
  
  public func displayName() -> String {
    let ageString = ageForDisplayName()
    
    if name.isEmpty {
        return ageString
      
    } else if ageString.isEmpty {
        return name
        
    } else {
        return "\(name)(\(ageString))"
    }
  }
  
  fileprivate func ageForDisplayName() -> String {
    if minAge == .noAge {
      return ""
      
    } else {
      if maxAge != .noAge && minAge != maxAge {
          return "\(minAge.displayName()) to \(maxAge.displayName())"
        
      } else {
        return "\(minAge.displayName())"
      }
    }
  }
}
