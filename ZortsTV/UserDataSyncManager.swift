//
//  UserDataSyncManager.swift
//  Zorts
//
//  Created by Anthony Miller on 3/10/17.
//  Copyright (c) 2017 App-Order, LLC. All rights reserved.
//

import Foundation

import CrashlyticsRecorder
import PromiseKit
import ZRQueryGenie
import RealmSwift
import ReactiveKit

import ZRCore

/// Manages configuring, throttling, and performing user data syncs, deleting stale myTeams, and syncing recent myTeams.
public class UserDataSyncManager {
    
    public typealias SeasonMyTeam = (myTeamStub: ZRMyTeamStub, seasonStub: SeasonStub)
    
    /// The shared singleton instance of `UserDataSyncManager`.
    public static var shared = UserDataSyncManager()
    
    private struct UserDefaultKeys {
         static let LastUserDataSyncTimeStamp = "Zorts.LastUserDataSyncTimeStamp"
    }
    
    public typealias DidSyncNewData = Bool
    
    private let SecondsInADay: TimeInterval = 60 * 60 * 24
    
    /*
     *  MARK: - Properties
     */
    
    /// Indicates if any sync operations are currently executing.
    public private(set) lazy var isSyncing: ReactiveKit.Property<Bool> = {
        let property = Property(false)
        
        self.currentSyncCount
            .observeIn(.immediate)
            .observeNext(with: { property.value = $0 != 0 })
            .dispose(in: self.disposeBag)
        
        return property
    }()
    
    private let currentSyncCount = Property(0)
    
    private let disposeBag = DisposeBag()
    
    private var lastUserDataSyncTimeStamp: Int64 {
        get {
            return UserDefaults.standard.value(forKey: UserDataSyncManager.UserDefaultKeys.LastUserDataSyncTimeStamp) as? Int64 ?? 0
        }
        set {
            UserDefaults.standard.set(newValue, forKey: UserDataSyncManager.UserDefaultKeys.LastUserDataSyncTimeStamp)
        }
    }
    
    var lastSyncUserDataThrottleTime: DispatchTime?
    var lastSyncRecentMyTeamsThrottleTime: DispatchTime?
    
    /*
     *  MARK: - Object Lifecycle
     */
    
    /// The designated initializer
//    init() {
//        NotificationCenter.default.addObserver(self,
//                                               selector: #selector(didLogout),
//                                               name: ZortsDidLogoutUserNotification,
//                                               object: ZortsAppDelegate)
//    }
//
//    deinit {
//        NotificationCenter.default.removeObserver(self)
//    }
    
    @objc
    func didLogout() {
        lastUserDataSyncTimeStamp = 0
    }

    /*
     *  MARK: Syncing Functions
     */
    
    /// Executes a `SyncUserDataOperation` with the approriate time stamp and records any errors to `Crashlytics`.
    ///
    /// - Parameters:
    ///   - forceFullSync:  If `true`, the operation will sync all user data. If `false`, only new changes since the
    ///                     last sync will be synced. Defaults to `false`.
    ///   - throttleByTime: A new operation will not be started if an operation has previously been started within the
    ///                     given time interval. If `nil`, the operation will not be throttled. Defaults to `nil`.
    ///
    /// - Returns: The `Promise` that will be fulfilled when the operation completes.
    @discardableResult
    public func syncUserData(forceFullSync: Bool = false,
                             throttledByTime throttleTime: TimeInterval? = nil) -> Promise<SyncUserDataOperation.ExecutionResult> {
        if throttleSyncUserData(withTime: throttleTime) {
            return Promise(value: (false, -1, []))
        }
        
        return startSyncUserDataOperation(forceFullSync: forceFullSync)
    }
    
    private func throttleSyncUserData(withTime throttleTime: TimeInterval?) -> Bool {
        let now = DispatchTime.now()
        defer { lastSyncUserDataThrottleTime = now }
        
        guard let throttleTime = throttleTime else { return false }
        return shouldThrottle(for: throttleTime, currentTime: now)
    }
    
    private func shouldThrottle(for throttleTime: TimeInterval, currentTime: DispatchTime) -> Bool {
        guard let lastTime = lastSyncUserDataThrottleTime else { return false }
        return currentTime.rawValue < (lastTime + throttleTime).rawValue
    }
    
    @discardableResult
    private func startSyncUserDataOperation(forceFullSync: Bool) -> Promise<SyncUserDataOperation.ExecutionResult> {
        let operation = SyncUserDataOperation(sinceTimeStamp: forceFullSync ? 0 : lastUserDataSyncTimeStamp)
        
        return API.default.execute(operation)
            .tap { result in
                switch result {
                case .fulfilled(_, let timeStamp, _):
                    self.lastUserDataSyncTimeStamp = timeStamp
                    
                default: break
                }
        }
    }
    
    /// Executes a `SyncMyTeamOperation` for the given `stub` and records any errors to `Crashlytics`.
    ///
    /// - Parameters:
    ///   - stub:           The stub that the operation should sync the `ZRMyTeam` for.
    ///   - seasonId:       The identifier of the season that belongs to the myTeam.
    ///   - forceFullSync:  If `true`, the operation will sync all data for the myTeam. If `false`, only new changes 
    ///                     since the myTeam's last sync will be synced. Defaults to `false`.
    ///   - subscribeToChangeNotifications: If `true` the current user will be subscribed to push notifications for
    ///                                     changes to the myTeam for the given `stub` on the appropriate platform.
    ///                                     Defaults to `false`.
    ///
    /// - Returns: The `Promise` that will be fulfilled when the operation completes.
    @discardableResult
    public func syncSingleMyTeam(for stub: ZRMyTeamStub,
                                 seasonId: String,
                                 forceFullSync: Bool = false,
                                 subscribeToChangeNotifications: Bool = false) -> Promise<SyncMyTeamOperation.ExecutionResult> {
        let platform = subscribeToChangeNotifications ? changeNotificationPlatform() : nil
        var forceFullSync = forceFullSync
        
        do {
            let myTeamsToCount = try Realm(myTeamId: stub.identifier,
                                                           seasonId: seasonId).objects(ZRMyTeam.self)
            
            if !forceFullSync && myTeamsToCount.count == 0 {
                    forceFullSync = true
            }
            
        } catch {
            return Promise(error: error)
        }
        
        let operation = SyncMyTeamOperation(myTeamStub: stub,
                                            seasonId: seasonId,
                                            forceFullSync: forceFullSync,
                                            changeNotificationPlatform: platform)
        
        currentSyncCount.value += 1
        return API.default.execute(operation)
            .recover { error -> Promise<SyncMyTeamOperation.ExecutionResult> in
                if !forceFullSync {
                    CrashlyticsRecorder.sharedInstance?.recordError(error)
                    return API.default.execute(SyncMyTeamOperation(myTeamStub: stub,
                                                                   seasonId: seasonId,
                                                                   forceFullSync: true,
                                                                   changeNotificationPlatform: platform))
                }
                
                return Promise(error: error)
                
            }
            .always(on: zalgo) {
                self.currentSyncCount.value -= 1
        }
    }
    
    private func changeNotificationPlatform() -> SyncMyTeamOperation.ChangeNotificationPlatform {
        #if DEBUG
            return .apnsSandbox
        #else
            return .apns
        #endif
    }

    /// Executes a `SyncUserDataOperation` then, if necessary, executes `SyncMyTeamOperation`s for the user's recent
    /// myTeams and marks any stale myTeam realms for deletion with the `RealmDeletionManager`. "Recent" myTeams are
    /// defined as the 5 most recently accessed myTeams that have been accessed within the last 14 days. If there are no
    /// "recent" myTeams, the 5 most recently accessed myTeams will be synced, regardless of how long ago they were last
    /// accessed. "Stale" myTeams are defined as myTeams whose season has ended over 7 days ago and that have not been
    /// accessed in the last 3 days.
    ///
    /// - Parameters:
    ///   - forceFullSync: If `true`, the method will sync all user data. If `false`, only new changes since the
    ///                    last sync will be synced. Defaults to `false`.
    ///   - throttleByTime: A new operation will not be started if an operation has previously been started within the
    ///                     given time interval. If `nil`, the operation will not be throttled. Defaults to `nil`.
    ///
    /// - Returns: The `Promise` that will be fulfilled when the sync completes.
    @discardableResult
    public func syncRecentMyTeams(forceFullSync: Bool = false,
                                  throttledByTime throttleTime: TimeInterval? = nil) -> Promise<DidSyncNewData> {
        if throttleSyncRecentMyTeams(withTime: throttleTime) {
            return Promise(value: false)
        }
        
        return syncUserData(forceFullSync: forceFullSync)
            .then { (didSyncNewUserData, _, myTeamIdsToSync) -> Promise<DidSyncNewData> in
                let realm = try Realm()
                let stubs = realm.objects(ZRMyTeamStub.self)
            
                return self.sync(myTeamsWithIds: myTeamIdsToSync, from: stubs, forceFullSync: forceFullSync)
                    .then { didSyncNewMyTeamData in
                        return didSyncNewUserData || didSyncNewMyTeamData
                }
        }
    }
    
    private func throttleSyncRecentMyTeams(withTime throttleTime: TimeInterval?) -> Bool {
        let now = DispatchTime.now()
        defer { lastSyncRecentMyTeamsThrottleTime = now }
        
        guard let throttleTime = throttleTime else { return false }
        return shouldThrottle(for: throttleTime, currentTime: now)
    }
    
    private func sync(myTeamsWithIds seasonMyTeamsToSync: [SyncUserDataOperation.SeasonMyTeamIds],
                      from stubs: Results<ZRMyTeamStub>,
                      forceFullSync: Bool) -> Promise<DidSyncNewData> {
        
        var idPairsToSync = [(myTeamId: String, seasonId: String)]()
        
            for seasonMyTeam in seasonMyTeamsToSync {
                let myTeamId = seasonMyTeam.myTeamId
                for seasonId in seasonMyTeam.seasonIds {
                    idPairsToSync.append((myTeamId: myTeamId, seasonId: seasonId))
            }
        }
        
        let stubsToRefresh = myTeamsToRefreshIfNeeded(from: stubs).filter { stubPair in
            return idPairsToSync.contains { idPair in
                return stubPair.myTeamStub.identifier == idPair.myTeamId &&
                    stubPair.seasonStub.identifier == idPair.seasonId
            }
        }
        
        guard stubsToRefresh.count > 0 else { return Promise(value: false) }
        
        return syncMyTeams(for: stubsToRefresh, forceFullSync: forceFullSync)
    }
    
    private func myTeamsToRefreshIfNeeded(from stubs: Results<ZRMyTeamStub>) -> [SeasonMyTeam] {
        var stubsForRefresh = [SeasonMyTeam]()
        
        let stubsNotBeingDeleted = stubs
            .filter { !$0.identifier.isIn(RealmDeletionManager.myTeamIdsMarkedForDeletion) }
        
        for stub in stubsNotBeingDeleted {
            for season in stub.organizationStub.seasons {
                stubsForRefresh.append((myTeamStub: stub, seasonStub: season))
            }
        }
        
        let fourteenDaysAgo = Date(timeIntervalSinceNow: -(SecondsInADay * 14))
        let stubsAccessedInLastFourteenDays = stubsForRefresh.filter {
            guard let lastAccessedDate = $0.seasonStub.lastAccessedDate else { return false }
            return lastAccessedDate > fourteenDaysAgo
        }
        
        if stubsAccessedInLastFourteenDays.count > 0 {
            stubsForRefresh = stubsAccessedInLastFourteenDays
        }

        stubsForRefresh = stubsForRefresh.sorted(by: {
            
            guard let firstAccessedDate = $0.seasonStub.lastAccessedDate,
                  let secondAccessedDate = $1.seasonStub.lastAccessedDate else {
                    return $0.myTeamStub.identifier > $1.myTeamStub.identifier
            }

            return firstAccessedDate > secondAccessedDate
        })
        
        return Array(stubsForRefresh)
    }
    
    private func syncMyTeams(for seasonMyTeams: [SeasonMyTeam], forceFullSync: Bool) -> Promise<DidSyncNewData> {
        let myTeamSyncPromises = seasonMyTeams.map { (myTeamStub: ZRMyTeamStub, seasonStub: SeasonStub) in
            self.syncSingleMyTeam(for: myTeamStub,
                                  seasonId: seasonStub.identifier,
                                  forceFullSync: forceFullSync)
        }
        
        return when(resolved: myTeamSyncPromises)
            .then { myTeamSyncResults -> Bool in
                return self.didSyncNewData(from: myTeamSyncResults)
        }
    }
    
    private func didSyncNewData(from myTeamSyncResults: [PromiseKit.Result<DidSyncNewData>]) -> Bool {
        for case let .fulfilled(didSyncNewData) in myTeamSyncResults {
            if didSyncNewData { return true }
        }
        
        return false
    }

}
