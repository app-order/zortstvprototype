//
//  MyTeamCollectionViewCell.swift
//
//  Created by Anthony Miller on 2/21/17.
//  Copyright (c) 2017 App-Order, LLC. All rights reserved.
//

import UIKit

class MyTeamCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var imageView: UIImageView!
    
    @IBOutlet var nameLabel: UILabel!
    
    @IBOutlet var infoLabel: UILabel!
    
    override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        switch isFocused {
        case true:
            layer.borderWidth = 2
            layer.borderColor = borderColor()
            
        case false:
            layer.borderWidth = 0
        }
    }
    
    private func borderColor() -> CGColor {
        switch traitCollection.userInterfaceStyle {
        case .light:
            return UIColor.black.cgColor
            
        default:
            return UIColor.lightGray.cgColor
        }
    }
    
}
