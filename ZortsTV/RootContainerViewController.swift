//
//  RootContainerViewController.swift
//  Zorts
//
//  Created by Anthony Miller on 9/4/15.
//  Copyright (c) 2015 App-Order. All rights reserved.
//

import UIKit

open class RootContainerViewController: UIViewController {
  
  open var rootViewController: UIViewController? {
    return childViewControllers.first
  }
  
  open func setViewController(_ controller: UIViewController, animationOptions: UIViewAnimationOptions?, completion: ( (Bool) -> Void )? = nil) {
    let oldVC = rootViewController
    
    oldVC?.willMove(toParentViewController: nil)
    addChildViewController(controller)
    controller.view.frame = self.view.frame
    
    if let oldVC = oldVC,
    let options = animationOptions {
      transition(from: oldVC, to: controller, duration: 0.5, options: options, animations: { () -> Void in
        
      }, completion: { (finished) -> Void in
        oldVC.removeFromParentViewController()
        controller.didMove(toParentViewController: self)
        
        completion?(finished)
      })
      
    } else {
      view.addSubview(controller.view)
      oldVC?.view.removeFromSuperview()
      oldVC?.removeFromParentViewController()
      controller.didMove(toParentViewController: self)
      
      completion?(true)
    }    
  }
  
}
