//
//  ScheduleItemViewModelDivisionInformationFormatter.swift
//  Zorts
//
//  Created by Anthony Miller on 7/19/16.
//  Copyright © 2016 App-Order. All rights reserved.
//

import Foundation

import ZRCore
import ReactiveKit
import Bond

protocol ScheduleItemViewModelDivisionInformationFormatter: DivisionInformationFormatter {
    
    associatedtype Item: ZRBaseScheduleItem
    
    var scheduleItemForDivisionInfo: Item? { get }
    
    var showDivision: Bool { get }
    
    var showSport: Bool { get }
    
    var showGender: Bool { get }
    
}

extension ScheduleItemViewModelDivisionInformationFormatter {
    
    var division: ZRDivision? {
        if !showDivision { return nil }
        
        if let provider = scheduleItemForDivisionInfo as? ScheduleItemDivisionInformationProvider {
            return provider.divisionInformationDivision
            
        } else if let division = scheduleItemForDivisionInfo?.division {
            return division
        }
        
        return nil
    }
    
    var sport: ZRSport? {
        if !showSport { return nil }
        
        if let provider = scheduleItemForDivisionInfo as? ScheduleItemDivisionInformationProvider {
            return provider.divisionInformationSport
            
        } else if let sport = scheduleItemForDivisionInfo?.sport {
            return sport
        }
        
        return nil
    }
    
    var gender: ZRGender? {
        if !showGender { return nil }
        
        if let provider = scheduleItemForDivisionInfo as? ScheduleItemDivisionInformationProvider {
            return provider.divisionInformationGender
            
        } else if let gender = scheduleItemForDivisionInfo?.gender {
            return gender
        }
        
        return nil
    }
    
}

extension ScheduleItemViewModelDivisionInformationFormatter where Self: AnyObject {
    
    /// A stream that updates with a formatted division information string when the division, sport, or gender, on 
    /// the `scheduleItem` changes.
    var divisionInformationStream: Signal1<String?>? {
        guard let scheduleItem = scheduleItemForDivisionInfo else { return nil }
        
        var signalKeyPaths = [String]()
        if showDivision { signalKeyPaths.append("division") }
        if showSport { signalKeyPaths.append("sport_string") }
        if showGender { signalKeyPaths.append("gender_string") }
        
        if signalKeyPaths.isEmpty { return nil }
        
        let firstKeyPath = signalKeyPaths.removeFirst()
        var signal = scheduleItem.reactive.keyPath(firstKeyPath,
                                                   ofType: Optional<Any>.self,
                                                   context: .immediateOnMain).toSignal()
        
        while !signalKeyPaths.isEmpty {
            let keyPath = signalKeyPaths.removeFirst()
            signal = signal.merge(with: scheduleItem.reactive.keyPath(keyPath, ofType: Optional<Any>.self,
                                                                      context: .immediateOnMain))
        }
        
        return signal.map({ [weak self] _ in self?.formattedDivisionInformation() })
    }
}

private protocol ScheduleItemDivisionInformationProvider {
    
    var belongsToSingleTeam: Bool { get }
    
    var divisionInformationDivision: ZRDivision? { get }
    
    var divisionInformationSport: ZRSport? { get }
    
    var divisionInformationGender: ZRGender? { get }
    
}

extension ZREvent: ScheduleItemDivisionInformationProvider {
    
    fileprivate var divisionInformationDivision: ZRDivision? {
        if let division = division {
            return division
            
        } else if let team = teams.first , belongsToSingleTeam {
            return team.division
        }
        
        return nil
    }
    
    fileprivate var divisionInformationSport: ZRSport? {
        if let sport = sport {
            return sport
            
        } else if let team = teams.first , belongsToSingleTeam {
            return team.sport
        }
        
        return nil
    }
    
    fileprivate var divisionInformationGender: ZRGender? {
        if let gender = gender {
            return gender
            
        } else if let team = teams.first , belongsToSingleTeam {
            return team.gender
        }
        
        return nil
    }
    
}

//extension NSObject {
//
//    /// Returns a ```DynamicSubject``` representing the given KVO path of the given type.
//    ///
//    /// E.g. ```user.dynamic(keyPath: "name", ofType: Optional<String>.self)```
//    ///
//    public func dynamic<T>(keyPath: String, ofType: T.Type) -> DynamicSubject2<NSObject, T> where T: OptionalProtocol {
//        return DynamicSubject(
//            target: self,
//            signal: RKKeyValueSignal(keyPath: keyPath, for: self).toSignal(),
//            get: { (target) -> T in
//                let maybeValue = target.value(forKeyPath: keyPath)
//                if let value = maybeValue as? T {
//                    return value
//                } else if maybeValue == nil {
//                    return T(nilLiteral: ())
//                } else {
//                    fatalError("Could not convert \(maybeValue) to \(T.self). Maybe `dynamic(keyPath:ofExpectedType:)` method might be of help?)")
//                }
//        },
//            set: {
//                if let value = $1._unbox {
//                    $0.setValue(value, forKeyPath: keyPath)
//                } else {
//                    $0.setValue(nil, forKeyPath: keyPath)
//                }
//        },
//            triggerEventOnSetting: false
//        )
//    }
//}

