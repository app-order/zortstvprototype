
//
//  SeasonsListCollectionViewController.swift
//  Zorts
//
//  Created by Jett Farmer on 9/7/17.
//  Copyright © 2017 App-Order. All rights reserved.
//

import UIKit

import ImageValet
import RealmSwift
import PromiseKit

import ZRCore
import ZRStyles

/// A view controller for displaying a list of `seasonStubs`
class SeasonsListCollectionViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    /*
     *   MARK: - Instance Properties
     */
    
    private let SeasonCellReuseId = "SeasonCell"
    
    private lazy var dateFomatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "M/dd/yyyy"
        return formatter
    }()
    
    internal(set) public var myTeamStub: ZRMyTeamStub! {
        didSet {
            if isViewLoaded {
                self.collectionView.reloadData()
            }
        }
    }
    
    internal var seasons: [SeasonStub] {
        return Array(myTeamStub.organizationStub.seasons)
    }
    
    @IBOutlet var collectionView: UICollectionView!
    
    /*
     *   MARK: - Object Life Cycle
     */
    
//    public init(myTeamStub: ZRMyTeamStub?) {
//        self.myTeamStub = myTeamStub
//        super.init(nibName: nil, bundle: nil)
//    }
//
//    public required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
    
    /*
     *   MARK: - View Life Cycle
     */

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource = self
        collectionView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        collectionView.reloadData()
    }

    /*
     *  MARK: - UICollectionViewDataSource
     */

    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return seasons.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SeasonCell", for: indexPath) as! SeasonCollectionViewCell
        
        configure(seasonCell: cell, indexPath: indexPath)
        
        return cell
    }
    
    private func configure(seasonCell: SeasonCollectionViewCell, indexPath: IndexPath) {
        // Make sure 'indexPath.item' is right
        let seasonStub = seasons[indexPath.item]
        seasonCell.nameLabel.text = seasonStub.name
        seasonCell.endDateLabel.text = dateFomatter.string(from: seasonStub.endDate)
        ImageValet(imageModel: myTeamStub.icon).deliverToImageView(seasonCell.logoImageView)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView {
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                         withReuseIdentifier: "Header",
                                                                         for: indexPath) as! MyTeamCollectionViewSectionHeader
        headerView.titleLabel.text = titleForHeader(inSection: indexPath.section)
        return headerView
    }
    
    private func titleForHeader(inSection section: Int) -> String? {
        if self.collectionView(collectionView, numberOfItemsInSection: section) == 0 { return nil }
        return myTeamStub.name + " - Seasons"
    }
    
}


/*
 *  MARK: - UICollectionViewDelegate
 */

extension SeasonsListCollectionViewController {
    
    func collectionView(_ collectionView: UICollectionView, canFocusItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presentMyTeam(at: indexPath.item)
    }
    
    private func presentMyTeam(at index: Int) {
        let myTeamId = myTeamStub.identifier
        let seasonId = myTeamStub.organizationStub.seasons[index].identifier
        print("syncing myteam")
        UserDataSyncManager.shared.syncSingleMyTeam(for: myTeamStub, seasonId: seasonId)
            .then { sync -> SyncMyTeamOperation.ExecutionResult in
                print("sync worked")
                try self.presentMyTeam(myTeamId: myTeamId, seasonId: seasonId)
                return sync
            }.catch { _ in
                print("sync failed")
                do {
                    try self.presentMyTeam(myTeamId: myTeamId, seasonId: seasonId)
                } catch {
                    print("fail on second catch")
                }
                
        }
    }
    
    func presentMyTeam(myTeamId: String, seasonId: String) throws {
        let myTeamRealm = try Realm(myTeamId: myTeamId, seasonId: seasonId)
        myTeamRealm.refresh()
        guard let myTeam = myTeamRealm.objects(ZRMyTeam.self).first else {
            print("MyTeam Not Found")
            return
        }
        
        guard let myTeamVC = self.storyboard?.instantiateViewController(withIdentifier: "MyTeam") as? MyTeamViewController else { return } // TODO: Throw error here
        myTeamVC.myTeam = myTeam
        
        self.present(myTeamVC, animated: true, completion: nil)
    }
    
}



